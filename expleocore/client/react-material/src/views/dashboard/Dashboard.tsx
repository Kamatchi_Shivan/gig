import React, {Component} from "react";
import {connect} from 'react-redux';
import { Typography } from '@material-ui/core';

type MyProps = {
    loginData:any    
};

class Dashboard extends Component<MyProps> {

    constructor(props:any) {
        super(props);
        this.state = {
            consversionPanel: true
        }
    }

    componentWillMount() {
        console.log("loginData:>>>>>>>", this.props.loginData);
        console.log("UserName:>>>>>>>", this.props.loginData.username);
      }

    render() {
        return (
            <Typography noWrap={false}><h5>Welcome {this.props.loginData.firstname}</h5></Typography>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(Dashboard);
