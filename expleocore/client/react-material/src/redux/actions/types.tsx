export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const FETCH_POSTS = "FETCH_POSTS";
export const GET_CONVERSION_RATE = "GET_CONVERSION_RATE";
export const GET_USD_RATE = "GET_USD_RATE";
export const TOGGLE_PANEL = "TOGGLE_PANEL";
export const GET_EUR_RATE = "GET_EUR_RATE";
export const GET_MODEL = "GET_MODEL";
export const VIN_VALIDATE = "VIN_VALIDATE";
export const SAVE_CONTRACT = "SAVE_CONTRACT";
export const DEPLOY_CONTRACT = "DEPLOY_CONTRACT";
// change password
export const CHANGE_PASSWORD = "CHANGE_PASSWORD";
//language
export const LANGUAGE = "LANGUAGE";
//profile update
export const UPDATE_PROFILE = "UPDATE_PROFILE";