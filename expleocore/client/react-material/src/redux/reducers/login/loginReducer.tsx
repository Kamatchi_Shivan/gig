import { LOGIN, LOGOUT } from '../../actions/types';

const initialState = {
    Email: null,
    isLoggedIn: false,
    redirect: false,
    token: null,
}

export default function(state = initialState, action:any) {
    switch (action.type) {
        case LOGIN:
            return {
                ...action.payload
            };
        case LOGOUT:
            return {
                ...action.payload
            };

        default:
            return state;
    }
}
