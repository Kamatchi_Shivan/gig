import { Injector } from '@angular/core';

export class Singleton {
    static injector: Injector;
}