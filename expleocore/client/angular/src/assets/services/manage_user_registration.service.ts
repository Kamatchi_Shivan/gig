import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Singleton } from '../singleton/singleton';

@Injectable({
  providedIn: 'root'
})
export class ManageUserService {

  private static TAG = 'ManageUser Service';
  private serviceListener: ServiceListner;
  private http: HttpClient;

  constructor(serviceListener: ServiceListner) {

    // Initializing the service listener
    this.serviceListener = serviceListener;

    // Initializing the http 
    this.http = Singleton.injector.get(HttpClient);

   }

   private convertJSONObjToURLSearchParams(jsonObject): URLSearchParams {
    const params: URLSearchParams = new URLSearchParams();
    for (const key in jsonObject) {
      if (jsonObject.hasOwnProperty(key)) {
        params.set(key, jsonObject[key]);
      }
    }

    return params;
  }

   public post(passbackKey: string, requestLink: string){
    console.log(passbackKey);
    if (this.serviceListener == null) {
      console.log(ManageUserService.TAG + 'error - serviceListener is not initialized');
      return;
    }

    console.log('Request type = POST');
    console.log('Request link = ' + requestLink);
    // console.log('Request body = ' + JSON.stringify(requestBody));

  //  Make the HTTP Post Request
    // const headers = new HttpHeaders(
    //   customHeader
    // );
    // const params = new HttpParams().set('RestId', '15');

    // console.log('headers:', headers);
    // Creating the request body
    // const body = jsonRequest ? requestBody : this.convertJSONObjToURLSearchParams(requestBody).toString();

    // Service has started running
    this.serviceListener.onServiceRunning(true, passbackKey);
    
    this.http.get(requestLink).pipe((data => data)).subscribe(posts => {
      // Service has stopped running
      this.serviceListener.onServiceRunning(false, passbackKey);

      // Sending back the service response
      this.serviceListener.onServiceResponse(posts, passbackKey);
    }, err => {
      // Service has stopped running
    this.serviceListener.onServiceRunning(false, passbackKey);

      // Sending back the error response
      this.serviceListener.onServiceError(err, passbackKey);
    });
}
}

