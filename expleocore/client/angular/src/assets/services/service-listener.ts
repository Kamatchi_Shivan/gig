/**
 * Service Listener Mothods
 */

interface ServiceListner {
    /**
 * Sends an update on the service running status
 * 
 * @param serviceRunning The service running status
 * @param passbackKey The passback key
 */

  onServiceRunning(serviceRunning: boolean, passbackKey: string);

    /**
 * Sends an update with the service response
 * 
 * @param serviceResponse The service response
 * @param passbackKey The passback key
 */
  onServiceResponse(serviceResponse: any, passbackKey: string);

  /**
 * Sends an update with the service error message if there was an error
 * 
 * @param serviceError The service error
 * @param passbackKey The passback key
 */
  onServiceError(serviceError: any, passbackKey: string);


}