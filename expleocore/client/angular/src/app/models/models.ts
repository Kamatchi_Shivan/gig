import { Binary } from '@angular/compiler';
import { NbIconConfig } from '@nebular/theme';
import { Params } from '@angular/router';

interface IProject {
    name: String;
    type: String;
    client: String;
    description: String;
    createdDate: Date;
    createdBy: String;
    updatedDate: Date;
    updatedBy: String;
    status: String;
    imgUrl: String;
    progress: number;
    nominees: String[];
    projectDetailsFile: Blob;
}

export class Project implements IProject {
    name: String;
    type: String;
    client: String;
    description: String;
    createdDate: Date;
    createdBy: String;
    updatedDate: Date;
    updatedBy: String;
    status: String;
    imgUrl: String;
    progress: number;
    nominees: String[];
    projectDetailsFile: Blob;
}
interface IUser {
    emailAddress: String;
    password: String;
    firstName: String;
    lastName: String;
    role: String;
}

export class User implements IUser {
    emailAddress: String;
    password: String;
    firstName: String;
    lastName: String;
    role: String;
}

interface IUserdeatils {
    email: String;
    employeeid: String;
    firstTimeLogin: String;
    domain: number[];
    username: String;
    _id:string;
    name:string;
    role:string;
    profilepic:string;
}

export class Userdetails implements IUserdeatils {
    email: String;
    employeeid: String;
    firstTimeLogin: String;
    domain: number[];
    username: String;
    _id:string;
    name:string;
    role:string;
    profilepic:string;

}

interface IArea {
    areaId: number;
    domainId: number;
    areaName: String;
    description: String;
}

export class Area implements IArea {
    areaId: number;
    domainId: number;
    areaName: String;
    description: String;
}

interface IProjectinfo {
    _id: string;
    projectName: string;
    projectRefNo: string;
    customerName: string;
    startDate: Date;
    endDate: Date;
    workPackageStatus: string;
    projectStatusPercentage: string;
}

export class Projectinfo implements IProjectinfo {
    _id: string;
    projectName: string;
    projectRefNo: string;
    customerName: string;
    startDate: Date;
    endDate: Date;
    workPackageStatus: string;
    projectStatusPercentage: string;
    projectDescription:string;
}

interface ISkills {
    projectId: number;
    projectRefNo: String;
    skillId: number;
    skillRating: number;
    skillName:String;
    skillWeightage:String;
    createdDate: Date;
    lastupdateDate: Date;
    Status: string;
}
export class Skills implements ISkills {
    projectId: number;
    projectRefNo: String;
    skillId: number;
    skillRating: number;
    skillName:String;
    skillWeightage:String;
    createdDate: Date;
    lastupdateDate: Date;
    Status: string;
}

interface IDomain {
    domainId: number;
    domainName: String;
    description: String;
    createdDate: Date;
    lastupdateDate: Date;
    Status: string;
}
export class Domain implements IDomain {
    domainId: number;
    domainName: String;
    description: String;
    createdDate: Date;
    lastupdateDate: Date;
    Status: string;
}

interface Irole {
    roleId: number;
    roleName: String;
    description: String;
    privilage: String;
    createdDate: Date;
    lastUpdateDate: Date;
    status: Binary;
}
export class Role implements Irole {
    roleId: number;
    roleName: String;
    description: String;
    privilage: String;
    createdDate: Date;
    lastUpdateDate: Date;
    status: Binary;
}

export class manageUserRegistration {
    name: String;
    email: String;
    phone: Number;
}

export declare class NbMenuItem {
    /**
     * Item Title
     * @type {string}
     */
    title: string;
    /**
     * Item relative link (for routerLink)
     * @type {string}
     */
    link?: string;
    /**
     * Item URL (absolute)
     * @type {string}
     */
    url?: string;
    /**
     * Icon class name or icon config object
     * @type {string | NbIconConfig}
     */
    icon?: string | NbIconConfig;
    /**
     * Expanded by default
     * @type {boolean}
     */
    expanded?: boolean;
    /**
     * Children items
     * @type {List<NbMenuItem>}
     */
    children?: NbMenuItem[];
    /**
     * HTML Link target
     * @type {string}
     */
    target?: string;
    /**
     * Hidden Item
     * @type {boolean}
     */
    hidden?: boolean;
    /**
     * Item is selected when partly or fully equal to the current url
     * @type {string}
     */
    pathMatch?: 'full' | 'prefix';
    /**
     * Where this is a home item
     * @type {boolean}
     */
    home?: boolean;
    /**
     * Whether the item is just a group (non-clickable)
     * @type {boolean}
     */
    group?: boolean;
    /** Whether the item skipLocationChange is true or false
     *@type {boolean}
     */
    skipLocationChange?: boolean;
    /** Map of query parameters
     *@type {Params}
     */
    roles: String[];
    queryParams?: Params;
    parent?: NbMenuItem;
    selected?: boolean;
    data?: any;
    fragment?: string;
    /**
     * @returns item parents in top-down order
     */
    static getParents(item: NbMenuItem): NbMenuItem[];
    static isParent(item: NbMenuItem, possibleChild: NbMenuItem): boolean;
}


