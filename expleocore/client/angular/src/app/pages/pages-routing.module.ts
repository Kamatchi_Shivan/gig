import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectsComponent } from './projects/projects.component';
import { ManageUserRegistrationComponent } from './manage-user-registration/manage-user-registration.component';
import { SchedulerComponent } from './projects/project-details/scheduler/scheduler.component';
import { ProjectPublishingComponent } from './projects/project-publishing/project-publishing.component';
import { ProjectReviewComponent } from './projects/project-review/project-review.component';
import { ProjectDataService } from './projects/projectData.service';
import { ProjectDetailsComponent } from './projects/project-details/project-details.component';

import { PlpComponent } from './projects/plp/plp.component';


import { CollaboratorsComponent } from './projects/project-details/collaborators/collaborators.component';
import { SchedulerModifyDetailsComponent } from './projects/project-details/scheduler/scheduler-modify-details/scheduler-modify-details.component';
import { MattableComponent } from './projects/mattable/mattable.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'manageUserRegistration',
        component: ManageUserRegistrationComponent,
      },
      {
        path: 'projects',
        component: ProjectsComponent,
      },
      {
        path: 'project-publishing',
        component: ProjectPublishingComponent,
        data : {title:'Welcome'}
      },
      {
        path: 'project-review',
        component: ProjectReviewComponent,
        data : {title:'Welcome'}
      },
      {
        path: 'project-details',
        component: ProjectDetailsComponent,
        data : {title:'Project Detail'}
      },
      {
        path: 'plp',
        component: PlpComponent,
        data : {title:'Projects'}
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
      {
        path: "scheduler",
        component: SchedulerComponent,
      },
      {
        path: 'collaborators',
        component: CollaboratorsComponent,
      },
      {
        path: 'scheduler-modify-details-component',
        component: SchedulerModifyDetailsComponent,
        data : {title: 'Scheduler Create/Modify Details'}
      },
      {
        path: 'mattable',
        component: MattableComponent,
        data : {title: 'Scheduler Details'}
      },
      {
        path: '',
        redirectTo: 'ManageUserRegistrationComponent',
        pathMatch: 'full',
      },
    ],
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ProjectDataService]
})
export class PagesRoutingModule {
}
