import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageUserRegistrationComponent } from './manage-user-registration.component';

describe('ManageUserRegistrationComponent', () => {
  let component: ManageUserRegistrationComponent;
  let fixture: ComponentFixture<ManageUserRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageUserRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageUserRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
