import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { SmartTableService } from '../../@core/mock/smart-table.service';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { PageService } from '../page-service';
import { ToastrService } from '../../shared/toastr/toastr.service';
import { ManageUserService } from '../../../assets/services/manage_user_registration.service';
import { ManageUserModel } from './data-model/manage-user-model';
import { environment } from '../../../environments/environment';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { MailService } from '../../auth/mail.service';

/**
 * @title Table with selection
 */
@Component({
  selector: 'exp-manage-user-registration',
  templateUrl: './manage-user-registration.component.html',
  styleUrls: ['./manage-user-registration.component.scss']
})
export class ManageUserRegistrationComponent implements OnInit {
  private static TAG = 'ManageUserRegistration';
  registerUserList: any;
  userStatusList: any;
  finalData: any;
  searchKey: string;
  emailData = [];
  domainList: any;
  appUrl = environment.appUrl; 
  private userModule: ManageUserModel;
  private manageUserService: ManageUserService;
  constructor(private service: SmartTableService, private pageService: PageService,
    private toastrService: ToastrService, private mailService: MailService) {
    // Intializing the service
    this.manageUserService = new ManageUserService(this);
    // Intializing the login model
    this.userModule = new ManageUserModel();
  }

  manageUserData: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = ["select", "name", "email", "phone", "employeeId", "role", "domain", "status", "Comments"];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  disabled: false;
  buttonDisabled: boolean;

  ngOnInit(): void {
    // this.getUserDetails();
    // const url = environment.gigApp.manageUser;
    // this.manageUserService.post(ManageUserModel.ManageUser, url);
    // getUserDetails() {;
    this.finalData = [];
    this.pageService.getRegisteredUserList().subscribe(result => {
      if (result) {
        this.registerUserList = result;
        for (let i = 0; i <= this.registerUserList.length - 1; i++) {
          if (this.registerUserList[i].role != 'Admin') {
            this.finalData.push({
              "_id": this.registerUserList[i]._id,
              "name": this.registerUserList[i].name,
              "email": this.registerUserList[i].email,
              "employeeid": this.registerUserList[i].employeeid,
              "phone": this.registerUserList[i].phone,
              "role": this.registerUserList[i].role,
              "domain": this.registerUserList[i].domain,
              "status": "",
              "comments": "",
              "active": "",
              "userStatusTabeleId": "",
              "domainName": []
            });
          }
        }

        console.log(this.finalData);
        this.pageService.getdomainlist().subscribe(result => {
          if (result) {
            console.log(result);
            this.getDomain(result);
          }
        }, error => {
          console.error('Error logging in : ', error);
        });
        // this.manageUserData = new MatTableDataSource(this.finalData);
      }
    }, error => {
      console.error('Error logging in : ', error);
    });
  }

  getDomain(domainList: any) {
    for (let i = 0; i <= this.finalData.length - 1; i++) {
      for (let j = 0; j <= domainList.length - 1; j++) {
        let domainNames = "";
        let domainId = this.finalData[i].domain;
        for (let id of domainId) {
          if (domainList[j].domainId === id) {
            if (domainNames === "") {
              domainNames = domainList[j].domainName
            } else {
              domainNames = domainList[j].domainName + ' '+ domainNames
            }
            this.finalData[i].domainName.push(domainNames);
          }
        }
      }
    }
    console.log(this.finalData);
    this.userStatus();
  }

  userStatus() {
    this.pageService.getUserRegistrationStatus().subscribe(result => {
      if (result) {
        this.userStatusList = result;
        for (let i = 0; i <= this.userStatusList.length - 1; i++) {
          for (let j = 0; j <= this.finalData.length - 1; j++) {
            if (this.userStatusList[i].userId === this.finalData[j]._id) {
              this.finalData[j].userStatusTabeleId = this.userStatusList[i]._id;
              this.finalData[j].status = this.userStatusList[i].regStatus;
              this.finalData[j].comments = this.userStatusList[i].regComments;
              this.finalData[j].active = this.userStatusList[i].regStatus;
            }
          }
        }
        this.manageUserData = new MatTableDataSource(this.finalData);
        this.ngAfterViewInit();
      }
    }, error => {
      console.error('Error logging in : ', error);
    });
  }

  /**
   * filter table start
   */
  applyFilter(value: String) {
    // const filterValue = (event.target as HTMLInputElement).value;
    this.manageUserData.filter = value.trim().toLowerCase();
  }
  /**
   * end
   */

  public status = [
    { value: 'accept', viewvalue: 'Accept' },
    { value: 'reject', viewvalue: 'Reject' },
    { value: 'onhold', viewvalue: 'Onhold' },
  ];
  /**
   * update selected users list start
   */
   async updateSelectedUsersList() {
    if (this.selection.selected.length > 0) {
      const tableData = this.selection.selected;
      let array = [];
      let date = new Date().getTime();
      for (let i = 0; i < tableData.length; i++) {
        array.push({
          "userId": tableData[i]._id,
          "regStatus": tableData[i].status,
          "regComments": tableData[i].comments,
          "createdDate": date,
          "lastUpdateDate": date,
        });
      }
       await this.pageService.manageUserRegistration(array).subscribe(res => {
        if (res) {
          console.log("res",res);
          this.toastrService.show('success', res.count + " Record", ' Updated successfully');
         // this.updateTableData(array);
          //array = null;
          this.selection.clear();
        }
      });
      await this.pageService.getAdminApprovedusers(array).subscribe(res => {
        console.log("res",res);
        console.log("array",array);
        for (let i = 0; i < array.length; i++) {
          for (let j = 0; j < res.length; j++) {
            if (array[i].userId == res[i]._id) {
              this.emailData.push({
                "_id": array[i].userId,
                "email": res[i].email,
                "status": array[i].regStatus,
                "comment": array[i].regComments,
              });
            }
          }}

          console.log("res",this.emailData);
          this.pageService.emailService(this.emailData).subscribe(result => {
            console.log(result);
          });
      });

    } else {
      this.toastrService.show('danger', "failed", 'please check the table');
    }
  }
  /**
   * end
   */

  onServiceRunning(serviceRunning: boolean, passbackKey: string) {
    console.log(ManageUserRegistrationComponent.TAG + passbackKey + ' service running = ' + serviceRunning);
  }

  onServiceResponse(serviceResponse: any, passbackKey: string) {
    console.log(ManageUserRegistrationComponent.TAG + passbackKey + 'service response = ' + JSON.stringify(serviceResponse));


    switch (passbackKey) {
      case ManageUserModel.ManageUser:

        this.userModule.setManageUserResponse(serviceResponse);
        if (this.userModule.manageUserValid) {
          this.manageUserData = new MatTableDataSource(serviceResponse);
          // this.listDatas = serviceResponse.invoice_list;
          // this.listDatas = new MatTableDataSource(this.listDatas);
          // this.listDatas.sort = this.sort;
          // this.listDatas.paginator = this.paginator;
        }
        //  this.commonService.hideLoader();
        break;
    }
  }

  onServiceError(serviceError: any, passbackKey: string) {
    console.log(ManageUserRegistrationComponent.TAG + passbackKey + 'service error = ' + JSON.stringify(serviceError));
    switch (passbackKey) {
      case ManageUserModel.ManageUser:
        if (serviceError.statusText === 'Unknown Error') {
          // this.navCtrl.navigateForward('/menu/login');
        }
        break;
    }
  }

  // DATA TABLE control start
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.manageUserData !== undefined) {
      const numSelected = this.selection.selected.length;
      const numRows = this.manageUserData.data.length;
      return numSelected === numRows;
    }
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.manageUserData.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
  // DATA TABLE END

  ngAfterViewInit() {
    if (this.manageUserData !== undefined) {
      this.manageUserData.sort = this.sort;
      this.manageUserData.paginator = this.paginator;
    }
  }

  /**
   * Update the table data
   * Start 
   */

  updateTableData(updatedResultData: any) {
    console.log("this.manageUserData", this.manageUserData);
    console.log("Data", updatedResultData);
    console.log("this.finalData", this.finalData);
    for (let i = 0; i <= this.finalData.length; i++) {
      for (let j = 0; j <= updatedResultData.length; j++) {
      console.log("Data[0]", updatedResultData[j].userId);
        if (updatedResultData[j].userId == this.finalData[i]._id) {
          this.finalData[i]._id = updatedResultData[j].userId;
          this.finalData[i].userStatusTabeleId = updatedResultData[j].userId;
          this.finalData[i].status = updatedResultData[j].regStatus;
          this.finalData[i].comments = updatedResultData[j].regComments;
          this.finalData[i].active = updatedResultData[j].regStatus;
          this.emailData.push({
            "_id": this.finalData[i]._id,
            "email": this.finalData[i].email,
            "status": this.finalData[i].status,
            "comment": this.finalData[i].comments,
            "appUrl": this.appUrl
          });
        }
      }
    }
    console.log(this.emailData);
    
    
    this.manageUserData = new MatTableDataSource(this.finalData);
    this.ngAfterViewInit();
  }
  //  Update the table data end

  /**
   * @MailService for ADMIN
   *  
   */

  clearSelection()
  {
    this.selection.clear();
  }


  emailService(userList: any) {
    this.pageService.emailService(userList).subscribe(result => {
      if (result) {
        console.log(result);
      }
    }, err => {
      console.error('Error logging in : ', err);
      this.toastrService.show('danger', 'Error', 'Error');
    });
  }
}
