export class ManageUserModel {

    public static ManageUser = 'ManageUser model';

    constructor() { }

    // BLOCK: ManageUserModel Params generator

  /**
   * Sets and gets the ManageUser params
   *
   * @returns The ManageUser parameters - request body
   */
    public setAndGetManageUserParams() {

        this.resetManageUserResponse();

        const manageUserParams = {
        };

        return manageUserParams;

    }

    // BLOCK: ManageUser Response

  private ManageUserResponse: any;


  /**
   * Resets the ManageUser response
   */
  private resetManageUserResponse() {
    this.ManageUserResponse = null;
  }

  /**
   * Sets the ManageUser response
   *
   * @param ManageUser Response The ManageUser response
   */
  public setManageUserResponse(ManageUserResponse: any) {
    this.ManageUserResponse = ManageUserResponse;
  }

  /**
   * Gets the login valid status
   *
   * @returns The login valid status
   */
  get manageUserValid(): boolean {
    console.log('model user');
    let manageUserResponse = this.ManageUserResponse;
    manageUserResponse = manageUserResponse;
    if (manageUserResponse) {
      return manageUserResponse;
    } else {
      return manageUserResponse;
    }
  }

};