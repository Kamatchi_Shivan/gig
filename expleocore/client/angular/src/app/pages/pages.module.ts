import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Injector } from '@angular/core';
import { NbMenuModule, NbSelectModule, NbCardModule, NbInputModule, NbPopoverModule, NbListModule } from '@nebular/theme';
import { SmartTableService } from '../@core/mock/smart-table.service';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ProjectPublishingModule } from './projects/project-publishing/project-publishing.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ProjectsComponent } from '../pages/projects/projects.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { ManageUserRegistrationComponent } from './manage-user-registration/manage-user-registration.component';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ProjectReviewModule } from './projects/project-review/project-review.module';
import { ProjectDetailsModule } from './projects/project-details/project-details.module';
import { Singleton } from '../../assets/singleton/singleton';
import { MatIconModule } from '@angular/material/icon';
import { ProjectSuccessComponent } from './projects/project-success/project-success.component';
import { SubmitNominationModule } from './projects/submit-nomination/submit-nomination.module';
import { PlpModule } from './projects/plp/plp.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';

import { PMReviewNominationModule } from './projects/pm-review-nomination/pm-review-nomination.module';
import { LMReviewNominationModule } from './projects/lm-review-nomination/lm-review-nomination.module';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MattableComponent } from './projects/mattable/mattable.component';


import { NumericDirective } from '../shared/numeric.directive';

@NgModule({
  imports: [
    FormsModule,
    FlexLayoutModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ProjectPublishingModule,
    ProjectReviewModule,
    PMReviewNominationModule,
    LMReviewNominationModule,
    ProjectDetailsModule,
    SubmitNominationModule,
    PlpModule,
    NbInputModule,
    NbSelectModule,
    NbCardModule,
    NbPopoverModule,
    NbListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDividerModule,
    MatListModule
  ],
  declarations: [
    PagesComponent,
    ProjectsComponent,
    ManageUserRegistrationComponent,
    ProjectSuccessComponent,
    MattableComponent
  ],
  providers:[
    SmartTableService,
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  
})
export class PagesModule {
  constructor(public injector: Injector) {
    // Global singleton injector initialization
    Singleton.injector= injector;
  }
}
