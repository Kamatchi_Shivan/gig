import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { RootService } from '../root.service';

@Component({
  selector: 'exp-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <exp-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </exp-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu = MENU_ITEMS;
  constructor(private service: RootService) { 
    
  }

  ngOnInit() {
    const role = this.service.mdpic;
    console.log(role);
    this.menu = MENU_ITEMS.filter(el => el.roles.includes(role.role));
    console.log('menu', this.menu);
  }
}
