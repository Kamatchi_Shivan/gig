import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, empty } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { from } from 'rxjs';
import { Area, Domain } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  apiUrl = environment.apiUrl;
  
  i: number;
  constructor(private http: HttpClient) { }

  /**
   * manage user table update service start
   * @param selected row data
   * @method put
   */
  manageUserRegistration(data): Observable<any> {
    const user = data;
    return this.http.put(`${this.apiUrl}v1/ManageUserRegistration/update/UserRegistrationStatus`, user);
  }
  getAdminApprovedusers(data): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/getadminapprovedusers`, data);
  }

  
  /**
   * end
   */

  /**
  * users details service start
  * @method get
  */
  getRegisteredUserList(): Observable<any> {
    return this.http.get(`${this.apiUrl}v1/ManageUserRegistration/get/Register`);
  }

  getdomainlist(): Observable<Domain> {
    return this.http.get(`${this.apiUrl}v2/registration/find/Domain`)
      .pipe(map((res: Domain) => {
        return res;
      }));
  }

  /**
   * users status service start
   * @method get
   */
  getUserRegistrationStatus(): Observable<any> {
    return this.http.get(`${this.apiUrl}v1/ManageUserRegistration/getList/UserRegistrationStatus`);
  }

  getarealist(): Observable<Area> {
    return this.http.get(`${this.apiUrl}v1/findarea/AreaofInterest`)
      .pipe(map((res: Area) => {
        return res;
      }));
  }

  sendprojectdetails(projectdata): Observable<any> {
    console.log(projectdata.projectName.value);
    const projectdetails = {
      'projectName': projectdata.projectName.value,
      'projectArea': projectdata.projectArea.value,
      'projectRefNo': projectdata.workOrderNumber.value,
      'customerName': projectdata.customer.value,
      'projectDescription': projectdata.projectDescription.value,
      'scopeofActivity': projectdata.scopeOfActivity.value,
      'input': projectdata.inputForProject.value,
      'tools': projectdata.tools.value,
      'deliverables': projectdata.deliverables.value,
      'startDate': projectdata.startDate.value,
      'endDate': projectdata.endDate.value,
      'budget': projectdata.projectBudget.value,
      'workPackageStatus': projectdata.workPackageStatus.value,
      'constraints': projectdata.constrains.value,
      'acceptanceCriteria': projectdata.acceptanceCriteria.value,
    };
    console.log(projectdetails);
    return this.http.post(`${this.apiUrl}v1/projectdashboard/insert/ProjectDetails`, projectdetails);
  }

  sendskilldetails(skilldata, projectid): Observable<any> {
    let result;
    var i: number;
    var data = [];
    for (i = 0; i < skilldata.skills.length; i++) {
      const skilldetails = {
        'projectId': projectid,
        'projectRefNo': skilldata.workOrderNumber,
        'skillId': i + 1,
        'skillName': skilldata.skills[i].name,
        'skillRating': skilldata.skills[i].rating,
        'skillWeightage': skilldata.skills[i].weightage,
      };
      data.push(skilldetails);
    }
    return this.http.post(`${this.apiUrl}v1/projectskill/insert/ProjectSkill`, data);
  }


  fileinfo(filedata): Observable<any> {
    return this.http.post(`${this.apiUrl}v2/registration/fileinfo/FileInfo`, filedata);
  }
  projectAvailability(project): Observable<any> {
    const projectname = {
      'projectName': project
    };
    return this.http.post(`${this.apiUrl}v1/projectAvailability`, projectname);
  }
  workorderAvailability(workorder): Observable<any> {
    console.log(workorder);
    const workorderno = {
      'projectRefNo': workorder
    };
    console.log(workorderno);
    return this.http.post(`${this.apiUrl}v1/workorderAvailability`, workorderno);
  }

  setPrevileges(id, role, projectid): Observable<any> {
    if ("Consultant" == role) {
      const userrole = {
        'userId': id,
        'projectId': projectid,
        'isConsultant': true,
      };
      return this.http.post(`${this.apiUrl}v1/projectprivilege/insert/ProjectPrivileges`, userrole);
    }
    else if ("Reviewer" == role) {
      const userrole = {
        'userId': id,
        'projectId': projectid,
        'isReviewer': true,
      };
      return this.http.post(`${this.apiUrl}v1/projectprivilege/insert/ProjectPrivileges`, userrole);
    }
    else if ("Project Manager" == role) {
      const userrole = {
        'userId': id,
        'projectId': projectid,
        'isProjectManager': true,
      };
      return this.http.post(`${this.apiUrl}v1/projectprivilege/insert/ProjectPrivileges`, userrole);
    }
    else if ("Line Manager" == role) {
      const userrole = {
        'userId': id,
        'projectId': projectid,
        'isLineManager': true,
      };
      return this.http.post(`${this.apiUrl}v1/projectprivilege/insert/ProjectPrivileges`, userrole);
    }
  }

  /**
   * get reviewer role start
   * @method get
   */

  getReviewerRole(): Observable<any> {
    return this.http.get(`${this.apiUrl}v1/Colloborators/getReviewer`);
  }
  // end

  /**
   * get shortlist nominees start
   */
  getShortListNominees(projectId: any): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/Colloborators/GetSelectedList/PMReviewStatus`, projectId);
  }
  // end

  /**
   * short details for userslist
   * @param userList 
   * @method post
   */
  getShortListUserList(userList: any): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/Colloborators/GetShortListUsersData/Register`, userList);
  }
  // end

  /**
   * save collaborators start
   * @param formData 
   * @method post
   */

  savecollaborators(formData: any) {
    return this.http.post(`${this.apiUrl}v1/collaborators/insert/Collobarotors`, formData);
  }
  // end

  /**
   * Email service for users based on the status by admin
   * start
   */

  emailService(userList: any): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/EmailService/sendEmailToUsers`, userList);
  }

  /**
   * end
   */

  /**
   * Project Privileges start
   */
  projectPrivileges(userList: any) {
    return this.http.post(`${this.apiUrl}v2/Project/ProjectPrivileges`, userList);
  }
  // end
  projectMoreDetails(projectid): Observable<any> {
    console.log(projectid);
    const projectinput = {
      'projectId': projectid,
      'id': projectid,
    };
    return this.http.post(`${this.apiUrl}v1/getProjectDetails`, projectinput);
  }

  saveMomforproject(projectid, MoM): Observable<any> {
    const projectMOM = {
      'projectId': projectid,
      'MOM': MoM
    };
    return this.http.post(`${this.apiUrl}v1/projectMOM/insert/ProjectMom`, projectMOM);
  }

  saveCloserMOM(projectid, closermom): Observable<any> {
    const closerMOM = {
      'projectId': projectid,
      'ClosureMOM': closermom
    };
    return this.http.post(`${this.apiUrl}v1/setCloserMOM`, closerMOM);
  }

  getAllMOMs(projectid): Observable<any> {
    console.log(projectid);
    const projectMOM = {
      'projectId': projectid,
    };
    return this.http.post(`${this.apiUrl}v1/getProjectMOM`, projectMOM);
  }
  getSkillList(projectid): Observable<any> {
    const projectID = {
      'projectId': projectid,
    };
    return this.http.post(`${this.apiUrl}v1/getProjectSkills`, projectID);
  }

  getprojectid(userid): Observable<any> {
    const userID = {
      'userId': userid,
    };
    return this.http.post(`${this.apiUrl}v1/getProject/Id`, userID);
  }

  getprojectlistdetails(proIdlist): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/projectDashbordInfo`, proIdlist);
  }

  setBasicinfo(projectid, projectdata): Observable<any> {
    const projectdetails = {
      'projectId': projectid,
      'projectName': projectdata.projectName,
      // 'projectArea': projectdata.projectArea,
      'projectRefNo': projectdata.workOrderNumber,
      'customerName': projectdata.customer,
      'projectDescription': projectdata.projectDescription,
      'scopeofActivity': projectdata.scopeOfActivity,
      'input': projectdata.inputForProject,
      'tools': projectdata.tools,
      'deliverables': projectdata.deliverables,
      'startDate': projectdata.startDate,
      'endDate': projectdata.endDate,
      'budget': projectdata.projectBudget,
      'workPackageStatus': projectdata.workPackageStatus,
      'constraints': projectdata.constrains,
      'acceptanceCriteria': projectdata.acceptanceCriteria,
    };
    return this.http.post(`${this.apiUrl}v1/setprojectdetails`, projectdetails);
  }

  deleteSkillSet(proIdlist): Observable<any> {
    const proID = {
      'projectId': proIdlist,
    };
    return this.http.post(`${this.apiUrl}v1/deleteSkillset`, proID);
  }

  getAllProjectlist(): Observable<any> {
    return this.
      http.get(`${this.apiUrl}v1/allProjectInfo`);
  }
  deleteSkillinfo(proIdlist): Observable<any> {
    const proID = {
      'projectId': proIdlist,
    };
    return this.http.post(`${this.apiUrl}v1/deleteSkillInfo`, proID);
  }

  getLMList(proid): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/getLM_List`,proid);
  }

  sendNomineeinfo(nomindata): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/nomination/insert`, nomindata);
  }

  sendNomineeskill(ids, skilllist): Observable<any> {
    let result;
    var i: number;
    var data = [];
    for (i = 0; i < skilllist.length; i++) {
      const skilldetails = {
        'userId': ids.userId,
        'projectId': ids.projectId,
        'skillId': i + 1,
        'skillName': skilllist[i].name,
        'skillRating': skilllist[i].rating,
      };
      data.push(skilldetails);
    }
    return this.http.post(`${this.apiUrl}v1/nominationskill/insert`, data);
  }

  setNominatorResume(filedata): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/setNominatorResume`, filedata);
  }

  nomineecheck(filedata, userid): Observable<any> {
    const projectID = {
      'userId': userid,
      'projectId': filedata
    };
    return this.http.post(`${this.apiUrl}v1/nomination/usercheck`, projectID);
  }
  deleteFileinfo(userid): Observable<any> {
    const userID = {
      'userId': userid,
    };
    return this.http.post(`${this.apiUrl}v1/deleteFileInfo`, userID);
  }

  getPMInfo(proId): Observable<any> {
    const projectID = {
      'projectId': proId
    };
    return this.http.post(`${this.apiUrl}v1/getPM_List`, projectID);
  }

  sendConsultant(userinfo, proid, lmInfo): Observable<any> {
    const htmltext = `
    <h4>Hi,${userinfo.name}</h4>
    <h4>Your nomination for the project ${proid.name} has been received. It is current under line manager ${lmInfo.name} review.</h4>
    <h4><br/><br/>Regards<br/>Administrator<br/>Gig Economy.</h4>`;

    const setmail = {
      'content': htmltext,
      'header': '[Gig] The Project successfully nominated',
      'to': userinfo.email
    };
    return this.http.post(`${this.apiUrl}v1/sendNomination`, setmail);
  }

  sendLMMessage(lminfo, userinfo, projectId): Observable<any> {
    const htmltext = `
    <h4>Hi,${lminfo.name}</h4>
    <h4>The Consultant ${userinfo.name} has submit his nomination for the project ${projectId.name}. Please review his nomination.</h4>
    <h4><br/><br/>Regards<br/>Administrator<br/>Gig Economy.</h4>`;

    const setmail = {
      'content': htmltext,
      'header': '[Gig] Project Nomination',
      'to': lminfo.email
    };
    return this.http.post(`${this.apiUrl}v1/sendNomination`, setmail);
  }

  sendPMMessage(pminfo, nomininfo, projectId, userinfo): Observable<any> {
    const htmltext = `
    <h4>Hi,${pminfo[0].name}</h4>
    <h4>The Consultant ${userinfo.name} has submit his nomination for the project ${projectId.name}. His budget quote for the project is ${nomininfo.budget}.
     His nomination is currently reviewed by his line manager.<br/>
    These are queries he has listed for the project. <br/> ${nomininfo.comments}<br/><br/>Regards<br/>Administrator<br/>Gig Economy.</h4>  `;

    const setmail = {
      'content': htmltext,
      'header': '[Gig] Project Nomination',
      'to': pminfo[0].email
    };
    return this.http.post(`${this.apiUrl}v1/sendNomination`, setmail);
  }
  /**MS Created Start**/
  msCreatedCons(consultants){
    return this.http.post(`${this.apiUrl}v1/sendConsultantMail`, consultants);
  }
  msCreatedReviewer(reviewer){
    return this.http.post(`${this.apiUrl}v1/reviewerMail`, reviewer);
  }
  /**MS Created End**/

  /**MS Updated**/
  msUpdateCons(name,email,status,actionPerformer){
    const htmltext = `
    <h4>Hi,${name}</h4>
    <h4>The Milestone status has been updated as ${status} and actions as <action> for the project ${actionPerformer}. Please check and continue on the work.</h4>  
    <br>
    <br>
    <p>Regards</p>
    <p>Administrator</p>
    <p>Gig Economy.</p>
    `;

    const setmail = {
      'content': htmltext,
      'header': '[Gig] Project Nomination',
      'to': email
    };
    return this.http.post(`${this.apiUrl}v1/sendNomination`, setmail);
  }
  msUpdateReviewer(name,email,status,actionPerformer){
    const htmltext = `
    <h4>Hi,${name}</h4>
    <h4>The Milestone status has been updated as ${status} and actions as <action> for the project ${actionPerformer}. Please check and continue on the work.</h4>  
    <br>
    <br>
    <p>Regards</p>
    <p>Administrator</p>
    <p>Gig Economy.</p>
    `;

    const setmail = {
      'content': htmltext,
      'header': '[Gig] Project Nomination',
      'to': email
    };
    return this.http.post(`${this.apiUrl}v1/sendNomination`, setmail);
  }
  msUpdatePM(name,email,status,actionPerformer){
    const htmltext = `
    <h4>Hi,${name}</h4>
    <h4>The Milestone status has been updated as ${status} and actions as <action> for the project ${actionPerformer}. Please check and continue on the work.</h4>  
    <br>
    <br>
    <p>Regards</p>
    <p>Administrator</p>
    <p>Gig Economy.</p>
    `;

    const setmail = {
      'content': htmltext,
      'header': '[Gig] Project Nomination',
      'to': email
    };
    return this.http.post(`${this.apiUrl}v1/sendNomination`, setmail);
  }
  /**MS Updated**/
  getNomineeList(projectID): Observable<any> {
    const proID = {
      'projectId': projectID
    };
    console.log(projectID);
    return this.http.post(`${this.apiUrl}v1/nomineeList`, proID);
  }

  getConsultant(proId): Observable<any> {
    const projectID = {
      'projectId': proId
    };
    return this.http.post(`${this.apiUrl}v1/getConsultantId`, projectID);
  }

  setReleaseNominee(statusinfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/releaseNominee/insert`, statusinfo);
  }
  setReleasestatus(Statusinfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/setApprovedstatus`, Statusinfo);
  }

  getLMApprovedNomineeList(projectID): Observable<any> {
    console.log("projectId3", projectID);
    const proID = {
      'projectId': projectID
    };
    return this.http.post(`${this.apiUrl}v1/lmApprovedNomineeList`, proID);
  }
  setPMReleaseNominee(statusinfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/pmrelease/insert`, statusinfo);
  }

  setPMReleasestatus(statusinfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/setPMApproved`, statusinfo);
  }

  setcurrentPMId(statusinfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/setConsultantApproved`, statusinfo);
  }
  getNomineeRatingSkills(projectID, userid): Observable<any> {
    console.log(projectID, userid);
    const proID = {
      'projectId': projectID,
      'userId': userid
    };
    return this.http.post(`${this.apiUrl}v1/getSkillsDetails`, proID);
  }

  checkSwitchRolevalid(statusinfo, selectedrole): Observable<any> {
    if ("Consultant" == selectedrole) {
      const userrole = {
        'userId': statusinfo._id,
        'isConsultant': true,
        'role': selectedrole
      };
      return this.http.post(`${this.apiUrl}v1/getCheckswitchrole`, userrole);
    }
    else if ("Reviewer" == selectedrole) {
      const userrole = {
        'userId': statusinfo._id,
        'isReviewer': true,
        'role': selectedrole
      };
      return this.http.post(`${this.apiUrl}v1/getCheckswitchrole`, userrole);
    }
    else if ("Project Manager" == selectedrole) {
      const userrole = {
        'userId': statusinfo._id,
        'isProjectManager': true,
        'role': selectedrole
      };
      return this.http.post(`${this.apiUrl}v1/getCheckswitchrole`, userrole);
    }
    else if ("Line Manager" == selectedrole) {
      const userrole = {
        'userId': statusinfo._id,
        'isLineManager': true,
        'role': selectedrole
      };
      return this.http.post(`${this.apiUrl}v1/getCheckswitchrole`, userrole);
    }

  }

  sendPMapprovalMessage(pminfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/sendApprovedMail`, pminfo);
  }
  sendLMapprovalMessage(pminfo): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/sendApprovedMail`, pminfo);
  }

  schedularget(_id: any) {
    let obj = {
      _id: _id
    }
    return this.http.post(`${this.apiUrl}v1/GetSchedulerdata`, obj);
  }


  /**
   * scheduler service start
   * @method post 
   * @params 
   */

  schedulerSave(formData: any) {
    console.log(formData);
    return this.http.post(`${this.apiUrl}v1/Scheduler/`, formData)
  }

  /**
   * end
   */

  schedulerGetDeatils(projectInfo: any) {
    let obj = {
      'projectId': projectInfo._id,
      'workOrderNumber': projectInfo.projectRefNo
    }
    return this.http.post(`${this.apiUrl}v1/GetSchedulerList`, obj);
  }

  
  schedulertestGetDeatils(projectInfo: any) {
    let obj = {
      '_id': projectInfo
    }
    return this.http.post(`${this.apiUrl}v1/getprojectschdulerlist`, obj);
  }

  schedulercollaboratoruserDetails(projectInfo: any) {
    let obj = {
      '_id': projectInfo
    }
    return this.http.post(`${this.apiUrl}v1/schedulercollaboratoruserDetails`, obj);
  }


  /**
   * scheduler Update
   */
  schedularReviwerUpdate(formData: any) {
    return this.http.post(`${this.apiUrl}v1/schedular/ReviwerUpdate`, formData);
  }

  schedularPMUpdate(formData: any) {
    return this.http.post(`${this.apiUrl}v1/schedular/PMUpdate`, formData);
  }

  schedularConsultantUpdate(formData: any) {
    return this.http.post(`${this.apiUrl}v1/schedular/ConsultantUpdate`, formData);
  }

  getConsultantprivilage(project_id: any) {
    return this.http.post(`${this.apiUrl}v1/consultant/getconsultantforproject`,project_id);
  }
  SetConsultantprivilage(formData: any) {
    return this.http.post(`${this.apiUrl}v1/consultant/insert`, formData);
  }
  deleteProject(proIdlist): Observable<any> {
    const proID = {
      'projectId': proIdlist,
    };
    return this.http.post(`${this.apiUrl}v1/deleteProject`, proID);
  }
  setProjectStatus(formData: any) {
    return this.http.post(`${this.apiUrl}v1/setProjectStatus`, formData);
  
  }
  getReviewerInfo(proId): Observable<any> {
    const projectID = {
      'projectId': proId
    };
    return this.http.post(`${this.apiUrl}v1/getReviewer_List`, projectID);
  }


  CheckswitchroleConsultant(userid): Observable<any> {
    const Userid = {
      'UserId': userid
    };
    return this.http.post(`${this.apiUrl}v1/getCheckswitchroleConsultant`, Userid);
  }


  ProjectListForConsultant(proId): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/projectListForConsultant`, proId);
  }
  
  GetAllprojectNomination(userid): Observable<any> {
      return this.http.post(`${this.apiUrl}v1/getAllusernominationInfo`, userid);  
  }

  GetprojectNominationAccpeted(projectid): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/getprojectusernominationlist`, projectid);  
}
  
  GetAllprojectInfo(): Observable<any> {
    const proId =""
    return this.http.post(`${this.apiUrl}v1/getAllprojectInfo`, proId);
  }
  
  isCollabratorAvailable(data): Observable<any> {
    return this.http.post(`${this.apiUrl}v1/isCollabratorAvailable`, data);
  }

  getNominees(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getNominees`, data);
  }

  getNominatedUser(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getNominatedUser`, data);
  }

 
  getLMApprovedNominees(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getLMApprovedNominees`, data);
  }

  getLMApprovedUser(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getLMApprovedUser`, data);
  }

  getPMReview(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getPMReview`, data);
  }

  getLMReview(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getLMReview`, data);
  }

  getProjectPrivileges(data): Observable<any>{
    return this.http.post(`${this.apiUrl}v1/getProjectPrivileges`, data);
  }
  /**
   * end
   */
}
