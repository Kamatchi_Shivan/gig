import { NbMenuItem } from '../models/models';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Project Dashboard',
    icon: 'home-outline',
    link: '/pages/plp',
     home: true,
     roles:['Project Manager', 'Reviewer', 'Admin', 'Consultant', 'Line Manager'],
  },
  {
    title: 'Manage User Registration',
    icon: 'briefcase-outline',
    link: '/pages/manageUserRegistration',
    home: true,
    roles:['Admin'],
  },
  {
    title: 'Logout',
    icon: 'power-outline',
    link: '/auth',
    home: true,
    roles:['Project Manager', 'Reviewer', 'Admin', 'Consultant', 'Line Manager'],
  }
  
  // {
  //   title: 'Project Details',
  //   icon: 'briefcase-outline',
  //   link: '/pages/project-details',
  //   home: true,
  //   roles:['Project Manager'],
  // },
  // {
  //   title: 'Project Publishing',
  //   icon: 'briefcase-outline',
  //   link: '/pages/project-publishing',
  //   home: true,
  //   roles:['Project Manager'],
  // }
 
 
  
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Auth',
  //   icon: 'lock-outline',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
