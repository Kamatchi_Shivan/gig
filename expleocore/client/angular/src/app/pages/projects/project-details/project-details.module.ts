import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
//import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { 
          NbCardModule,
          NbButtonModule,
          NbIconModule,
          NbProgressBarModule,
          NbUserModule
        } from '@nebular/theme';


import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatListModule } from '@angular/material/list';

import { CommonModule } from '@angular/common';



import { ProjectDetailsComponent } from './project-details.component';
import { RatingComponent } from './rating/rating.component';
import { SchedulerComponent } from './scheduler/scheduler.component';
import { CollaboratorsComponent } from './collaborators/collaborators.component';
import { CollaboratorsModelComponent } from './collaborators/collaborators-model/collaborators-model.component';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectFilterModule } from 'mat-select-filter';
import { SchedulerModifyDetailsComponent } from './scheduler/scheduler-modify-details/scheduler-modify-details.component';
import { MatDialogModule } from '@angular/material/dialog';

const materialModules = [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatChipsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatListModule,
    MatAutocompleteModule,
    MatIconModule,
    MatCheckboxModule,
    MatDialogModule
  ];
  
  @NgModule({
    imports: [
      NbCardModule,
      NbIconModule,
      NbButtonModule,
      NbUserModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      NbProgressBarModule,
      ...materialModules,
      RouterModule,
     // AngularMultiSelectModule,
     MatSelectFilterModule
    ],
    declarations: [
        ProjectDetailsComponent,
        RatingComponent,
        SchedulerComponent,
        CollaboratorsComponent,
        CollaboratorsModelComponent,
        SchedulerModifyDetailsComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
    ]
  })
  export class ProjectDetailsModule {
      
  }