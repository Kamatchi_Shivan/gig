import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { RootService } from '../../../../root.service';
import { PageService } from '../../../page-service';
import { ToastrService } from '../../../../shared/toastr/toastr.service';

@Component({
  selector: 'exp-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss']
})
export class SchedulerComponent implements OnInit {

  projectId: any;
  isConsultant: boolean;
  isPM: boolean;
  isLM: boolean;
  isReviewer: boolean;
  loggedUserRole: string;
  projectInfo: any;
  schedulerData: MatTableDataSource<any>;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  datasource: any;
  constructor(private activateRoute: ActivatedRoute, private router: Router,
    private rootservice: RootService, private service: PageService, private toastrService: ToastrService) { }
  columnsToDisplay = [];
  displaycolums = [];
  //displayedColumns: string[] = this.columnsToDisplay;
  displayedColumns: string[] = [];
  ngOnInit(): void {
    this.loggedUserRole = this.rootservice.mdpic.role.toLowerCase().replace(" ", "_");
    switch (this.loggedUserRole) {
      case 'consultant':
        this.isConsultant = true;
        break;
      case 'project_manager':
        this.isPM = true;
        break;
      case 'reviewer':
        this.isReviewer = true;
        break;
      case 'line_manager':
        this.isLM = true;
        break;
    }
    this.projectInfo = this.rootservice.getProjectInfo();
    this.schedulerGetDeatils(this.projectInfo);
    this.activateRoute.queryParams.subscribe(params => {
      this.projectId = params['pid'];
    })
    this.ngAfterViewInit();
  }

  ngAfterViewInit() {
    if (this.schedulerData !== undefined) {
      this.schedulerData.sort = this.sort;
      this.schedulerData.paginator = this.paginator;
    }
  }

  setHeader(column: string) {
    return column
      .replace(/([A-Z])/g, function (match) {
        return " " + match;
      })

      .replace(/^./, function (match) {
        return match.toUpperCase();
      });
  }

  toggleRow(tableData: any) {
    this.router.navigate(['/pages/scheduler-modify-details-component'], { queryParams: { 'project': this.projectId , 'typemethod' : 'edit'}, skipLocationChange: true });
  }

  backToProjectDeatils() {
    this.router.navigate(['/pages/project-details'], { queryParams: { id: this.projectId } });
  };
  
  createScheduler() {
    this.router.navigate(['/pages/scheduler-modify-details-component'], { queryParams: { 'project': this.projectId , 'typemethod' : 'new' }, skipLocationChange: true });
  }

  schedulerGetDeatils(projectInfo: any) {
    this.service.schedulerGetDeatils(projectInfo).subscribe(result => {
      if (typeof (result) === 'undefined' && this.isPM) {
        this.createScheduler();
      } else {
        console.log('result', result);
        this.rootservice.setSchdularList(result);
        this.displayedColumns.push('Edit');
        this.schedulerData = new MatTableDataSource(<any>result['data']);

        var keysIndex = 0;
        var maxIndex;

        for(var index in result['data']){
          var keys = Object.keys(result['data'][index]);
          if(keysIndex < keys.length){
            keysIndex = keys.length
            maxIndex = result['data'][index];
          }
        }

        for (let v in maxIndex) {
          if (v !== "_id") {
            this.displayedColumns.push(v);
            this.columnsToDisplay = this.displayedColumns;
          }
        }
        console.log('displaycolums', this.displayedColumns);
        this.toastrService.show('success', 'Success', 'Scheduler details fetching successfully');
      }
    }, error => {

    })
  }

}
