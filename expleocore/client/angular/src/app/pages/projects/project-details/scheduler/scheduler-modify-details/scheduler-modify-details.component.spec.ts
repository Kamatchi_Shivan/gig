import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulerModifyDetailsComponent } from './scheduler-modify-details.component';

describe('SchedulerModifyDetailsComponent', () => {
  let component: SchedulerModifyDetailsComponent;
  let fixture: ComponentFixture<SchedulerModifyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulerModifyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulerModifyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
