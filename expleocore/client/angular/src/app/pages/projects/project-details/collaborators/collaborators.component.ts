import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CollaboratorsModelComponent } from './collaborators-model/collaborators-model.component';


@Component({
  selector: 'exp-collaborators',
  templateUrl: './collaborators.component.html',
  styleUrls: ['./collaborators.component.scss']
})
export class CollaboratorsComponent implements OnInit {
  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
    this.openDialog();
    this.loadbodyclass();
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-collaborators');
  }
  /**
   * collaborators dialog start
   */

  openDialog(): void {
    const dialogRef = this.dialog.open(CollaboratorsModelComponent, {
      width: '500px',
      hasBackdrop: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  //end
}
