import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaboratorsModelComponent } from './collaborators-model.component';

describe('CollaboratorsModelComponent', () => {
  let component: CollaboratorsModelComponent;
  let fixture: ComponentFixture<CollaboratorsModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaboratorsModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaboratorsModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
