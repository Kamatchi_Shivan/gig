import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PageService } from '../../../../page-service';
import { ToastrService } from '../../../../../shared/toastr/toastr.service';
import { RootService } from '../../../../../root.service';

@Component({
  selector: 'exp-collaborators-model',
  templateUrl: './collaborators-model.component.html',
  styleUrls: ['./collaborators-model.component.scss']
})
export class CollaboratorsModelComponent implements OnInit
{
  reviewerData: any;
  public reviewerArr: Array<any> = [];
  collaborators: FormGroup;
  pmShortListData: any;
  usersShortList: any;
  shortlistUsersArr: any;
  shortlistUsers: any;
  selectedStates: any;
  reviewerListData: any;
  filteredListReviewer: any;
  projectId: any;
  loggedUserinfo;
  allMOMs = "";
  reviewer_selection = true;
  reviewer_selected_name :any;

  userinfo: any;
  constructor(private router: Router, private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CollaboratorsModelComponent>, private service: PageService,
    private toastrService: ToastrService, private activateRoute: ActivatedRoute, private rootService: RootService) { }





  ngOnInit(): void {
    this.userinfo = this.rootService.mdpic;
    console.log(this.userinfo);
    this.activateRoute.queryParams.subscribe(params => {
      this.projectId = params['id'];
      console.log("---->",this.projectId);
    })


    this.collaborators = this.formBuilder.group({
      reviewer: [null, Validators.required],
      // consultant: [null, Validators.required],
    });


    this.service.getReviewerRole().subscribe(result => {

      console.log('resultrole : ', result);
      if (result.length === 0) {
        this.toastrService.show('danger', " Reviewer", ' not avaiable in gig platform');
      }
      this.reviewerData = result;
      this.reviewerArr = [];
      for (let i = 0; i <= this.reviewerData.length - 1; i++) {
        this.reviewerArr.push({
          id: this.reviewerData[i]._id,
          name: this.reviewerData[i].name,
        });
      }

      
      setTimeout(() => {
        this.getShortListReviewer();
        this.getShortListNominees();
        
      }, 1000);

      console.log('Success : ', this.reviewerArr);

      this.filterFuntcion();
    }, error => {
      console.error('Error logging in : ', error);
    });
  }

  filterFuntcion() {
    this.filteredListReviewer = this.reviewerArr.slice();
  }


  cancelModel() {
    this.dialogRef.close();
    // this.router.navigate(['/pages/project-details'], { queryParams: { id: this.projectId } } );
  };
  /**
   * get nominees shortlisted datas start
   */
  async getShortListReviewer() {
    let obj = {
      projectId: this.projectId
    }
    await this.service.getConsultantprivilage(this.projectId).subscribe(result => {
      this.reviewerListData = result;
    for (let i = 0; i < this.reviewerListData.length; i++) {
      for (let u = 0; u < this.reviewerData.length; u++) {
      if(this.reviewerData[u]._id == this.reviewerListData[i].reviewer)
      {
          this.reviewer_selected_name =this.reviewerData[u].name;
          this.reviewer_selection = false;
      }
    }
    }
    });
  }
  /**
   * end
   * end
   */
  /**
   * get nominees shortlisted datas start
   */
  async getShortListNominees() {
    let obj = {
      projectId: this.projectId
    }
    await this.service.getShortListNominees(obj).subscribe(async result => {
      console.log("getShortListNominees--", result);
      this.pmShortListData = result;

      this.getuserlist(result);


    });
  }
  /**
   * end
   * end
   */
  /**
   * get short list users from user details
   * @param userList 
   */

  // ngAfterViewInit(){
  //   this.getShortListNominees();
  // }
  async getuserlist(userdata) {
    console.log("result", userdata);
    await this.service.getShortListUserList(userdata).subscribe(result => {
      this.usersShortList = result;
      console.log("result", result);
      const userinfo = result;
      localStorage.setItem('getuserlist', JSON.stringify(result));

      for (let i = 0; i < userinfo.length; i++) {
        console.log("this.userinfo[i].name", userinfo[i].name);

        this.allMOMs += `${i+1}. ${userinfo[i].name}<br/>`
      }
      console.log("--->Moms",this.allMOMs);
      document.getElementById('consultantList').innerHTML = this.allMOMs;

    });

  }



  /**
   * collaborators form save start
   */

  collaboratorsSave(formValue: any) {
    console.log("formValue---", formValue);
    const date = new Date().getTime();
    var projectdata = [];
    this.loggedUserinfo = JSON.parse(localStorage.getItem('getuserlist'));
    for (let i = 0; i < this.loggedUserinfo.length; i++) {
      const userdetails = {
        "_id": this.loggedUserinfo[i]._id,
      };
      projectdata.push(userdetails);
    }

    const formData = {
      "projectId": this.projectId,
      "reviewer": formValue.reviewer.id,
      "consultant": projectdata,
      "ProjectManager": this.userinfo._id,
      "createdDate": date,
      "lastUpdateDate": date,
    }

    console.log("formData--", formData);
    this.service.savecollaborators(formData).subscribe(result => {
      console.log("result1--", result);
    });

    this.saveprivileges();

    const role = "Reviewer";
    this.service.setPrevileges(formValue.reviewer.id, role, this.projectId).subscribe(result => {
      console.log("result3--", result);
      this.dialogRef.close();
      this.router.navigate(['/pages/project-details'], { queryParams: { id: this.projectId } });
      this.toastrService.show('success', " Record", ' inserted successfully');
    }, error => {
      console.error('Error logging in : ', error);
    })

  }

  saveprivileges() {
    var projectdata = [];
    this.loggedUserinfo = JSON.parse(localStorage.getItem('getuserlist'));
    for (let i = 0; i < this.loggedUserinfo.length; i++) {
      const userdetails = {
        "userId": this.loggedUserinfo[i]._id,
        "projectId": this.projectId,
      };
      projectdata.push(userdetails);
    }

    console.log("SetConsultantprivilage", projectdata);
    this.service.SetConsultantprivilage(projectdata).subscribe(result => {
      console.log("result2--", result);
    });
  }

  ProjectPrivileges(userList: any[]) {
    console.log("userList", userList);
    this.service.projectPrivileges(userList).subscribe(result => {
      if (result) {
        console.log(result);
      }
    }, error => {
      console.error('Error logging in : ', error);
    })
  }

}
