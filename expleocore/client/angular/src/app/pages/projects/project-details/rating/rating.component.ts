import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'exp-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  public ratingRange = [1, 2, 3, 4, 5];

  @Input() rating: number;
  constructor() {
   }

  ngOnInit(): void {
  }

}
