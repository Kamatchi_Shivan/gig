import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder, Form } from '@angular/forms';
import { RootService } from '../../../../../root.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageService } from '../../../../page-service';
import { ToastrService } from '../../../../../shared/toastr/toastr.service';
import { DatePipe } from '@angular/common';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'exp-scheduler-modify-details',
  templateUrl: './scheduler-modify-details.component.html',
  styleUrls: ['./scheduler-modify-details.component.scss']
})
export class SchedulerModifyDetailsComponent implements OnInit {
  schedulerForm: FormGroup;
  //Scheduler input file controls
  workOrdNum: FormControl;
  actRefer: FormControl;
  milest: FormControl;
  launchDate: FormControl;
  expDelDate: FormControl;
  actStDate: FormControl;
  proDelDate: FormControl;
  actDelDate: FormControl;
  status: FormControl;
  action: FormControl;
  otd: FormControl;
  oqd: FormControl;
  loggedUserRole: string;
  isConsultant: Boolean;
  isPM: Boolean;
  isReviewer: Boolean;
  isLM: boolean;
  data: any;
  createMileStone: boolean;
  role: string;
  create: boolean = false;
  update: boolean = false;
  reviewInfo: FormArray;
  projectInfo: any;
  revStDate:any;
  revEndDate:any;
  febaDate:any;
  delDateAfRework
  rejectInfo: any;
  startDate: any;
  endDate : any;
  minDate: Date;
  maxDate: Date;
  loggedUserinfo;
  closedCount = 0;
  postdata:any;
  reviewer_disable :boolean =true;
  dynamicFiledAdded: boolean = false;
  project_id_static: any;

  pipe = new DatePipe('en-US');

  STATEVALUE: any = {
    notStarted: 'Not Started',
    inProgress: 'In progress',
    delivered: 'delivered',
    reworkInProgress: 'Rework In progress',
    launched: 'launched',
    rejectRework: 'reject-rework',
    accepted: 'accepted'
  }
  ACTIONVALUE: any = {
    rejectforRework: 'Reject for rework',
    shareKPI: 'shareKPI',
    closed: 'Closed',
    acceptKPI: 'acceptKPI',
    rejectKPI: 'rejectKPI',
    submitForReview: 'Submit for review'
  }
  todayDate: Date = new Date();
  public statuslov = [
    { value: 'notStarted', viewvalue: 'Not Started', role: "consultant" },
    { value: 'inProgress', viewvalue: 'In progress', role: "consultant" },
    { value: 'delivered', viewvalue: 'Delivered', role: "consultant" },
    { value: 'reworkInProgress', viewvalue: 'Rework In progress', role: "consultant" },
    { value: 'launched', viewvalue: 'Launched', role: "project_manager" },
    { value: 'reject-rework', viewvalue: 'Reject-rework', role: 'reviewer' },
    { value: 'reject-rework', viewvalue: 'Reject-rework', role: 'project_manager' },
    { value: 'accepted', viewvalue: 'Accepted', role: 'project_manager' },
    { value: 'accepted', viewvalue: 'Accepted', role: 'reviewer' },
    { value: 'closed', viewvalue: 'Closed', role: 'project_manager' },
  ];

  public actionlov = [
    { value: 'rejectforRework', viewvalue: 'Reject for rework', role: 'project_manager' },
    { value: 'shareKPI', viewvalue: 'Share KPI', role: 'project_manager' },
    { value: 'closed', viewvalue: 'Closed', role: 'project_manager' },
    { value: 'acceptKPI', viewvalue: 'Accept KPI', role: "consultant" },
    { value: 'rejectKPI', viewvalue: 'Reject KPI', role: "consultant" },
    { value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
  ]

  constructor(private rootservice: RootService, private activateRoute: ActivatedRoute,
    private fb: FormBuilder, private service: PageService, private toastrService: ToastrService,
    private router: Router) { }
  //creates the form input controlls 
  createFormControls() {
    this.workOrdNum = new FormControl({ value: '', disabled: true });
    this.actRefer = new FormControl("", Validators.required);
    this.milest = new FormControl("", Validators.required);
    this.launchDate = new FormControl("", Validators.required);
    this.expDelDate = new FormControl("", Validators.required);
    this.actStDate = new FormControl("", Validators.required);
    this.proDelDate = new FormControl("", Validators.required);
    this.actStDate = new FormControl({ value: '' });
    this.proDelDate = new FormControl({ value: '' });
    this.actDelDate = new FormControl({ value: '' });
    this.otd = new FormControl("", Validators.required);
    this.oqd = new FormControl("", Validators.required);
    this.action = new FormControl("", Validators.required);
    this.status = new FormControl("", Validators.required);
    this.reviewInfo = new FormArray([]);
  }

  createForm() {
    this.schedulerForm = new FormGroup({
      workOrdNum: this.workOrdNum,
      actRefer: this.actRefer,
      milest: this.milest,
      launchDate: this.launchDate,
      expDelDate: this.expDelDate,
      actStDate: this.actStDate,
      proDelDate: this.proDelDate,
      actDelDate: this.actDelDate,
      reviewInfo: this.reviewInfo,
      status: this.status,
      otd: this.otd,
      oqd: this.oqd,
      action: this.action,
    });
  }
  getNewInfoGroup(): FormGroup {
    return this.fb.group({
      revStDate: [{ value: '' , disabled: false}],
      revEndDate: [{ value: '' , disabled: false}],
      febaDate: [{ value: '', disabled: false }],
      delDateAfRework: [{ value: '', disabled: false }]
    })
  }

  onAddNewRejectionInfo() {
    (<FormArray>this.schedulerForm.get('reviewInfo'))
      .push(this.getNewInfoGroup());
  }

  setRejectInfo(rejectInfo: any): FormArray {
    const formArray = new FormArray([]);
    let reviewArrLen = rejectInfo.length;
    if (this.loggedUserRole === 'reviewer' && !this.reviewer_disable) {
        rejectInfo.forEach((r, i) => {
          formArray.push(this.fb.group({
            revStDate: [{ value: r.revStDate, disabled: (i != reviewArrLen - 1) }, Validators.required],
            revEndDate: [{ value: r.revEndDate, disabled: (i != reviewArrLen - 1) }, Validators.required],
            febaDate: [{ value: r.febaDate, disabled: true }, Validators.required],
            delDateAfRework: [{ value: r.delDateAfRework, disabled: true }, Validators.required]
          }));
        });
    } else {
      rejectInfo.forEach(r => {
        formArray.push(this.fb.group({
          revStDate: [{ value: r.revStDate, disabled: true }, Validators.required],
          revEndDate: [{ value: r.revEndDate, disabled: true }, Validators.required],
          febaDate: [{ value: r.febaDate, disabled: true }, Validators.required],
          delDateAfRework: [{ value: r.delDateAfRework, disabled: true }, Validators.required]
        }));
      });
    }
    return formArray;
  }

  updateValues(data: any) {
    this.schedulerForm.patchValue({
      workOrdNum: data.workOrderNumber,
      actRefer: data.activityReference,
      milest: data.milestone,
      launchDate: data.launchingDate,
      expDelDate: data.expectedDeliveryDate,
      actStDate: data.activityStartDate,
      proDelDate: data.proposedDeliveryDate,
      actDelDate: data.actualDeliveryDate,
      status: data.status,
      reviewInfo:data.reviewInfo,
      otd: data.otd,
      oqd: data.oqd,
      action: data.action,
    });
    if(data.status == "delivered")
    {
      this.reviewer_disable=false;
    }

    if (Object.keys(data).length === 0 && data.constructor === Object) {
      this.schedulerForm.patchValue({
        workOrdNum: this.projectInfo.projectRefNo,
        status: this.STATEVALUE.launched.toLowerCase(),
      });
      let obj = {
        "revStDate": null,
        "revEndDate": null,
        "febaDate": null,
        "delDateAfRework": null
      }
      let rev = [];
      rev.push(obj);
      this.schedulerForm.setControl('reviewInfo', this.setRejectInfo(rev));
    }
    else if (data.workPackageStatus?.toLowerCase() != this.STATEVALUE.launched) {
      this.schedulerForm.setControl('reviewInfo', this.setRejectInfo(data.reviewInfo));

    }
    this.schedulerForm.updateValueAndValidity();

  }


  projectId :any;
  type:any;
  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
    this.rejectInfo = this.rootservice.getSchularList();
    this.loggedUserRole = this.rootservice.mdpic.role.toLowerCase().replace(" ", "_");
    this.projectInfo = this.rootservice.getProjectInfo();
    console.log(this.loggedUserRole);
    this.activateRoute.queryParams.subscribe(params => {
      this.projectInfo =(params);
    })
    if(typeof this.projectInfo.project === 'undefined')
    {
    this.startDate =  this.pipe.transform(this.projectInfo.startDate,'dd/MM/yyyy');
    this.endDate =  this.pipe.transform(this.projectInfo.endDate,'dd/MM/yyyy');

    this.minDate = new Date(this.projectInfo.startDate);
    this.maxDate = new Date(this.projectInfo.endDate);
    
    this.project_id_static = this.projectInfo._id;
    this.activateRoute.queryParams.subscribe(params => {
      this.getSchedualrData(params);
    })

   

    this.loggedUserinfo = JSON.parse(localStorage.getItem('user'));
    this.service.schedulertestGetDeatils(this.projectInfo._id).subscribe((res: any) => {
      if (res.length == 0) {
        // this.createScheduler();
      } else {

      }

    });
    this.service.schedulercollaboratoruserDetails(this.projectInfo._id).subscribe((res: any) => {
      console.log(res);
      if (res.length == 0) {
        // this.createScheduler();
      } else {

      }

    });
  }
  else{
    this.service.projectMoreDetails(this.projectInfo.project).subscribe((res: any) => {
      this.projectInfo = res.projectInfo;
      this.create =true;
      this.disableschdulerfield();
      this.enableBasicCreationInfo();
      this.stage_pm_open();
      this.schedulerForm.patchValue({
          workOrdNum: res.projectInfo.projectRefNo,
          status: this.STATEVALUE.launched
        });

    });
    
  }
  }

  isValid(date)
  {
    //console.log(date);
    if((date < this.startDate) && (date > this.endDate)){
      console.log("Date error");
      return true;
    }
  }
  
  backToProjectDeatils()
  {
    location.reload();
    //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.projectInfo._id } });
  }

  async getSchedualrData(params) {
    await this.service.schedularget(params._id).subscribe(result => {
      console.log("result", result);
      this.data = result;
      this.disableschdulerfield();
   
      
      if (result) {
          if (Object.keys(this.data).length === 0 && this.data.constructor === Object) {
          this.create = true; 
        }
        else {
          console.log(params.status);
            if(this.loggedUserRole == "project_manager")
            {
              if(params.status == "accepted" && (params.action == "Submit_for_review" || params.action =="rejectKPI") )
              {
              this.update = true;
              this.enableOtdOqd();
              this.enableAction();
              this.stage_pm_oot_oqd();
              }
              else if(params.status == "accepted" && params.action =="acceptKPI"){
                this.update = true;
                this.enableStatusAction();
                this.stage_pm_final();
              }
            }
            else if(this.loggedUserRole == "consultant" && ( params.status == "accepted" || params.status == "inProgress" || params.status == "reject-rework"  || params.status == "notStarted"  || params.status == "launched" || params.status == "reworkInProgress"))
            {
          
             
                if(params.status == "inProgress" )
                {
                  this.update = true;
                 if(params.actDelDate == '' ||  params.actStDate == '' || params.proDelDate == '')
                 {
                    this.enabledConsultant();
                 }
                  this.stage_consultant_delievery();
                  this.enableStatusAction();
                  //this.schedulerForm.get('action').setValidators(Validators.required);
                  this.schedulerForm.get('status').setValidators(Validators.required);
                }
                else if(params.status == "notStarted" )
                {
                  this.update = true;
                   if(params.actDelDate == '' ||  params.actStDate == '' || params.proDelDate == '')
                 {
                    this.enabledConsultant();
                 }
                  this.stage_consultant_notstarted();
                  this.enableStatus();
                }
                else if(params.status == "accepted"  && (params.action == "shareKPI" || params.action == "rejectKPI" ) )
                {
                  this.update = true;
                  this.stage_consultant_oot_oqd();
                  this.enableAction();
                }
                else if(params.status == "launched" )
                {
                  this.update = true;
                  this.enabledConsultant();
                  this.stage_consultant_launched();
                  this.enableStatus();
               }
                else if(params.status == "reject-rework" )
                {
                  this.update = true;
                  //this.enabledConsultant();
                  this.stage_consultant_reworkrequest();
                  this.enableStatus();
                }
                else if(params.status == "reworkInProgress" )
                {
                  this.update = true;
                  //this.enabledConsultant();
                  this.stage_consultant_delievery_after();
                  this.enableStatus();
                }
                
            }
            else if(this.loggedUserRole == "reviewer" && params.status == "delivered" )
            {
              this.update = true;
              this.enableStatus();
              this.stage_reviewer_update();
              this.schedulerForm.setControl('reviewInfo', this.setRejectInfo(this.data.reviewInfo));
            }
              /**Update data starts***/
      console.log(this.data);
      this.updateValues(this.data);
      

      /**Update data ends***/
          }
      }
    }, error => {

    })


  }
  stage_pm_open()
  {
    //this.schedulerForm.get('status').setValidators(Validators.required);
    this.statuslov = [
      { value: 'launched', viewvalue: 'Launched', role: "project_manager" }
    ];
    this.actionlov = [
      //{ value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
    ]
  }
  stage_consultant_notstarted()
  {
    this.statuslov = [
      { value: 'inProgress', viewvalue: 'In progress', role: "consultant" },
 ];
  
    this.actionlov = [
      { value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
       ]
  }
  stage_consultant_launched()
  {
    this.statuslov = [
      { value: 'notStarted', viewvalue: 'Not Started', role: "consultant" },
      { value: 'inProgress', viewvalue: 'In progress', role: "consultant" },
    ];
  
    this.actionlov = [
      { value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
       ]
  }
  stage_consultant_reworkrequest()
  {
    this.statuslov = [
      { value: 'reworkInProgress', viewvalue: 'Rework In progress', role: "consultant" },
      { value: 'delivered', viewvalue: 'Delivered', role: "consultant" },
    ];
  
  }
  stage_consultant_delievery()
  {
    this.statuslov = [
      { value: 'delivered', viewvalue: 'Delivered', role: "consultant" },
    ];
    this.actionlov = [
      { value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
       ]
  } 
  stage_consultant_delievery_after()
  {
    this.statuslov = [
      { value: 'delivered', viewvalue: 'Delivered', role: "consultant" },
    ];
  } 
  stage_consultant_KPI()
  {
    this.statuslov = [
      { value: 'notStarted', viewvalue: 'Not Started', role: "consultant" },
      { value: 'inProgress', viewvalue: 'In progress', role: "consultant" },
      { value: 'delivered', viewvalue: 'Delivered', role: "consultant" },
      { value: 'reworkInProgress', viewvalue: 'Rework In progress', role: "consultant" }
    ];
  
    this.actionlov = [
      { value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
       ]
  }

  stage_reviewer_update()
  {

    this.statuslov = [
      { value: 'reject-rework', viewvalue: 'Reject-rework', role: 'reviewer' },
      { value: 'accepted', viewvalue: 'Accepted', role: 'reviewer' }
    ];
  
    /**this.actionlov = [
      { value: 'rejectforRework', viewvalue: 'Reject for rework', role: 'project_manager' },
      { value: 'Closed', viewvalue: 'Closed', role: 'project_manager' }
      ]**/
  }
 
  stage_cosultant_rework()
  {
    this.statuslov = [
     // { value: 'delivered', viewvalue: 'Delivered', role: "consultant" },
     // { value: 'Rework In progress', viewvalue: 'Rework In progress', role: 'consultant' }
    ];
  
    this.actionlov = [
      { value: 'Submit_for_review', viewvalue: 'Submit for review', role: "consultant" }
    ]
  }
  stage_pm_oot_oqd()
  {
    this.actionlov = [
        { value: 'shareKPI', viewvalue: 'Share KPI', role: 'project_manager' },
    ]
  }
  stage_pm_final()
  {
    this.statuslov = [
       { value: 'closed', viewvalue: 'Closed', role: "project_manager" },
     ];
    this.actionlov = [
        { value: 'closed', viewvalue: 'Closed', role: 'project_manager' },
    ]
  }
  stage_consultant_oot_oqd()
  {
    this.actionlov = [
      { value: 'acceptKPI', viewvalue: 'Accept KPI', role: "consultant" },
      { value: 'rejectKPI', viewvalue: 'Reject KPI', role: "consultant" }
    ]
  }
  disableschdulerfield()
  {
    /***CREATION INFO PM AREA STARTS***/

    this.schedulerForm.controls['actRefer'].disable();
    this.schedulerForm.controls['milest'].disable();
    this.schedulerForm.controls['launchDate'].disable();
    this.schedulerForm.controls['expDelDate'].disable();

    /***CREATION INFO PM AREA ENDS***/
    
    /***CONSULATANT AREA STARTS***/

    this.schedulerForm.controls['actDelDate'].disable();
    this.schedulerForm.controls['actStDate'].disable();
    this.schedulerForm.controls['proDelDate'].disable();

   /***CONSULATANT AREA ENDsS***/


   /***REVIEW AREA STARTS***/

   /**this.schedulerForm.controls['revStDate'].disable();
   this.schedulerForm.controls['revEndDate'].disable();
   this.schedulerForm.controls['febaDate'].disable();
   this.schedulerForm.controls['delDateAfRework'].disable();**/

   /***REVIEW AREA ENDS***/
   
    /***OTD AREA STARTS***/

    this.schedulerForm.controls['otd'].disable();
    this.schedulerForm.controls['oqd'].disable();

   /***OTD AREA ENDS***/ 

   /***STATUS ACTION AREA STARTS***/ 

   this.schedulerForm.controls['status'].disable();
   this.schedulerForm.controls['action'].disable();

    /***STATUS ACTION AREA ENDS***/ 

  }

  enableBasicCreationInfo(){
    this.schedulerForm.controls['actRefer'].enable();
    this.schedulerForm.controls['milest'].enable();
    this.schedulerForm.controls['launchDate'].enable();
    this.schedulerForm.controls['expDelDate'].enable();
  }  


  enabledConsultant() {
    this.schedulerForm.controls['actDelDate'].disable();
    this.schedulerForm.controls['actStDate'].enable();
    this.schedulerForm.controls['proDelDate'].enable();
  }

  enableOtdOqd() {
    this.schedulerForm.controls['otd'].enable();
    this.schedulerForm.controls['oqd'].enable();
  }

  enableStatusAction() {
    this.schedulerForm.controls['status'].enable();
    this.schedulerForm.controls['action'].enable();
  }
  enableStatus() {
    this.schedulerForm.controls['status'].enable();
  }
  enableAction() {
    this.schedulerForm.controls['action'].enable();
  }


 



  onChange(status: any) {
    console.log(status);
    if (this.loggedUserRole === 'consultant' && status === 'delivered') {
      this.schedulerForm.controls.action.setValue('Submit_for_review');
      this.schedulerForm.controls.actDelDate.setValue(new Date());
    } 
    // else if (this.loggedUserRole === 'reviewer' && status === "reject-rework") {
    //   if (!this.dynamicFiledAdded) {
    //     (<FormArray>this.schedulerForm.get('reviewInfo'))
    //       .push(this.getNewInfoGroup());
    //     this.dynamicFiledAdded = true;
    //   }
    // } 
    else if (this.loggedUserRole === 'reviewer' && status === this.STATEVALUE.accepted) {
      const controlArray = <FormArray>this.schedulerForm.get('reviewInfo');
      if (this.dynamicFiledAdded) {
        let reviewArr = (<FormArray>this.schedulerForm.get('reviewInfo'));
        (<FormArray>this.schedulerForm.get('reviewInfo')).removeAt(reviewArr.length - 1);
        this.dynamicFiledAdded = false;
      }
      controlArray.controls[controlArray.length-1].get('febaDate').setValue(new Date());
    }
    else if(this.loggedUserRole === 'project_manger' && status === this.STATEVALUE.accepted){
      if(this.schedulerForm.get('action') == this.ACTIONVALUE.shareKPI){
        this.schedulerForm.get('otd').setValidators(Validators.required);
        this.schedulerForm.get('oqd').setValidators(Validators.required);
        this.schedulerForm.controls['otd'].enable();
        this.schedulerForm.controls['oqd'].enable();
      }


      const controlArray = <FormArray>this.schedulerForm.get('reviewInfo');
      if (this.dynamicFiledAdded) {
        let reviewArr = (<FormArray>this.schedulerForm.get('reviewInfo'));
        (<FormArray>this.schedulerForm.get('reviewInfo')).removeAt(reviewArr.length - 1);
        this.dynamicFiledAdded = false;
      }
      controlArray.controls[controlArray.length-1].get('febaDate').setValue(new Date());
    }
  }

  /**
   * schedulerSave start
   */

  schedulerSave() {
    console.log(this.schedulerForm.value);
    let obj = {

      //workOrderNumber: this.schedulerForm.getRawValue().workOrdNum,
      actRefer: this.schedulerForm.value.actRefer,
      //actStDate: this.schedulerForm.value.actStDate,
      expDelDate: this.schedulerForm.value.expDelDate,
      launchDate: this.schedulerForm.value.launchDate,
      milest: this.schedulerForm.value.milest,
      //proDelDate: this.schedulerForm.value.proDelDate,
      workOrdNum: this.schedulerForm.getRawValue().workOrdNum,
      projectId: this.projectInfo._id,
      reviewInfo: [{
        revStDate: "",
        revEndDate: "",
        febaDate: "",
        delDateAfRework: "",
        disable :"false"
      }],
      milestoneStatus: this.schedulerForm.value.reviewInfo ? this.schedulerForm.value.reviewInfo : null,
      status: this.STATEVALUE.launched,
      otd: this.schedulerForm.value.otd ? this.schedulerForm.value.status : null,
      oqd: this.schedulerForm.value.oqd ? this.schedulerForm.value.oqd : null,
      action: this.schedulerForm.value.action ? this.schedulerForm.value.action : null,

    }

    console.log(this.schedulerForm.value);
    if (this.schedulerForm.valid) {
      const PROJECT_ID = this.project_id_static;
      console.log("---------->", PROJECT_ID);
      this.service.schedulerSave(obj).subscribe(result => {
        if (result) {
          this.setporojectstatus();
          this.toastrService.show('success', 'Success', 'Scheduler details successfully updated',
            { queryParams: { 'pid': PROJECT_ID }, skipLocationChange: true });
          this.mailService();
          location.reload();
          //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.projectInfo._id } });

        }
      }, error => {
        console.log(error);
      })
    }
  }
  /**
   * end
   */
  mailService(){
    let proId = this.projectInfo._id;
    let proName = this.projectInfo.projectName;
    let consultanList = [];
    this.service.getNomineeList(proId).subscribe(result => {
      consultanList = result.map(item => {
        return {
          "projectName": proName,
          "nomineename": item.name,
          "nomineeMailId": item.mail,
        }
      });
      this.service.msCreatedCons(consultanList).subscribe(result =>{
      });
      this.service.getReviewerInfo(proId).subscribe(res=>{
        const reviewer = res;
        const reviewerInfo = {
          "projectName" : proName,
          "reviewerName" : reviewer[0].name,
          "reviewerEmail": reviewer[0].email
        }
        this.service.msCreatedReviewer(reviewerInfo).subscribe(res=>{
        });
        this.toastrService.show('success', 'Success', 'Mail has successfully');
      });
    }, error => {
      console.log(error);
    })
    console.log();
  }
  addRejectionFormGroup(): FormGroup {
    return this.fb.group(
      {
        revStDate: new FormControl("", Validators.required),
        revEndDate: new FormControl("", Validators.required),
        febaDate: new FormControl("", Validators.required),
        delDateAfRework: new FormControl("", Validators.required)
      }
    )
  }

  // update call start 
  schedulerUpdate() {
    if (this.loggedUserRole === 'consultant') {
      this.schedularConsultantUpdate();
    } else if (this.loggedUserRole === 'reviewer') {
      this.schedularReviwerUpdate();
    } else if (this.loggedUserRole === 'project_manager') {
      this.schedularPMUpdate();
    }
    this.setporojectstatus();
   

  }

  setporojectstatus(){

    this.service.schedulertestGetDeatils(this.project_id_static).subscribe((res: any) => {
      console.log(" schedular res:"+res);
      console.log("len:" + res.length);
      if (res.length > 0) {
        var i: number;
        var j: number=0;
        var data = [];
        for (i = 0; i < res.length; i++) {
          console.log("res action" +res[i].action);
        if (res[i].action == "Closed")
        {
          this.closedCount = j+1;
          j++;
        }
        }
        let projectPercent = 0;
        let projectPercentage = 0;
        if(this.closedCount > 0)
        {
          projectPercent = (this.closedCount / res.length *100);
          projectPercentage = Number(projectPercent);
          
        }
        console.log("proj percent:" +projectPercent);
        console.log("pp:" +projectPercentage);
        
          if (projectPercentage < 100)
          {
            this.postdata ={
              'projectId':this.project_id_static,
              'workPackageStatus': "In progress",
              'projectStatusPercentage': projectPercentage
              }
          }
          else{
            this.postdata ={
              'projectId':this.project_id_static,
              'workPackageStatus': "Closed",
              'projectStatusPercentage': projectPercentage
              }
          }
         console.log("postData:"+this.postdata);
        this.service.setProjectStatus(this.postdata).subscribe((res: any) => {
            console.log(res);

        });
      
      }
    });
  }

  /**
   * disbling the formarray 
   */
  disableInputs() {
    let reviewArr = (<FormArray>this.schedulerForm.get('reviewInfo')).controls;
    let reviewArrLen = reviewArr.length;
    if (reviewArrLen > 0) {

    }
    reviewArr
      .slice(0, reviewArrLen - 1)
      .forEach(control => {
        control.disable();
      })
  }

  /**
   * schedular Update start
   */

  schedularReviwerUpdate() {
    let reviewArr = this.schedulerForm.getRawValue().reviewInfo;
    let reviewArrLen = reviewArr.length;
    console.log(this.schedulerForm.value);
    if(this.schedulerForm.valid){
      if (this.loggedUserRole === 'reviewer' && this.schedulerForm.value.status === "accepted" ) {
        let reviewArr = this.schedulerForm.getRawValue().reviewInfo;
        if (reviewArr.length > 0) {
          reviewArr[reviewArr.length - 1].febaDate = new Date();
          reviewArr[reviewArr.length - 1].delDateAfRework = new Date();
        }
        let obj = {
          _id: this.data._id,
          status: this.schedulerForm.value.status ? this.schedulerForm.value.status : null,
          action: "Submit_for_review",
          reviewInfo: reviewArr ? reviewArr : [],
          addReviewInfo: true
        };
        console.log("-----> Review", obj);
        
        this.service.schedularReviwerUpdate(obj).subscribe(result => {
          if (result) {
  
            this.toastrService.show('success', 'Success', 'Scheduler details successfully updated', { queryParams: { 'projectId': this.data.projectId }, skipLocationChange: true });
            location.reload();
            //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.project_id_static } });
          }
        }, error => {
  
        })
      } else if (this.loggedUserRole === 'reviewer' && this.schedulerForm.value.status === this.STATEVALUE.rejectRework) {
        if (this.schedulerForm.valid) {

          let reviewArr = this.schedulerForm.getRawValue().reviewInfo;
          if (reviewArr.length > 0) {
            reviewArr[reviewArr.length - 1].febaDate = new Date();
          }
          reviewArr.push({
            revStDate: "",
            revEndDate: "",
            febaDate: "",
            delDateAfRework: ""
          });
          let obj = {
            _id: this.data._id,
            status: this.schedulerForm.value.status ? this.schedulerForm.value.status : null,
            action: "Submit_for_review",
            reviewInfo: reviewArr ? reviewArr : [],
            addReviewInfo: true
          };
  
          console.log('reviwer reject-rework', obj);

          this.service.schedularReviwerUpdate(obj).subscribe(result => {
            if (result) {
              this.toastrService.show('success', 'Success', 'Scheduler details successfully updated', { queryParams: { 'projectId': this.data.projectId }, skipLocationChange: true });
              //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.project_id_static } });
              location.reload();
            }
          }, error => {
  
          })
  
        }
      } 
    }
    

  }

  schedularPMUpdate() {
    if(this.schedulerForm.valid){   
      console.log(this.schedulerForm.value); 
          let reviewArr = this.schedulerForm.getRawValue().reviewInfo;
          console.log(this.schedulerForm.getRawValue())
          let obj={};
          if(this.schedulerForm.value.action != "closed")
          {
           obj = {
            _id: this.data._id,
            status: this.schedulerForm.value.action == "closed" ? "closed" : "accepted",
            action: this.schedulerForm.value.action ? this.schedulerForm.value.action : null,
            otd: this.schedulerForm.value.otd ? this.schedulerForm.value.otd : null,
            oqd: this.schedulerForm.value.oqd ? this.schedulerForm.value.oqd : null,
            reviewInfo: reviewArr ? reviewArr : [],
          };
        }
        if(this.schedulerForm.value.action == "closed")
          {
           obj = {
            _id: this.data._id,
            status: this.schedulerForm.value.action == "closed" ? "closed" : "accepted",
            action: this.schedulerForm.value.action ? this.schedulerForm.value.action : null,
            reviewInfo: reviewArr ? reviewArr : [],
          };
        }
          console.log(obj);
          console.log('PM accepted close kpi', obj);
          this.service.schedularPMUpdate(obj).subscribe(result => {
            if (result) {
              this.toastrService.show('success', 'Success', 'Scheduler details successfully updated', { queryParams: { 'projectId': this.data.projectId }, skipLocationChange: true });
              location.reload(); 
              //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.data._id } });
            }
          }, error => {
  
          })
        
        
      
    }
  }

  schedularConsultantUpdate() {
    if (this.loggedUserRole === 'consultant') {
      if (this.schedulerForm.valid) {
        if (this.schedulerForm.value.action == "acceptKPI" || this.schedulerForm.value.action == "rejectKPI") {
          let obj = {
            _id: this.data._id,
            status: this.schedulerForm.value.status ? this.schedulerForm.value.status : null,
            action: this.schedulerForm.value.action ? this.schedulerForm.value.action : null
          }
          obj.status = "accepted";
          obj.action = this.schedulerForm.value.action;
          console.log("consultan accepted kpi and accepted status", obj)
          this.service.schedularConsultantUpdate(obj).subscribe(result => {
            if (result) {
              console.log(this.project_id_static);
              this.toastrService.show('success', 'Success', 'Scheduler details successfully updated',
                { queryParams: { 'pid': this.project_id_static }, skipLocationChange: true });
                location.reload(); 

                //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.projectInfo._id } });

            }
          }, error => {
  
          })
        }

     else {
          let reviewArr = this.schedulerForm.getRawValue().reviewInfo;
           let obj = {
            _id: this.data._id,
            status: this.schedulerForm.value.status ? this.schedulerForm.value.status : null,
            action: this.schedulerForm.value.status == ( "delivered" || "reworkInProgress" )? "Submit_for_review" : null,
            actualDeliveryDate: new Date(),
            activityStartDate: this.schedulerForm.getRawValue().actStDate ? this.schedulerForm.getRawValue().actStDate : null,
            proposedDeliveryDate: this.schedulerForm.getRawValue().proDelDate ? this.schedulerForm.getRawValue().proDelDate : null,
            reviewInfo: reviewArr ? reviewArr : [],
          }
          console.log(reviewArr);
          if(reviewArr.length > 1){   
            reviewArr[reviewArr.length - 2].delDateAfRework = new Date();
            obj.reviewInfo = reviewArr;
          }
          console.log(reviewArr);
          console.log("accepted status", obj);
          this.service.schedularConsultantUpdate(obj).subscribe(result => {
            if (result) {
              console.log(this.project_id_static);
              this.toastrService.show('success', 'Success', 'Scheduler details successfully updated',
                { queryParams: { 'pid': this.project_id_static }, skipLocationChange: true });
                location.reload(); 

                //this.router.navigate(['/pages/mattable'], { queryParams: { 'pid': this.projectInfo._id } });
            }
          }, error => {
  
          })
          //this.schedulerForm.get('reviewInfo').value[len-2].delDateAfRework = new Date();
        }
      }
    }
  }

  /**
   * end
   */

  onStartChanged() {
    this.schedulerForm.patchValue({ 'endDate': null })
  }

}
