import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { RatingComponent } from './rating/rating.component';
import { PageService } from '../../page-service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { Skills } from '../../../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { RootService } from '../../../root.service';
import { MatDialog } from '@angular/material/dialog';
import {​​​​ DatePipe }​​​​ from '@angular/common';
import { CollaboratorsModelComponent } from  './collaborators/collaborators-model/collaborators-model.component';
@Component({
  selector: 'exp-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit,AfterViewInit {
  pageData: any;
  projectInfo:any;
  skillinfo:any;
  fileInfo:any;
  projectid;
  allMOMs:any;
  name:any;
  Download;
  projectSkills: Skills;
  cardImageBase64: string;
  today: number = Date.now();
  pmContent = ['kickOff','addMomBtn','constraints','closureMom','closureMomBtn','projectStatus','q&a','q&aUpload','q&aDownLink','skillset','skillRating','skillWeightage','collaborators','scheduleLink','deliverables','budget','tools','acceptanceCriteria'];
  lmContent = ['projectStatus'];
  reviewerContent = ['kickOff','constraints','closureMom','projectStatus','scheduleLink','deliverables','budget','tools','acceptanceCriteria'];
  consultantContent = ['kickOff','constraints','projectStatus','q&a','q&aDownLink','skillset','scheduleLink','deliverables','closureMom','budget','tools','acceptanceCriteria'];
  displayProgressSpinner = false;
  isMomCommVis: boolean = false;
  isCloseMom: boolean = true;
  isClosureMomVis: boolean = false;
  isLoading:boolean = true;
  isSchedulerLinkVisible = false;
  pipe = new DatePipe('en-US');
  avatars = [
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },{
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },{
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image" : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
      'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
      'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    }
  ]

  //form object
  momForm: FormGroup;
  closureMomForm: FormGroup;

  momText: FormControl;
  closureMomText: FormControl;

  constructor(
        private fb: FormBuilder,
        public rootservice: RootService,
        private pageService: PageService,
        private toastrService: ToastrService,
        private activatedRoute:ActivatedRoute,
        private router: Router,
        private dialog: MatDialog) { }

  //creates the form input controlls 
  createFormControls(){
    this.momText = new FormControl("", Validators.required);
    this.closureMomText = new FormControl("", Validators.required);
  }

   showToaster(){​​​
     let Today = new Date();
     let date = this.pipe.transform(Today, 'dd/MM/yyyy');
     let endDate = this.pipe.transform(this.projectInfo.endDate,'dd/MM/yyyy');
     if(endDate < date ){​​​​
     this.toastrService.show('warning', 'Warning', 'End Date Should be Changed!!');
     }​​​​
    }

  dateDiff = function(stDt, edDt) {
    let st = new Date(stDt);
    let ed = new Date(edDt);

    let diff_time = (ed.getTime() - st.getTime()) / (1000 * 3600 * 24);

    return diff_time;
  }
  createForm() {
    this.momForm = new FormGroup({
      momText: this.momText,
    });

  

    this.closureMomForm = new FormGroup({
      closureMomText: this.closureMomText,
    });
  }
  showPdf() {
    const linkSource = this.pageData.fileinfo.fileData;
    const downloadLink = document.createElement("a");
    const fileName = "sample.pdf";

    downloadLink.href = linkSource;
    downloadLink.download = fileName;
    downloadLink.click();
  }
  isVisible(content: string){
    const role:string = this.rootservice.mdpic.role.toLowerCase().replace(" ", "_");
    switch (role) {
      case 'project_manager':
            if(this.pmContent.indexOf(content)>-1){
              return true;
            }
            break;
      case 'line_manager':
            if(this.lmContent.indexOf(content)>-1){
              return true;
            }
            break;
      case 'reviewer':
            if(this.reviewerContent.indexOf(content)>-1){
              return true;
            }
            break;
      case 'consultant':
            if(this.consultantContent.indexOf(content)>-1){

              return true;
            }
            break;
    }
  }
  addMOM(): void{
    this.isMomCommVis = !this.isMomCommVis;
  }
  addClosureMom(): void{
    this.isClosureMomVis = !this.isClosureMomVis;
    console.log("test");
  }
  getAllMOMs(projectid) {
    var i: number;
    this.pageService.getAllMOMs(projectid).subscribe(res => {
      console.log(res.mom.length);
      this.allMOMs = "";
      if (res.mom.length > 0) {
        for (i = 0; i < res.mom.length; i++) {
          this.allMOMs += `${res.mom[i].MOM}<br/>------------------------------------------------<br/>`;
        }
        console.log(this.allMOMs);
        document.getElementById('result').innerHTML = this.allMOMs;
        this.displayProgressSpinner = false;
      }
    });
  }
  saveMOM() {
    this.pageService.saveMomforproject(this.projectid, this.momForm.value.momText).subscribe(res => {
      if (res.insertedId) {
        this.toastrService.show('success', 'Success', 'MOM successfully saved');
        this.getAllMOMs(this.projectid);
        this.momForm.reset();
      }

    });
  }

  saveCloserMOM() {
    this.pageService.saveCloserMOM(this.projectid, this.closureMomForm.value.closureMomText).subscribe(res => {
      console.log(this.closureMomForm.value.closureMomText);
      if (res.code == 200) {
        this.toastrService.show('success', 'Success', 'CloserMOM successfully saved');
        this.closureMomForm.reset();
        this.isClosureMomVis = !this.isClosureMomVis;
        //this.getAllMOMs(this.projectid);
      }
    });

  }

  getprojectinfo(projectid)
  {
    //console.log("getprojectinfo",projectid);
    this.pageService.projectMoreDetails(projectid).subscribe(res => {
      if(res.projectInfo.workPackageStatus  !=  'Closed'  &&  res.projectInfo.projectStatusPercentage != '100')
      {
        this.isCloseMom=false;
      }
    this.projectInfo = res.projectInfo;
    this.rootservice.setProjectInfo(this.projectInfo);
    this.skillinfo = res.skillInfo;
    this.fileInfo = res.fileInfo;
    this.cardImageBase64 =this.fileInfo.fileData;
    const spiltfile =this.cardImageBase64.split(',', 2);
    this.name =spiltfile[1];
    this.isLoading = false;
  });
  }


  download() {
    var decodedString = atob(this.name);
    //var bindata = window.atob(this.name);
    console.log(decodedString);
    function DownloadExcel() {
      window.location.href = "data:application/vnd.ms-excel;base64, decodedString";
    }
    var blob = new Blob([decodedString], { type: 'application/vnd.ms-excel' });
    if (window.navigator && window.navigator.msSaveBlob) {
      window.navigator.msSaveBlob(blob);
    }
    else {
      this.Download = URL.createObjectURL(blob);
      window.open(this.Download);

    }

  }

  getskilllist(projectid) {
    this.pageService.getSkillList(projectid).subscribe((res: any) => {
      this.projectSkills = res;
      console.log(this.projectSkills);
    });

  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-projectdetails');
  }

  ngOnInit(): void {
    this.loadbodyclass();
    this.createFormControls();
    this.createForm();
    this.activatedRoute.queryParams.subscribe(params => {
      this.projectid = params['id'];
      console.log(this.projectid);
    })
   // this.projectid = "5f1a870a4b831d1fa09f002a";
    this.getprojectinfo(this.projectid);
    this.getskilllist(this.projectid);
    this.displayProgressSpinner = false;
    this.isSchedulerPageVisible();
  }
  isSchedulerPageVisible(){
    const role:string = this.rootservice.mdpic.role.toLowerCase().replace(" ", "_");
    let data = {
      "userId": JSON.parse(localStorage.getItem('user'))._id,
      "projectId": this.projectid	,
       "role":role
    }

    this.pageService.isCollabratorAvailable(data).subscribe((res: any) => {
      console.log(res);
      const role:string = this.rootservice.mdpic.role.toLowerCase().replace(" ", "_");

      this.isSchedulerLinkVisible = Object.keys(res.user).length != 0;
    });
  }
  openCollaboratorsModel(): void {
    const dialogRef = this.dialog.open(CollaboratorsModelComponent,{
      panelClass: 'icon-outside',
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngAfterViewInit(){
    setTimeout (() => {
      this.getAllMOMs(this.projectid);
      this.showToaster();
   }, 1000);
  }
}
