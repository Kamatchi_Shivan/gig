import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { NbCardModule,NbButtonModule,NbIconModule,NbProgressBarModule } from '@nebular/theme';


import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';

import { CommonModule } from '@angular/common';


import { SharedModule } from '../../../shared/shared.module';
import { SubmitNominationComponent } from './submit-nomination.component';
import { MatTooltipModule } from '@angular/material/tooltip';

const materialModules = [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatChipsModule,
    MatTooltipModule
  ];
  
  @NgModule({
    imports: [
      NbCardModule,
      NbIconModule,
      NbButtonModule,
      ReactiveFormsModule,
      CommonModule,
      NbProgressBarModule,
      ...materialModules,
      SharedModule
    ],
    declarations: [
        SubmitNominationComponent
    ]
  })
  export class SubmitNominationModule {
      
  }