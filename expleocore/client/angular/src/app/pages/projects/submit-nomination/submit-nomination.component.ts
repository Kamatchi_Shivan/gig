import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as _ from 'lodash';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { PageService } from '../../page-service';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { Userdetails } from '../../../models/models';
import { RootService } from '../../../root.service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { Router } from '@angular/router';
import { NbIconLibraries } from '@nebular/theme';
@Component({
  selector: 'exp-submit-nomination',
  templateUrl: './submit-nomination.component.html',
  styleUrls: ['./submit-nomination.component.css']
})
export class SubmitNominationComponent implements OnInit {
  //file attributes 
  selectedFiles;
  filename;
  filetype;
  getdoucmentbyte;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  nominationForm: FormGroup;
  skills: FormArray;
  projectSkills = [];
  pmInfo = [];
  currentManager: FormControl;
  budget: FormControl;
  comments: FormControl;
  lMlist: Userdetails;
  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<SubmitNominationComponent>,
    @Inject(MAT_DIALOG_DATA) public projectId: any, private pageService: PageService, public rootservice: RootService,
    private toastrService: ToastrService, private router: Router, public dialog: MatDialog, private iconLibraries: NbIconLibraries) {
    this.iconLibraries.registerFontPack('font-awesome', { iconClassPrefix: 'fas' });
  }

  //creates the form input controlls 
  createFormControls() {
    this.currentManager = new FormControl("", Validators.required);
    this.budget = new FormControl("", Validators.required);
    this.comments = new FormControl("", Validators.required);
    this.skills = new FormArray([]);
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  createForm() {
    this.nominationForm = new FormGroup({
      skills: this.skills,
      currentManager: this.currentManager,
      budget: this.budget,
      comments: this.comments
    })
  }

  setExistingSkills(skillsSet): FormArray {
    const formArray = new FormArray([]);
    skillsSet.forEach(s => {
      formArray.push(this.fb.group({
        id: s.skillId,
        name: [s.skillName, Validators.required],
        rating: ["", Validators.required]
      }));
    });
    return formArray;
  }
  updateValues(data: any) {
    this.nominationForm.setControl('skills', this.setExistingSkills(data.skills));
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-nomination');
  }

  ngOnInit(): void {
    this.loadbodyclass();
    console.log(this.projectId);
    this.createFormControls();
    this.createForm();
    this.getskilllist(this.projectId.id);
    this.getLineManagerList();
  }

  openDialog(): void {
    const dialogRef = this.dialog.closeAll();
  }

  submit() {
    console.log("input", this.nominationForm.value);
    const userid = this.rootservice.mdpic;
    if (this.nominationForm.valid) {
      this.pageService.nomineecheck(this.projectId.id, userid._id).subscribe((res: any) => {
        console.log(res.code);
        if (res.code == 200) {
          this.nominationForm.reset();
          this.toastrService.show('danger', 'The project already nominated', 'Please choose the different project');
          this.openDialog();
        }
        else {
          if (this.filename != undefined) {
            if (this.filetype == "application/pdf") {
              this.setFileUpload();
            }
            else {
              this.toastrService.show('danger', 'Please select the pdf format', 'Please verify your file format');
            }
          }

          this.sendNominationInfo(this.projectId);

          this.sendmail(this.projectId);
          // this.nominationForm.reset();
          this.toastrService.show('success', 'Success', 'The project successfully nominated');
          this.openDialog();
          location.reload();
        }
      });
    }
  }

  async sendmail(projectId) {
    this.pageService.getPMInfo(projectId.id).subscribe(async res => {
      this.pmInfo = [...res];
      const userinfo = this.rootservice.mdpic;
      const lminfo = this.nominationForm.value.currentManager;
      console.log("pmInfo", this.pmInfo);
      console.log("projectId", projectId);
      console.log("userinfo", userinfo);
      console.log("lminfo", lminfo);
      await this.pageService.sendConsultant(userinfo, projectId, this.nominationForm.value.currentManager).subscribe(async res => {
        await this.pageService.sendPMMessage(this.pmInfo, this.nominationForm.value, projectId, userinfo).subscribe(async res => {
          await this.pageService.sendLMMessage(lminfo, userinfo, projectId).subscribe(res => {
            console.log("--sentmail---", res);
          });
        });
      });
    });
  }

  async sendNominationInfo(projectId) {
    const userid = this.rootservice.mdpic;
    const nomineeinfo = {
      'userId': userid._id,
      'projectId': projectId.id,
      'CurrentManager': this.nominationForm.value.currentManager._id,
      'budget': this.nominationForm.value.budget,
      'comments': this.nominationForm.value.comments
    };
    console.log(nomineeinfo);
    const skills = this.nominationForm.value.skills;

    await this.pageService.sendNomineeinfo(nomineeinfo).subscribe(async (res: any) => {
      //console.log("sendskill",skills);
      await this.pageService.sendNomineeskill(nomineeinfo, skills).subscribe((res: any) => {
        console.log("sendNomineeskill-", res);
        const userid = this.nominationForm.value.currentManager._id;
        const role = "Line Manager";
        //const role = this.nominationForm.value.currentManager.role;
        let data ={
          "projectId": this.projectId.id,
          "userId": userid
        }
        //let isSubmitted = await this.isUserSubmitNomination(data);
        this.pageService.getProjectPrivileges(data).subscribe(res => {
          if(res.length == 0){
            this.pageService.setPrevileges(userid, role, projectId.id).subscribe((res: any) => {
              console.log("setPrevileges-", res);
              this.dialogRef.close();
            });
          }
          else{
            this.dialogRef.close();
          }
        });
      });
    });
  }


  getskilllist(projectid) {
    this.pageService.getSkillList(projectid).subscribe((res: any) => {
      this.projectSkills = [...res];
      //console.log(this.projectSkills);
      const data = { "skills": this.projectSkills }
      this.updateValues(data);
    });

  }

  getLineManagerList() {
    this.pageService.getPMInfo(this.projectId.id).subscribe((res: any) => {
      console.log(res[0]._id);
      this.pageService.getLMList(res[0]._id).subscribe((res: any) => {
        this.lMlist = res;
      });
    });
  }

  setFileUpload() {
    const userid = this.rootservice.mdpic;
    const fileinfo = {
      'id': userid._id,
      'fileName': this.filename,
      'fileType': this.filetype,
      'fileData': this.getdoucmentbyte
    };
    this.pageService.deleteFileinfo(userid._id).subscribe((res: any) => {
      this.pageService.setNominatorResume(fileinfo).subscribe((res: any) => {
      });
    });
  }

  fileChangeEvent(fileInput: any) {
    const reader = new FileReader();
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['application/pdf', '.pdf'];
      const max_height = 300;
      const max_width = 300;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( .pdf )';
        return false;
      }

      reader.onload = (e: any) => {
        const image = new Image();
        this.getdoucmentbyte = e.target.result;
        /** console.log(this.getdoucmentbyte); */
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          console.log(img_height, img_width);


          if (img_height > max_height && img_width > max_width) {
            this.imageError =
              'Maximum dimentions allowed ' +
              max_height +
              '*' +
              max_width +
              'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;
            this.cardImageBase64 = imgBase64Path;
            this.isImageSaved = true;
            // this.previewImagePath = imgBase64Path;
          }
        };
      };

      this.filename = fileInput.target.files[0].name,
        this.filetype = fileInput.target.files[0].type

      reader.readAsDataURL(fileInput.target.files[0]);
    }
    //   removeImage() {
    //     this.cardImageBase64 = null;
    //     this.isImageSaved = false;
    // }

  }

  Clearfile() {
    this.filename = undefined;
    this.filename = undefined;

  }


}
