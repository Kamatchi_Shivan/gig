import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ProjectDataService {

  data = { 
        proBasic: {
            projectName:'',
            workOrderNumber: '',
            projectArea: '',
            customer: '',
            projectDescription: '',
            scopeOfActivity: '',
            inputForProject:'',
            tools:'',
            deliverables:''
        }, 
        proCriteria: {
            startDate: '',
            endDate: '',
            projectBudget: '',
            workPackageStatus: '',
            constrains: '',
            acceptanceCriteria: '',
            skills:[]
        }
    };
  private dataSource = new BehaviorSubject(this.data);
  currentData = this.dataSource.asObservable();
  filetype;
  filename;
  filedata;
  arealist=[];
  constructor() { }

  changeData(newData: any){
    this.dataSource.next(newData); 
  }

}