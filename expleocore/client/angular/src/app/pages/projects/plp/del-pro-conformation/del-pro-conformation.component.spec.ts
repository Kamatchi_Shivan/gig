import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelProConformationComponent } from './del-pro-conformation.component';

describe('DelProConformationComponent', () => {
  let component: DelProConformationComponent;
  let fixture: ComponentFixture<DelProConformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelProConformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelProConformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
