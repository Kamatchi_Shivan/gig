import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'exp-del-pro-conformation',
  templateUrl: './del-pro-conformation.component.html',
  styleUrls: ['./del-pro-conformation.component.css']
})
export class DelProConformationComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DelProConformationComponent>) { }

  ngOnInit(): void {

  }


  cancel(): void {
    this.dialogRef.close(false);
  }

  ok() {
    this.dialogRef.close(true);
  }

}
