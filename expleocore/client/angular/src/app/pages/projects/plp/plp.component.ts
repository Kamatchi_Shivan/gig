import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { NbAlertComponent, NbIconLibraries } from '@nebular/theme';
import { MatDialog } from '@angular/material/dialog';

import { FilterProjectService } from '../../../shared/filter-project/filterProject.service';
import { RootService } from '../../../root.service';
import { SubmitNominationComponent } from '../submit-nomination/submit-nomination.component';
import { PageService } from '../../page-service';
import { Projectinfo } from '../../../models/models';
import { Userdetails } from '../../../models/models';

import { PMReviewNominationComponent } from '../pm-review-nomination/pm-review-nomination.component';
import { LMReviewNominationComponent } from '../lm-review-nomination/lm-review-nomination.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DelProConformationComponent } from './del-pro-conformation/del-pro-conformation.component';
import { ToastrService } from '../../../shared/toastr/toastr.service';

import { Router } from '@angular/router';

import { RolesService } from '../../../shared/roles.service';
@Component({
  selector: 'exp-plp',
  templateUrl: './plp.component.html',
  styleUrls: ['./plp.component.css']
})
export class PlpComponent implements OnInit, OnDestroy, AfterViewInit {
  data: Projectinfo[] = [];
  consultantArray: Userdetails[] = [];
  defaultRole;
  color = 'primary';
  mode = 'indeterminate';
  value = 50;



  avatars = [
    {
      "base64image": 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
        'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
        'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image": 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
        'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
        'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image": 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
        'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
        'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    }, {
      "base64image": 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
        'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
        'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    },
    {
      "base64image": 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyBAMAAADsEZWCAAAAG1BMVEVEeef///+4zPaKq/ChvPPn7' +
        'vxymu3Q3flbieqI1HvuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAQUlEQVQ4jWNgGAWjgP6ASdncAEaiAhaGiACmFhCJLsMaIiDAEQEi0WXYEiMC' +
        'OCJAJIY9KuYGTC0gknpuHwXDGwAA5fsIZw0iYWYAAAAASUVORK5CYII='
    }
  ]
  pageData = [];
  query: string;
  isConsultant: boolean;
  isPM: boolean;
  isLM: boolean;
  isSwitchPM: boolean;
  isSwitchCons: boolean;
  Projectlist: FormGroup;
  isReviewer: boolean;
  loggedUserRole: string;
  loggedUserinfo;
  switchuserinfo;
  userNomination :any = [];
  nominationacceptedproject :any = [];
  selected = 'Project Manager';
  constructor(private formBuilder: FormBuilder,
    private iconLibraries: NbIconLibraries,
    private _proFilterService: FilterProjectService,
    public rootservice: RootService,
    public dialog: MatDialog,
    private pageService: PageService,
    private router:Router,
    private toastrService: ToastrService,
    private _roleService: RolesService
  ) {

    this.iconLibraries.registerFontPack('font-awesome', { iconClassPrefix: 'fas' });
  }

  openReviewDialog(proId, start, end): void {
    let dialogRef: any;
    // if(this.rootservice.mdpic.role
    const getswitchrole = JSON.parse(localStorage.getItem('user'));
    console.log("--->",getswitchrole.role);
    if (getswitchrole.role.toLowerCase() == 'project manager') {
      dialogRef = this.dialog.open(
        PMReviewNominationComponent,
        {
          panelClass: 'icon-outside',
          data: { id: proId },
        }
      );
    }
    else {
      dialogRef = this.dialog.open(
        LMReviewNominationComponent,
        {
          panelClass: 'icon-outside',
          data: { id: proId, startdate: start, enddate: end },
        }
      );
    }

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  dateDiff = function (stDt, edDt) {

    let sd = new Date(stDt);
    let ed = new Date(edDt);
    let obj = { exceeded: false, value: "" };
    //debugger
    if (new Date().getTime() < sd.getTime()) {
      obj.value = 'Not Started'
      return obj;
    }
    else {
      let diff_time = (ed.getTime() - new Date().getTime()) / (1000 * 3600 * 24);
      if (diff_time < 0) {
        obj.value = 'Exceeded';
        obj.exceeded = true;
      }
      else {
        obj.value = Math.round(diff_time) + ' Days left';
      }
      return obj;
    }

  }
  filterProject(filterStatus) {
    if (filterStatus == 'all') {
      this.pageData = this.data;
    }
    else {
      this.pageData = this.data.filter(pro => {
        const wps = pro.workPackageStatus.toLowerCase().replace(" ", "_");
        return wps == filterStatus;
      });
    }

  }
  openDelConformDialog(id): void {
    let proId = id;
    const dialogRef = this.dialog.open(DelProConformationComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.onDeletePro(proId);
    });
  }
  onDeletePro(proId) {
    this.pageService.deleteProject(proId).subscribe((res: any) => {
      this.toastrService.show('success', 'Deleted', 'Project deleted successfully');
      this.ngOnInit();
    });
  }
  openDialog(proId, proname): void {
    const dialogRef = this.dialog.open(
      SubmitNominationComponent,
      {
        panelClass: 'icon-outside',
        data: { id: proId, name: proname },

      }
    );

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-plpcomponent');
  }


  createForm(loggedUserinfo) {
    this.Projectlist = this.formBuilder.group({
      switchrole: [loggedUserinfo.role, Validators.required],

      // password: ['', Validators.required, Validators.maxLength(6)]
    });
  }


  ngOnInit(): void {

    this.loadbodyclass();
    this.loggedUserinfo = JSON.parse(localStorage.getItem('user'));
    this.createForm(this.loggedUserinfo);
    const userrole = this.loggedUserinfo.role;
    this.loggedUserRole = userrole.toLowerCase().replace(" ", "_");
    this._roleService.changeData(this.loggedUserRole);

    switch (this.loggedUserRole) {
      case 'consultant':
        this.isConsultant = true;
        break;
      case 'project_manager':
        this.isPM = true;
        break;
      case 'reviewer':
        this.isReviewer = true;
        break;
      case 'line_manager':
        this.isLM = true;
        break;
    }

    const switchrole = JSON.parse(localStorage.getItem('previousRole'));
    console.log("switchrole:", switchrole)
    if (switchrole == "Project Manager") {
      this.isSwitchPM = true;
    }
    else {
      this.isSwitchPM = false;
    }
    if (switchrole == "Consultant") {
      this.isSwitchCons = true;
    }
    else {
      this.isSwitchCons = false;
    }
    this.getprojectidlist(this.loggedUserinfo);
    this._proFilterService.showProjectFilter(true);
    // this.data = [{"_id":"5f1a870a4b831d1fa09f002a","projectName":"GIG","projectRefNo":"10022155","customerName":"EXPLEO","projectDescription":"project management project","startDate":"2020-07-21T18:30:00.000Z","endDate":"2020-07-28T18:30:00.000Z","workPackageStatus":"Launched","projectStatusPercentage":"50"},{"_id":"5f1a870a4b831d1fa09f0021","projectName":"Test","projectRefNo":"GIG/DES/2020/001","customerName":"Expleo UK","projectDescription":"Project is to create digital data of SA XLR wing components, Project is to create digital data of SA XLR wing components, Project is to create digital data of SA XLR wing components, Project is to create digital data of SA XLR wing components","startDate":"2020-07-23T18:30:00.000Z","endDate":"2020-08-11T18:30:00.000Z","workPackageStatus":"In Progress","projectStatusPercentage":"60"},{"_id":"5f184fc95d0a2212e42f00d9","projectName":"JIJ","projectRefNo":"10022155","customerName":"Airbus","projectDescription":"Project 2","startDate":"2020-07-21T18:30:00.000Z","endDate":"2020-07-28T18:30:00.000Z","workPackageStatus":"Completed","projectStatusPercentage":"50"},{"_id":"5f184fc95d0a2212e42f00d9","projectName":"JIJ","projectRefNo":"10022155","customerName":"Airbus","projectDescription":"Project 2","startDate":"2020-07-21T18:30:00.000Z","endDate":"2020-07-28T18:30:00.000Z","workPackageStatus":"On Hold","projectStatusPercentage":"50"}];


  }

  countProjectType() {
    const filterData = {
      all: this.data.length,
      launched: 0,
      inProgress: 0,
      completed: 0,
      onHold: 0
    }
    this.data.map(pro => {
      const type = pro.workPackageStatus.toLowerCase().replace(" ", "_");
      switch (type) {
        case 'launched':
          filterData.launched++;
          break;
        case 'in_progress':
          filterData.inProgress++;
          break;
        case 'completed':
          filterData.completed++;
          break;
        case 'on_hold':
          filterData.onHold++;
          break;
      }
    })
    return filterData;
  }
  ngOnDestroy() {
    this._proFilterService.showProjectFilter(false);
  }

  ngAfterViewInit() {
    setTimeout (() => {
      this._roleService.changeData(this.loggedUserinfo.role);
   }, 2000);
    
  }
  geteditProject(projectid) : any{
    this.pageService.GetprojectNominationAccpeted(projectid).subscribe((res: any) => {
      this.nominationacceptedproject=(res);
      if(this.nominationacceptedproject.length>0){
        this.toastrService.show('warning', 'Warning', 'Collaborator is already is selected');
      }
      else{
      this.router.navigate(['/pages/project-review'],{queryParams: {'id': projectid ,'isEditable':true }});
      }    
    });
    ///**[routerLink]="['/pages/project-review']"/**[queryParams]="{ id: pro._id,isEditable:true }"*/
  }

  async getprojectidlist(loggedUserinfo) {
    const userid = loggedUserinfo;
    console.log(userid.role);
    console.log(this.loggedUserinfo._id);
    var projectdata = [];
    await  this.pageService.GetAllprojectNomination(this.loggedUserinfo._id).subscribe((res: any) => {
      this.userNomination=(res);
});
    if (userid.role == "Consultant") {
      await this.pageService.CheckswitchroleConsultant(this.loggedUserinfo._id).subscribe(async (res: any) => {
        if (res.length > 0) {
          await this.pageService.ProjectListForConsultant(res).subscribe((res: any) => {
            var i: number;
            this.data = [...res];
            for (i = 0; i < this.data.length; i++) {
              this.consultantArray = [];
              /***double Nomination Checking***/
              var i: number;
             
            //  console.log("id--", this.data[i]._id);
              /**
              * Consultant images for projectlist. functionality done but commanded due to performance issue.
              */
              // this.pageService.getConsultant(this.data[i]._id).subscribe((res:any) => {
              //   console.log("array-",res);
              //   this.consultantArray =[...res];
              //   console.log("this.consultantpic--",this.consultantArray);
              //   console.log("set--",this.consultantArray);
              // });
              let dateObj = this.dateDiff(this.data[i].startDate, this.data[i].endDate);
              const projectdetails = {
                '_id': this.data[i]._id,
                'projectName': this.data[i].projectName,
                'projectRefNo': this.data[i].projectRefNo,
                'customerName': this.data[i].customerName,
                'projectDescription': this.data[i].projectDescription,
                'time_diff': dateObj.value,
                'isExceeded': dateObj.exceeded,
                'startDate': this.data[i].startDate,
                'endDate': this.data[i].endDate,
                'consultant': "",
                'workPackageStatus': this.data[i].workPackageStatus,
                'projectStatusPercentage': this.data[i].projectStatusPercentage,
              };
              projectdata.push(projectdetails);
            }
            this.data = [...projectdata];
            this.pageData = [...projectdata];
            const filterData = this.countProjectType();
            this._proFilterService.changeData(filterData);
            // this._proFilterService.showProjectFilter(true);
            this._proFilterService.getFilterType().subscribe((param: any) => {
              if (param !== undefined) {
                this.filterProject(param);
              }
            });

          });

        }
        else {
          await this.pageService.GetAllprojectInfo().subscribe((res: any) => {
            var i: number;
            console.log("1111");
            this.data = [...res];
            var u :number;
            var nomination_value: string;
            nomination_value = "false";
            for (i = 0; i < this.data.length; i++) {
              this.consultantArray = [];
              console.log("usernoination");
              console.log(this.userNomination);
              console.log("usersdata");
              console.log(this.data);
              for (u = 0; u < this.userNomination.length; u++)
              {
                nomination_value=(((this.userNomination[u].projectId  ==  this.data[i]._id))   ? "true" :"false");
                if((this.userNomination[u].projectId  ==  this.data[i]._id))
                {
                   break;
                }
              }
              //console.log("id--", this.data[i]._id);
              /**
              * Consultant images for projectlist. functionality done but commanded due to performance issue.
              */
              // this.pageService.getConsultant(this.data[i]._id).subscribe((res:any) => {
              //   console.log("array-",res);
              //   this.consultantArray =[...res];
              //   console.log("this.consultantpic--",this.consultantArray);
              //   console.log("set--",this.consultantArray);
              // });
              let dateObj = this.dateDiff(this.data[i].startDate, this.data[i].endDate);
              const projectdetails = {
                '_id': this.data[i]._id,
                'projectName': this.data[i].projectName,
                'projectRefNo': this.data[i].projectRefNo,
                'customerName': this.data[i].customerName,
                'projectDescription': this.data[i].projectDescription,
                'time_diff': dateObj.value,
                'isExceeded': dateObj.exceeded,
                'startDate': this.data[i].startDate,
                'endDate': this.data[i].endDate,
                'consultant': "",
                'workPackageStatus': this.data[i].workPackageStatus,
                'projectStatusPercentage': this.data[i].projectStatusPercentage,
                'nomination' : nomination_value,
              };
              projectdata.push(projectdetails);
            }

            this.data = [...projectdata];
            this.pageData = [...projectdata];
            const filterData = this.countProjectType();
            this._proFilterService.changeData(filterData);
            // this._proFilterService.showProjectFilter(true);
            this._proFilterService.getFilterType().subscribe((param: any) => {
              if (param !== undefined) {
                this.filterProject(param);
              }
            });

          });
        }

      
      });

    }
    else if (userid.role == "Project Manager") {
    

      this.pageService.getprojectid(userid._id).subscribe((res: any) => {
        this.pageService.getprojectlistdetails(res).subscribe((res: any) => {
          var i: number;
          var p: number;
          var projectdata = [];
          this.data = [...res];
          for (i = 0; i < this.data.length; i++) {
            this.consultantArray = [];

          

           // console.log("id--", this.data[i]._id);
            /**
            * Consultant images for projectlist. functionality done but commanded due to performance issue.
            */
            // this.pageService.getConsultant(this.data[i]._id).subscribe((res:any) => {
            //   console.log("array-",res);
            //   this.consultantArray =[...res];
            //   console.log("this.consultantpic--",this.consultantArray);
            //   console.log("set--",this.consultantArray);
            // });
            let dateObj = this.dateDiff(this.data[i].startDate, this.data[i].endDate);
            const projectdetails = {
              '_id': this.data[i]._id,
              'projectName': this.data[i].projectName,
              'projectRefNo': this.data[i].projectRefNo,
              'customerName': this.data[i].customerName,
              'projectDescription': this.data[i].projectDescription,
              'time_diff': dateObj.value,
              'isExceeded': dateObj.exceeded,
              'startDate': this.data[i].startDate,
              'endDate': this.data[i].endDate,
              'consultant': "",
              'workPackageStatus': this.data[i].workPackageStatus,
              'projectStatusPercentage': this.data[i].projectStatusPercentage,
            };
            projectdata.push(projectdetails);
            console.log(projectdata);
          }
          // debugger
          console.log(res);
          this.data = [...projectdata];
          this.pageData = [...projectdata];
          console.log(this.data);

          //this.pageData = [...this.data];

          const filterData = this.countProjectType();

          this._proFilterService.changeData(filterData);
          // this._proFilterService.showProjectFilter(true);
          this._proFilterService.getFilterType().subscribe((param: any) => {
            if (param !== undefined) {
              this.filterProject(param);
            }
          });
        });

      });
    }
    else if (userid.role == "Line Manager" || userid.role == "Reviewer") {
      this.pageService.checkSwitchRolevalid(this.loggedUserinfo, userid.role).subscribe((res: any) => {
        console.log("rolestatus--", res);
        this.pageService.getprojectlistdetails(res).subscribe((res: any) => {
          // console.log(res);
          // this.data =[...res];
          // this.pageData =[...this.data];

          var i: number;
          var projectdata = [];
          this.data = [...res];
          for (i = 0; i < this.data.length; i++) {
            this.consultantArray = [];
            console.log("id--", this.data[i]._id);
            /**
            * Consultant images for projectlist. functionality done but commanded due to performance issue.
            */
            // this.pageService.getConsultant(this.data[i]._id).subscribe((res:any) => {
            //   console.log("array-",res);
            //   this.consultantArray =[...res];
            //   console.log("this.consultantpic--",this.consultantArray);
            //   console.log("set--",this.consultantArray);
            // });
            let dateObj = this.dateDiff(this.data[i].startDate, this.data[i].endDate);
            const projectdetails = {
              '_id': this.data[i]._id,
              'projectName': this.data[i].projectName,
              'projectRefNo': this.data[i].projectRefNo,
              'customerName': this.data[i].customerName,
              'projectDescription': this.data[i].projectDescription,
              'time_diff': dateObj.value,
              'isExceeded': dateObj.exceeded,
              'startDate': this.data[i].startDate,
              'endDate': this.data[i].endDate,
              'consultant': "",
              'workPackageStatus': this.data[i].workPackageStatus,
              'projectStatusPercentage': this.data[i].projectStatusPercentage,
            };
            projectdata.push(projectdetails);
          }
          // debugger
          console.log(res);
          this.data = [...projectdata];
          this.pageData = [...projectdata];
          // console.log(this.data);

          const filterData = this.countProjectType();

          this._proFilterService.changeData(filterData);
          // this._proFilterService.showProjectFilter(true);
          this._proFilterService.getFilterType().subscribe((param: any) => {
            if (param !== undefined) {
              this.filterProject(param);
            }
          });
        });

      });
    }
  }

  onSwitchrole(role) {
    let selectedrole = role.value;
    console.log(selectedrole);
    //this._roleService.changeData(selectedrole);
    this.loggedUserinfo = JSON.parse(localStorage.getItem('user'));
    // this.pageService.checkSwitchRolevalid(this.loggedUserinfo,selectedrole).subscribe((res: any) => {
    //   console.log("rolestatus--",res);
    //   this.pageService.getprojectlistdetails(res).subscribe((res:any) => {
    //     console.log(res);
    //     this.data =[...res];
    //     this.pageData =[...this.data];
    //     const filterData = this.countProjectType();

    //     this._proFilterService.changeData(filterData);
    //    // this._proFilterService.showProjectFilter(true);
    //     this._proFilterService.getFilterType().subscribe((param: any) => {
    //       if (param !== undefined) {
    //         this.filterProject(param);
    //       }
    //     });
    //   });

    // });

    const onChangeRole = {
      '_id': this.loggedUserinfo._id,
      'username': this.loggedUserinfo.username,
      'status': this.loggedUserinfo.status,
      'role': selectedrole,
      'phone': this.loggedUserinfo.phone,
      'name': this.loggedUserinfo.name,
      'employeeid': this.loggedUserinfo.employeeid,
      'email': this.loggedUserinfo.email,
      'domain': this.loggedUserinfo.domain
    }
    console.log("onChangeRole--", onChangeRole);
    localStorage.setItem('user', JSON.stringify(onChangeRole));
    location.reload();
    // const getswitchrole =JSON.parse(localStorage.getItem('user'))
    // this.loggedUserinfo = getswitchrole;
    // console.log("checkrole",this.loggedUserinfo.role);
    // this.loggedUserRole = this.loggedUserinfo.role.toLowerCase().replace(" ", "_");
    // switch (this.loggedUserRole) {
    //   case 'consultant':
    //         this.isConsultant = true;
    //         break;
    //   case 'project_manager':
    //         this.isPM = true;
    //         break;
    //   case 'reviewer':
    //         this.isReviewer = true;
    //         break;
    //   case 'line_manager':
    //         this.isLM = true;
    //         break;
    // }


    // this._proFilterService.showProjectFilter(true);

  }

}
