import { NgModule } from '@angular/core';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { 
          NbCardModule,
          NbButtonModule,
          NbIconModule,
          NbProgressBarModule,
          NbInputModule,
          NbFormFieldModule,
          NbUserModule 
        } from '@nebular/theme';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';

import { CommonModule } from '@angular/common';


import { FilterProject } from './project-filter.pipe';
import { PlpComponent } from './plp.component';
import { ProgressSpinnerModule } from '../../../shared/progress-spinner/progress-spinner.module';
import { AppOverlayModule } from '../../../shared/overlay/overlay.module';
import { DelProConformationComponent } from './del-pro-conformation/del-pro-conformation.component';


const materialModules = [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatChipsModule,
    MatTooltipModule,
    MatMenuModule,
    MatProgressSpinnerModule
  ];
  
  @NgModule({
    imports: [
      NbCardModule,
      NbIconModule,
      NbButtonModule,
      NbInputModule,
      NbFormFieldModule,
      NbUserModule,
      ReactiveFormsModule,
      CommonModule,
      FormsModule,
      RouterModule,
      NbProgressBarModule,
      ...materialModules
    ],
    declarations: [
        PlpComponent,
        FilterProject,
        DelProConformationComponent
    ]
  })
  export class PlpModule {
      
  }