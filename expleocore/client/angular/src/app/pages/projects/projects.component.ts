import { Component, OnInit } from '@angular/core';
import { Project } from '../../models/models';
import { MatDialog } from '@angular/material/dialog';

import { SubmitNominationComponent } from './submit-nomination/submit-nomination.component';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'exp-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit {
  project: Project;
  sortMetaData = [
    { label: 'Progress', value: 'progress' },
    { label: 'Created Time', value: 'created' },
    { label: 'Last Active', value: 'updated' },
  ];

  sortBy: string = 'created';

  projects: Project[];

  constructor(public dialog: MatDialog, private activatedRoute:ActivatedRoute) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(SubmitNominationComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  ngOnInit() {
    this.activatedRoute
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        //this.page = +params['page'] || 0;
        console.log(params)
      });
  }

}
