import { Component, OnInit, ViewChild, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { PageService } from '../../page-service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { resetFakeAsyncZone } from '@angular/core/testing';

@Component({
  selector: 'exp-mattable',
  templateUrl: './mattable.component.html',
  styleUrls: ['./mattable.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class MattableComponent implements OnInit {

  @ViewChild('outerSort', { static: true }) sort: MatSort;
  @ViewChildren('innerSort') innerSort: QueryList<MatSort>;
  @ViewChildren('innerTables') innerTables: QueryList<MatTable<Address>>;

  dataSource: MatTableDataSource<User>;
  projectInfo;
  projectRefNo;
  length: number;
  loggedUserinfo;
  projectId;
  loggedUserRole;
  isConsultant: boolean;
  isPM: boolean;
  isLM: boolean;
  isReviewer: boolean;
  schedulerData: User[] = [];
  columnsToDisplay = [{
    'title' : 'Edit',
    'datafield' : 'edit'
  },
  {
    'title' : 'Work Order Num',
    'datafield' :'workOrderNumber'
  },
  {
    'title' : 'Activity Ref',
    'datafield' :'activityReference'
  },
  {
    'title' : 'Milestone',
    'datafield' :'milestone'
  },
  {
    'title' : 'Launching Date',
    'datafield' :'launchingDate'
  },
  {
    'title' : 'Expected Delivery Date',
    'datafield' :'expectedDeliveryDate'
  },
  {
    'title' : 'Activity Start Date',
    'datafield' :'activityStartDate'
  },
  {
    'title' : 'Proposed Delivery Date',
    'datafield' :'proposedDeliveryDate'
  },
  {
    'title' : 'Actual Delivery Date',
    'datafield' :'actualDeliveryDate'
  },
  {
    'title' : 'Satus',
    'datafield' :'status'
  },
  {
    'title' : 'Action',
    'datafield' :'action'
  },
  {
    'title' : 'OTD',
    'datafield' :'otd'
  },
  {
    'title' : 'OQD',
    'datafield' :'oqd'
  }
  ];
  displayedColumnsKeys = [];
  //innerDisplayedColumns = ['revStDate', 'revEndDate', 'febaDate', 'delDateAfRework'];
  innerDisplayedColumns = [
    {
      'title' : 'Review Start Date',
      'datafield' :'revStDate'
    },
    {
      'title' : 'Review End Date',
      'datafield' :'revEndDate'
    },
    {
      'title' : 'Feedback Date',
      'datafield' :'febaDate'
    },
    {
      'title' : 'Delivery date after rework',
      'datafield' :'delDateAfRework'
    }
  ];
  innerDisplayedColumnsKeys =  [];
  expandedElement: User | null;
  constructor(private activateRoute: ActivatedRoute, private service: PageService, private cd: ChangeDetectorRef, private router: Router) { }
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageSize = 10;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  dataSourceLength: any;
  ngOnInit(): void {

    this.activateRoute.queryParams.subscribe(params => {
      this.projectId = params['pid'];
    })
    
    this.displayedColumnsKeys = this.columnsToDisplay.map(col => col.datafield);
    this.innerDisplayedColumnsKeys = this.innerDisplayedColumns.map(col => col.datafield);
    this.loggedUserinfo = JSON.parse(localStorage.getItem('user'));
    this.loggedUserRole =this.loggedUserinfo.role.toLowerCase().replace(" ", "_");
    switch (this.loggedUserRole) {
      case 'consultant':
        this.isConsultant = true;
        break;
      case 'project_manager':
        this.isPM = true;
        break;
      case 'reviewer':
        this.isReviewer = true;
        break;
      case 'line_manager':
        this.isLM = true;
        break;
    }

    console.log("this.projectId", this.projectId);
    this.schedulerGetDeatils(this.projectId);
  }
  onEditScheduler($event, tableData) {
    $event.stopPropagation();
    console.log(tableData);
    this.router.navigate(
      ['/pages/scheduler-modify-details-component'], { queryParams: tableData, skipLocationChange: true });
  }
  getRecord(name) {
    console.log("test", name);
  }

  createScheduler() {
    this.router.navigate(['/pages/scheduler-modify-details-component'], { queryParams: { 'project': this.projectId }, skipLocationChange: true });
  }


  backToProjectDetails() {
   // this.router.navigate(['/pages/project-details'], { queryParams: { id: this.projectId } });
   window.history.back();
  };

  schedulerGetDeatils(projectInfo: any) {
    console.log(projectInfo);
    this.service.schedulertestGetDeatils(projectInfo).subscribe((res: any) => {
      console.log("rs",res);
      if (res.length == 0) {
        this.createScheduler();
      }
      else {
        this.dataSourceLength = res.length;
        const USERS: User[] = [...res];
        console.log("this.USERS", USERS);

        USERS.forEach(user => {
          if (user.reviewInfo && Array.isArray(user.reviewInfo) && user.reviewInfo.length) {
            this.schedulerData = [...this.schedulerData, { ...user, reviewInfo: new MatTableDataSource(user.reviewInfo) }];
          } else {
            this.schedulerData = [...this.schedulerData, user];
          }
        });
        console.log("this.dataSource", this.schedulerData);
        this.dataSource = new MatTableDataSource(this.schedulerData);
        this.dataSource.paginator =  this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  toggleRow(element: User) {
    element.reviewInfo && (element.reviewInfo as MatTableDataSource<Address>).data?.length ? (this.expandedElement = this.expandedElement === element ? null : element) : null;
    this.cd.detectChanges();

    console.log("table.dataSource", element.reviewInfo);
    this.innerTables.forEach((table, index) => (table.dataSource as MatTableDataSource<Address>).sort = this.innerSort.toArray()[index]);
    console.log("this.innerTables", this.innerTables);
  }

  applyFilter(filterValue: string) {
    this.innerTables.forEach((table, index) => (table.dataSource as MatTableDataSource<Address>).filter = filterValue.trim().toLowerCase());
  }

}

export interface User {
  _id: string;
  workOrderNumber: string;
  projectId: string;
  activityReference: string;
  milestone: string;
  launchingDate: string;
  expectedDeliveryDate: string;
  activityStartDate: string;
  proposedDeliveryDate: string;
  actualDeliveryDate: string;
  milestoneStatus: string;
  status: string;
  otd: string;
  oqd: string;
  action: string;
  reviewInfo?: Address[] | MatTableDataSource<Address>;
}

export interface Address {
  revStDate: string;
  revEndDate: string;
  febaDate: string;
  delDateAfRework: string;
}

export interface UserDataSource {
  workOrderNumber: string;
  projectId: string;
  activityReference: string;
  milestone: string;
  launchingDate: string;
  expectedDeliveryDate: string;
  activityStartDate: string;
  proposedDeliveryDate: string;
  actualDeliveryDate: string;
  milestoneStatus: string;
  otd: string;
  oqd: string;
  action: string;
  reviewInfo?: MatTableDataSource<Address>;
}