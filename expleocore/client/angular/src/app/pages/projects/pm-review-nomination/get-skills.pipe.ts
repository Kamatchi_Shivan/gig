import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({
    name: 'getSkills',
    pure: true
})
export class GetSkillsPipe implements PipeTransform {
    transform(skills: any, args?: any): any {
        return skills.map(cd => cd.skillName).join(",");;
    }
}