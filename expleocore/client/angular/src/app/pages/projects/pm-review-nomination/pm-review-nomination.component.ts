import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { RootService } from '../../../root.service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { NbIconLibraries } from '@nebular/theme';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpHeaders } from '@angular/common/http';
import { PageService } from '../../page-service';

import { Observable, forkJoin } from 'rxjs';
@Component({
  selector: 'exp-pm-review-nomination',
  templateUrl: './pm-review-nomination.component.html',
  styleUrls: ['./pm-review-nomination.component.scss']
})
export class PMReviewNominationComponent implements OnInit, AfterViewInit {
  statusArr = [
    { value: 'accept', viewvalue: 'Accept' },
    { value: 'reject', viewvalue: 'Reject' },
    { value: 'onhold', viewvalue: 'Onhold' }
  ];
  displayedColumns: any[] = ["select", "name", "skills", "rating", "resume", "status", "comment"];
  dataSource: MatTableDataSource<any>;
  length: number;
  pageSize = 10;
  downloadFile: any;
  projectskills = [];
  loggedUserinfo;
  data = [];
  projectWeighted: number = 0;
  nomineeWeighted: number = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  selection = new SelectionModel<any>(true, []);
  constructor(
    @Inject(MAT_DIALOG_DATA) public projectId: any,
    private iconLibraries: NbIconLibraries,
    public rootservice: RootService,
    public dialogRef: MatDialogRef<PMReviewNominationComponent>, private pageService: PageService, private toastrService: ToastrService, private router: Router, public dialog: MatDialog) {
    this.iconLibraries.registerFontPack('font-awesome', { iconClassPrefix: 'fas' });
  }

  ngOnInit(): void {
    this.getnomineelist(this.projectId);
  }

  ngAfterViewInit() {
    
    // this.length = data.length;
    // this.dataSource = new MatTableDataSource(data);
  }
  getSkillSet(skills) {
    return skills.map(cd => cd.skillName).join(",");
  }

  // async getnomineelist(projectId) {
  //   console.log("projectId2", this.projectId);
  //   await this.pageService.getSkillList(projectId.id).subscribe(async(res: any) => {
  //     // console.log("projectskills", res);
  //     this.projectskills = res;
  //   });


  //   await this.pageService.getLMApprovedNomineeList(projectId.id).subscribe(async (res: any) => {
  //     this.data = res;
  //     this.data = [...res];
  //     console.log("NomineeList--", this.data.length);
  //     this.getresults(this.data);

  //   });
  // }

  getnomineelist(projectId){
    let data = {
      "projectId": projectId.id
    }
    let proSkill = this.pageService.getSkillList(projectId.id);
    let nominess = this.pageService.getLMApprovedNominees(data);
    let pmReviewStatus = this.pageService.getPMReview(data);
    forkJoin([proSkill, nominess, pmReviewStatus]).subscribe(results => {
      // results[0] is our character
      // results[1] is our character homeworld
      this.projectskills = results[0];
      let user = results[1];
      let pmReview = results[2];
 
      this.pageService.getLMApprovedUser(user).subscribe((res: any) => {
        
        let data= [...res];
        for(let i=0;i<user.length;i++){
          user[i].name = res[i].name;
          user[i].email = res[i].email;
          let userId = user[i].userId;
          let pmComm = pmReview.filter(i => { return i.userId == userId });
          console.info("---->PMCOOM",pmComm)
          if(pmComm.length!=0){
            if(pmComm[0].reviewStatus.toLowerCase() == "accept"){
              user[i].status = user[i].pmReviewStatus.toLowerCase();
              user[i].comment = pmComm[0].comments;
            }
            else{
              user[i].status = pmComm[0].reviewStatus.toLowerCase();
              user[i].comment = pmComm[0].comments;
            }
          }
          else{
            user[i].status = '';
            user[i].comment = '';
          }
        }
        console.log("data-",user);
        this.getresults(user);
      });
      
    });

  }
  async getresults(data) {
    console.log("data--,", data);
    var projectdata = [];
    var i: number;
    var j: number;

    for (i = 0; i < data.length; i++) {
      var skills = [];
      const NomineeListdata = data[i];
      const projectskills = this.projectskills;
      console.log("dprojectskills--,", projectskills);
      let res = await this.getTableData(this.projectId.id, data[i].userId);
      skills =  res.nomineeSkill;
        const filedata = res.fileInfo;
        console.log("filedata---", filedata);
        console.log("filedata---", filedata);
        for (j = 0; j < projectskills.length; j++) {
          this.projectWeighted = this.projectWeighted + projectskills[j].skillWeightage * projectskills[j].skillRating;
          this.nomineeWeighted = this.nomineeWeighted + projectskills[j].skillWeightage * skills[j].skillRating;
        }
        const skillrating = this.projectWeighted / this.nomineeWeighted;

        console.log("skillrating---", skillrating);
        
        const skillsdetails = {
          '_id': NomineeListdata._id,
          'userId' : NomineeListdata.userId,
          'name': NomineeListdata.name,
          'email': NomineeListdata.email,
          'skills': skills,
          'resume': filedata,
          'rating': skillrating.toFixed(2),
          'picture': NomineeListdata.profilepic,
          'status' : NomineeListdata.status.toLowerCase(),
          'comment' : NomineeListdata.comment
        };
  
        projectdata.push(skillsdetails);
      
    }
 


      this.length = projectdata.length;
      this.dataSource = new MatTableDataSource(projectdata);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    console.log("finaldata--", projectdata);
  }


  getTableData(proId,userId): Promise<any> {
    return new Promise((resolve, reject) => {
      this.pageService.getNomineeRatingSkills(proId,userId).subscribe((res: any) => {
        resolve(res);
      });  
    });
  }

  download(file) {
    console.log(file);
    const linkSource = 'data:application/pdf;base64,' + file.fileData;
    const downloadLink = document.createElement("a");

    downloadLink.href = linkSource;
    downloadLink.download = file.fileName;
    downloadLink.click();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  /**
   * update selected users list start
   */
  async updateSelectedUsersList() {
    if (this.selection.selected.length > 0) {
      const tableData = this.selection.selected;
      let array = [];
      let date = new Date().getTime();
      for (let i = 0; i < tableData.length; i++) {

        if(tableData[i].status != 'accepted'){
          this.loggedUserinfo = JSON.parse(localStorage.getItem('user'));
          array.push({
            "userId": tableData[i].userId,
            'projectId': this.projectId.id,
            "nomineename": tableData[i].name,
            "reviewStatus": tableData[i].status,
            'cumulativeRating': tableData[i].rating,
            "comments": tableData[i].comment,
            "currentpmId": this.loggedUserinfo._id,
            "pmName": this.loggedUserinfo._id,
            "pmMailId": this.loggedUserinfo._id,
            "NomineeMailId": tableData[i].email,
          });
        }
      }
      console.log(array);

      await this.pageService.setPMReleaseNominee(array).subscribe(async (res: any) => {
        console.log("insert--", res);
        await this.pageService.setPMReleasestatus(array).subscribe(async (res: any) => {
          console.log("updatestatus--", res);
          await this.pageService.setcurrentPMId(array).subscribe(async (res: any) => {
            this.toastrService.show('success', 'Success', 'Selected Nominees has been approved');
            this.openDialog();
            await this.pageService.sendPMapprovalMessage(array).subscribe((res: any) => {
            
            });

          });
        });

      });


    } else {
      this.toastrService.show('danger', "failed", 'please check the table');
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.closeAll();
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  logSelection() {
    this.selection.selected.forEach(s => console.log(s.name));
  }

  /**
   * filter table start
  */
  applyFilter(value: String) {
    // const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = value.trim().toLowerCase();
  }

}
