import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NbCardModule, NbUserModule, NbButtonModule } from '@nebular/theme';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { CommonModule } from '@angular/common';



import { PMReviewNominationComponent } from './pm-review-nomination.component';
import { GetSkillsPipe } from './get-skills.pipe';
const materialModules = [
  MatTableModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule
  ];
  
  @NgModule({
    imports: [
      NbCardModule,
      NbUserModule,
      CommonModule,
      FormsModule,
      NbButtonModule,
      ...materialModules
    ],
    declarations: [
        PMReviewNominationComponent,
        GetSkillsPipe
    ]
  })
  export class PMReviewNominationModule {
      
  }