import { Component, OnInit, ViewChild, AfterViewInit,Inject } from '@angular/core';
import { RootService } from '../../../root.service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {SelectionModel} from '@angular/cdk/collections';
import { NbIconLibraries } from '@nebular/theme';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PageService } from '../../page-service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Observable, forkJoin } from 'rxjs';
@Component({
  selector: 'exp-lm-review-nomination',
  templateUrl: './lm-review-nomination.component.html',
  styleUrls: ['./lm-review-nomination.component.scss']
})
export class LMReviewNominationComponent implements OnInit, AfterViewInit {
  statusArr = [
    { value: 'accept', viewvalue: 'Accept' },
    { value: 'reject', viewvalue: 'Reject' },
    { value: 'onhold', viewvalue: 'Onhold' }
  ];
  displayedColumns: any[] = ["select","name","startDate","endDate","status","comment"];
  dataSource: MatTableDataSource<any>;
  length:number;
  pageSize = 10;
  startDate;
  endDate;
  pmInfo=[];
  loggedUserinfo;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  selection = new SelectionModel<any>(true, []);
  constructor(
    @Inject(MAT_DIALOG_DATA) public projectId: any,
    private iconLibraries: NbIconLibraries, 
    public rootservice: RootService,
    public dialogRef: MatDialogRef<LMReviewNominationComponent>,private pageService: PageService, private toastrService: ToastrService,private router: Router,public dialog: MatDialog) { 
    this.iconLibraries.registerFontPack('font-awesome', { iconClassPrefix: 'fas' });
  }

  ngOnInit(): void {
    console.log("projectIdLM",this.projectId);
    this.startDate =this.projectId.startdate;
    this.endDate =this.projectId.enddate;
    this.getnomineelist(this.projectId);

  }
  ngAfterViewInit(){
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    // this.length = data.length;
    // this.dataSource = new MatTableDataSource(data);
  }

  // getnomineelist(projectId)
  // {
  //   this.pageService.getNomineeList(projectId.id).subscribe((res: any) => {
  //     let data= [...res];
  //     console.log("data-",data);
  //     this.length = data.length;
  //     this.dataSource = new MatTableDataSource(data);
  //     });
  // }
  getnomineelist(projectId){
    let data = {
      "projectId": projectId.id
    }
    let nominess = this.pageService.getNominees(data);
    let lmReviewStatus = this.pageService.getLMReview(data);
    forkJoin([nominess, lmReviewStatus]).subscribe(results => {
      let user = results[0];
      let lmReview = results[1];
      this.pageService.getNominatedUser(user).subscribe((res: any) => {
        let data= [...res];
        for(let i=0;i<user.length;i++){
          user[i].name = res[i].name;
          user[i].email = res[i].email;
          let userId = user[i].userId;
          let lmComm = lmReview.filter(i => { return i.userId == userId });
          console.log(lmComm);
          console.log(user);

          if(lmComm.length!=0){
            if(lmComm[0].releaseStatus.toLowerCase() == "accept"){
              user[i].status = user[i].approvedstatus.toLowerCase();
              user[i].comment = lmComm[0].comments;
            }
            else{
              user[i].status = lmComm[0].releaseStatus.toLowerCase();
              user[i].comment = lmComm[0].comments;
            }
          }
          else{
            user[i].status = '';
            user[i].comment = '';
          }

          // user[i].status = user[i].approvedstatus.toLowerCase();
          // user[i].comment = user[i].Comments;

        }
        console.log("data-",user);
        this.length = user.length;
        this.dataSource = new MatTableDataSource(user);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });
    this.pageService.getNominees(data).subscribe((user: any) => {

    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  /**
   * update selected users list start
   */
  async updateSelectedUsersList() {
    if (this.selection.selected.length > 0) {
      const tableData = this.selection.selected;
      let array = [];
      let date = new Date().getTime();

      this.pmInfo = await  this.getPMInfo(this.projectId.id);

      for (let i = 0; i < tableData.length; i++) {
        if(tableData[i].status != 'accepted'){
          this.loggedUserinfo = JSON.parse(localStorage.getItem('user'));
          array.push({
            "userId": tableData[i].userId,
            "projectId":this.projectId.id,
            "nomineename" :tableData[i].name,
            "NomineeMailId":tableData[i].email,
            "releaseStatus": tableData[i].status,
            "comments": tableData[i].comment,
            "lmName":this.loggedUserinfo._id,
            "lmMailId":this.loggedUserinfo.email,
            "pmMaild":this.pmInfo[0].email,
            "pmname":this.pmInfo[0].name,
          });
        }
        
      }

      console.log("input",array);
      await  this.pageService.setReleaseNominee(array).subscribe(async (res: any) => {
          console.log("insert--",res);
          await  this.pageService.setReleasestatus(array).subscribe(async (res: any) => {
            console.log("updatestatus--",res);
            this.toastrService.show('success', 'Success', 'Selected Nominees has been approved');
            this.openDialog();
            await  this.pageService.sendLMapprovalMessage(array).subscribe((res: any) => {
     
          });
        });
      });

    } else {
      this.toastrService.show('danger', "failed", 'Please check the details');
    }
  }

  getPMInfo(proId): Promise<any> {
    return new Promise((resolve, reject) => {
      this.pageService.getPMInfo(proId).subscribe(res => {
        resolve(res);
      });
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  logSelection() {
    this.selection.selected.forEach(s => console.log(s.name));
  }

  /**
   * filter table start
  */
  applyFilter(value: String) {
    // const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = value.trim().toLowerCase();
  }
  openDialog(): void {
    const dialogRef = this.dialog.closeAll();
  }
}
