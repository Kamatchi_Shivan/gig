import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NbCardModule, NbUserModule, NbButtonModule } from '@nebular/theme';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { CommonModule } from '@angular/common';

import { LMReviewNominationComponent } from './lm-review-nomination.component';

const materialModules = [
  MatTableModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule
  ];
  
  @NgModule({
    imports: [
      NbCardModule,
      NbUserModule,
      NbButtonModule,
      CommonModule,
      FormsModule,
      ...materialModules
    ],
    declarations: [
        LMReviewNominationComponent
    ]
  })
  export class LMReviewNominationModule {
      
  }