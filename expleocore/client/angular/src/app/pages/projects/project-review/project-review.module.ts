import { NgModule } from '@angular/core';
import { ReactiveFormsModule,FormGroup } from '@angular/forms';

import { NbCardModule } from '@nebular/theme';

import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';

import { CommonModule } from '@angular/common';
import { ProjectReviewComponent } from './project-review.component';

import { SharedModule } from '../../../shared/shared.module';


const materialModules = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatButtonModule,
  MatDialogModule,
  MatTooltipModule
];

@NgModule({
  imports: [
    NbCardModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    ...materialModules
  ],
  declarations: [
    ProjectReviewComponent
  ]
})
export class ProjectReviewModule {
    
}
