import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { ProjectDataService } from '../projectData.service';
import { PageService } from '../../page-service';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { RootService } from '../../../root.service';
import { Area } from '../../../models/models';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { ProjectSuccessComponent } from '../project-success/project-success.component';

@Component({
  selector: 'exp-project-review',
  templateUrl: './project-review.component.html',
  styleUrls: ['./project-review.component.css']
})
export class ProjectReviewComponent   {
  //date constarian
  minDate: Date = new Date();
  currProCreationData: any;
  //file attributes 
  projectNameValue: string;
  selectedFiles;
  projectInfo;
  fileInfo;
  skillinfo=[];
  projectid;
  editable_view;
  filename;
  name;
  filedata;
  filetype;
  getAreaList;
  Download;
  getdoucmentbyte;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  area: Area;
  //form object
  proReviewForm: FormGroup;

  //input file controls
  projectName: FormControl;
  workOrderNumber: FormControl;
  projectArea: FormControl;
  customer: FormControl;
  projectDescription: FormControl;
  scopeOfActivity: FormControl;
  inputForProject: FormControl;
  tools: FormControl;
  deliverables: FormControl;
  startDate: FormControl;
  endDate: FormControl;
  projectBudget: FormControl;
  workPackageStatus: FormControl;
  constrains: FormControl;
  acceptanceCriteria: FormControl;
  skills: FormArray;
  cumulativeWeightInvalid:boolean = false;
  projectreviewdisable:boolean =false;
  cumulGreater = false;


  //creates the form input controlls 
  createFormControls() {
    this.projectName = new FormControl("", Validators.required);
    this.workOrderNumber = new FormControl("", Validators.required);
    this.projectArea = new FormControl("", Validators.required);
    this.customer = new FormControl("", [Validators.required,Validators.pattern('^[a-zA-Z ]*$')]);
    this.projectDescription = new FormControl("", Validators.required);
    this.scopeOfActivity = new FormControl("", Validators.required);
    this.inputForProject = new FormControl("", Validators.required);

    this.tools = new FormControl("", Validators.required);
    this.deliverables = new FormControl("", Validators.required);

    this.startDate = new FormControl("", Validators.required);
    this.endDate = new FormControl("", Validators.required);
    this.projectBudget = new FormControl("", Validators.required);

    this.workPackageStatus = new FormControl("", Validators.required);
    this.constrains = new FormControl("", Validators.required);
    this.acceptanceCriteria = new FormControl("", Validators.required);
    this.skills =  new FormArray([]);
 
  }
  //creat the skill row with validation rules
  getNewSkillGroup(): FormGroup{
    return this.fb.group({
      name: ['', Validators.required],
      rating: ['', [Validators.required]],
      weightage: ['', [Validators.required, Validators.min(1),Validators.max(100)]]
    })
  }


  onAddNewSkill(){
    (<FormArray>this.proReviewForm.get('skills'))
    .push(this.getNewSkillGroup());
  }
  //creates the form 
  createForm() {
    this.proReviewForm = new FormGroup({
      projectName: this.projectName,
      workOrderNumber: this.workOrderNumber,
      projectArea: this.projectArea,
      customer: this.customer,

      projectDescription: this.projectDescription,
      scopeOfActivity: this.scopeOfActivity,

      inputForProject: this.inputForProject,
      tools: this.tools,
      deliverables: this.deliverables,

      startDate: this.startDate,
      endDate: this.endDate,

      projectBudget: this.projectBudget,
      skills: this.skills,
      workPackageStatus: this.workPackageStatus,
      constrains: this.constrains,
      acceptanceCriteria: this.acceptanceCriteria,
    });
  }

  constructor(public dialog: MatDialog ,
    public rootservice: RootService,
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private _proSharedData: ProjectDataService,
    private pageService: PageService,
    private router: Router,private activatedRoute:ActivatedRoute) {
     }
    
  
  openDialog(): void {
    const dialogRef = this.dialog.open(ProjectSuccessComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  saveProject(){
   //this.enableProject();
    this.SendProjectDetails();
    //this.proReviewForm.reset();
  }

  async SendProjectDetails(){
    console.log(this.proReviewForm.controls);
    if (this.proReviewForm.valid) {
      console.log("post-",this.proReviewForm.value);
     if(this.isSkillWeightageNotValid(this.proReviewForm.value.skills)){
      this.toastrService.show('danger', 'Skill Weightage', 'Cumulative weightage must be 100');
      return;
     }
     await this.pageService.sendprojectdetails(this.proReviewForm.controls).subscribe(async res => {
        const projectid = res.insertedId;
        console.log(projectid);
        await this.pageService.sendskilldetails(this.proReviewForm.value,res.insertedId).subscribe(async res => {
            const fileinfo = {
              'id': projectid,
              'fileName': this._proSharedData.filename,
              'fileType': this._proSharedData.filetype,
              'fileData': this._proSharedData.filedata
            };
           console.log(fileinfo);
           await this.pageService.fileinfo(fileinfo).subscribe(async (res: any) => {
              const userid = this.rootservice.mdpic;
              await this.pageService.setPrevileges(userid._id,userid.role,projectid).subscribe(res => {
               //this.toastrService.show('success', 'Success', 'Project details successfully saved');
               this.openDialog();
               this.router.navigateByUrl('pages/plp');
             }, err => {
               console.error('Error logging in : ', err);
               this.toastrService.show('danger', 'Previlege cannot set', 'Please check the user details');
            });
           }, err => {
            console.error('Error logging in : ', err);
            this.toastrService.show('danger', 'File is not upload', 'Please check the Q & A file');

            });
          }, err => {
            console.error('Error logging in : ', err);
            this.toastrService.show('danger', 'Invalid Project Skills', 'Please check the project skill details');
           });
         
      }, err => {
        console.error('Error logging in : ', err);
        this.toastrService.show('danger', 'Invalid Project Data', 'Please check the project details');
      });
    }
  }

  onStartChanged(){
    this.proReviewForm.patchValue( {'endDate':null} )
  }


  /*
  called while component is initized 
  creatint the input with form contolls
  */
  bindData() {
    // console.log("areadata-", this._proSharedData.arealist);
    // this.projectArea.setValue(this._proSharedData.arealist)
     this.projectArea.setValue(this._proSharedData.arealist);
     this.projectArea.updateValueAndValidity();
  }

 loadbodyclass() {
  const body = document.getElementsByTagName('body')[0];
  body.classList.add('pg-projectreview');
  } 

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.projectid = params['id'];
      if(params['isEditable']){
      this.editable_view = params['isEditable'];
      }
      else{
        this.editable_view = false;
      }
    })

    this.loadbodyclass();
    
    this.createFormControls();
    this.createForm();

    if (this.projectid == null) {
      setTimeout(() => {
      this._proSharedData.currentData.subscribe(data => {
        console.log(data);
        this.updateValues(data);
      });
      this.loadArealist();
   
        this.bindData();
       // (<HTMLInputElement>document.getElementById("updateBtn")).disabled = true;
      }, 1000);

      this.disableProject();
    }
    else
    {
      this.disableProject();
      this.pageService.projectMoreDetails(this.projectid).subscribe(res => {
        this.projectInfo = res.projectInfo;
        this.fileInfo = res.fileInfo;
        this.filedata =this.fileInfo.fileData;
        const spiltfile =this.filedata.split(',', 2);
        this.name =spiltfile[1];
        console.log("filedata-",this.name);
        this.pageService.getSkillList(this.projectid).subscribe(res => {
          console.log(res);
          this.skillinfo = [...res];
          this.setupdateValues(this.projectInfo,this.skillinfo);
          setTimeout(() => {
          //(<HTMLInputElement> document.getElementById("saveBtn")).disabled = true;
          //(<HTMLInputElement> document.getElementById("editBtn")).disabled = true;
          
        }, 1000);
      });
    });

    this.loadArealist();
    this.bindData();
    this.getAreaList=this._proSharedData.arealist;
    }
    this.proReviewForm
    .get('skills')
    .valueChanges
    .debounceTime(200)
    .subscribe(val => {
      this.onSkillsChanged(val);
    });
  }
  onSkillsChanged(val){
    this.cumulativeWeightInvalid = this.isSkillWeightageNotValid(val)
  }

  Clearfile()
  {
     this.filename =undefined;
     this.filename =undefined;
    
  }
  sumOfArr(items, prop){
      return items.reduce( function(a, b){
          return parseInt(a) + parseInt(b[prop]);
      }, 0);
  };
  isSkillWeightageNotValid(skills){
    let sum = this.sumOfArr(skills,'weightage');
    if(sum>=100){
      this.cumulGreater  = true;
    }
    else{
      this.cumulGreater = false;
    }
    return  sum != 100;
  }

  async updateProject() {
    console.log("Review-", this.proReviewForm.value);
    if(this.isSkillWeightageNotValid(this.proReviewForm.value.skills)){
      this.toastrService.show('danger', 'Skill Weightage', 'Cumulative weightage must be 100');
    }
    await this.pageService.setBasicinfo(this.projectid, this.proReviewForm.value).subscribe(async res => {
      console.log("res-basic", res);
      await this.pageService.deleteSkillSet(this.projectid).subscribe(async res => {
        await this.pageService.sendskilldetails(this.proReviewForm.value, this.projectid).subscribe(async res => {
          if(this.filename != undefined)
          {
            await this.pageService.deleteSkillinfo(this.projectid).subscribe(async res => {
          const fileinfo = {
            'id': this.projectid,
            'fileName': this.filename,
            'fileType': this.filetype,
            'fileData': this.getdoucmentbyte
          };
         console.log(fileinfo);
         await this.pageService.fileinfo(fileinfo).subscribe((res: any) => {
          this.toastrService.show('success', 'Success', 'Project details successfully updated');
          this.proReviewForm.reset();
          this.router.navigateByUrl('pages/plp');
        });
      });
      }
      else{
        this.toastrService.show('success', 'Success', 'Project details successfully updated');
        this.proReviewForm.reset();
        this.router.navigateByUrl('pages/plp');
      }
        });
      });
    });
  }

  setExistingSkills(skillsSet):FormArray{
    const formArray = new FormArray([]);
    skillsSet.forEach(s => {
      formArray.push(this.fb.group({
        name: [s.name, Validators.required],
        rating: [s.rating, Validators.required],
        weightage: [s.weightage, Validators.required]
      }));
    });
    return formArray;
  }

  reviewExistingSkills(skillsSet):FormArray{
    const formArray = new FormArray([]);
    skillsSet.forEach(s => {
      formArray.push(this.fb.group({
        name: [s.skillName, Validators.required],
        rating: [s.skillRating, Validators.required],
        weightage: [s.skillWeightage, Validators.required]
      }));
    });
    return formArray;
  }

  loadArealist() {
    this.pageService.getarealist().subscribe((res: Area) => {
      this.area = res;
     // console.log(this.area);
     // console.log(this.projectArea);

    });
 }
 setupdateValues(data:any,skills:any[]) {
  console.log("reviewdata-",data);
  this.proReviewForm.patchValue({
    projectName: data.projectName,
    workOrderNumber: data.projectRefNo,
     projectArea: data.projectArea,
    customer: data.customerName,
    projectDescription: data.projectDescription,
    scopeOfActivity: data.scopeofActivity,
    inputForProject: data.input,
    tools: data.tools,
    deliverables: data.deliverables,
    startDate: data.startDate,
    endDate: data.endDate,
    projectBudget: data.budget,
    workPackageStatus: data.workPackageStatus,
    constrains: data.constraints,
    acceptanceCriteria: data.acceptanceCriteria,
  });
  this.proReviewForm.setControl('skills',this.reviewExistingSkills(skills))
}

  updateValues(data:any) {
    console.log(data);
    this.proReviewForm.patchValue({
      projectName: data.proBasic.projectName,
      workOrderNumber: data.proBasic.workOrderNumber,
      projectArea: data.proBasic.projectArea,
      customer: data.proBasic.customer,
      projectDescription: data.proBasic.projectDescription,
      scopeOfActivity: data.proBasic.scopeOfActivity,
      inputForProject: data.proBasic.inputForProject,
      tools: data.proBasic.tools,
      deliverables: data.proBasic.deliverables,
      startDate: data.proCriteria.startDate,
      endDate: data.proCriteria.endDate,
      projectBudget: data.proCriteria.projectBudget,
      workPackageStatus: data.proCriteria.workPackageStatus,
      constrains: data.proCriteria.constrains,
      acceptanceCriteria: data.proCriteria.acceptanceCriteria,
      QandAdata:data.proCriteria.QandAdocument,
      Qandfiletype:data.proCriteria.filetype,
      Qandfilename:data.proCriteria.filename,
      QandAdatabyte:data.proCriteria.QandAdata, 
    });
    this.proReviewForm.setControl('skills',this.setExistingSkills(data.proCriteria.skills))
  }

  download() {
    var decodedString = atob(this.name);
    //var bindata = window.atob(this.name);
    console.log(decodedString);
    function DownloadExcel() {
      window.location.href = "data:application/vnd.ms-excel;base64, decodedString";
    }
    var blob = new Blob([decodedString], { type: 'application/vnd.ms-excel' });
    if (window.navigator && window.navigator.msSaveBlob) {
      window.navigator.msSaveBlob(blob);
    }
    else {
      this.Download = URL.createObjectURL(blob);
      window.open(this.Download);

    }

  }

  disableProject() {
    this.proReviewForm.controls['projectName'].disable();
    this.proReviewForm.controls['workOrderNumber'].disable();
    this.proReviewForm.controls['projectArea'].disable();
    this.proReviewForm.controls['customer'].disable();
    this.proReviewForm.controls['projectDescription'].disable();
    this.proReviewForm.controls['scopeOfActivity'].disable();
    this.proReviewForm.controls['inputForProject'].disable();
    this.proReviewForm.controls['tools'].disable();
    this.proReviewForm.controls['deliverables'].disable();
    this.proReviewForm.controls['startDate'].disable();
    this.proReviewForm.controls['endDate'].disable();
    this.proReviewForm.controls['projectBudget'].disable();
    this.proReviewForm.controls['workPackageStatus'].disable();
    this.proReviewForm.controls['constrains'].disable();
    this.proReviewForm.controls['acceptanceCriteria'].disable();
    this.proReviewForm.controls['skills'].disable();
    //this.proReviewForm.controls['skills'].disable();
    this.projectreviewdisable =true;
    //this.proReviewForm.controls['rating'].disable();
    //this.proReviewForm.controls['weightage:'].disable();
  }
  enableProject() {
    this.proReviewForm.controls['projectName'].enable();
    this.proReviewForm.controls['workOrderNumber'].enable();
    this.proReviewForm.controls['projectArea'].enable();
    this.proReviewForm.controls['customer'].enable();
    this.proReviewForm.controls['projectDescription'].enable();
    this.proReviewForm.controls['scopeOfActivity'].enable();
    this.proReviewForm.controls['inputForProject'].enable();
    this.proReviewForm.controls['tools'].enable();
    this.proReviewForm.controls['deliverables'].enable();
    this.proReviewForm.controls['startDate'].enable();
    this.proReviewForm.controls['endDate'].enable();
    this.proReviewForm.controls['projectBudget'].enable();
    this.proReviewForm.controls['workPackageStatus'].enable();
    this.proReviewForm.controls['constrains'].enable();
    this.proReviewForm.controls['acceptanceCriteria'].enable();
    this.proReviewForm.controls['skills'].enable();
    this.projectreviewdisable =false;
    //this.proReviewForm.controls['name'].enable();
    //this.proReviewForm.controls['rating'].enable();
    //this.proReviewForm.controls['weightage:'].enable();
  }



  fileChangeEvent = (fileInput: any) => {
    const reader = new FileReader();
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
        // Size Filter Bytes
        const max_size = 20971520;
        const allowed_types = ['application/vnd.ms-excel'];
        const max_height = 300;
        const max_width = 300;
  
        if (fileInput.target.files[0].size > max_size) {
            this.imageError =
                'Maximum size allowed is ' + max_size / 1000 + 'Mb';
            return false;
        }
        if(fileInput.target.files[0].name.replace(/\.[^.]+$/, '') != ('Q&A '+ this.projectNameValue.trim())){
          this.imageError = 'File name must be  Q&A ' + this.projectNameValue.trim();
            return false;
        }
        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
            this.imageError = 'Only EXCEL Document Allowed!';
            return false;
        }
      
        reader.onload = (e: any) => {
            const image = new Image();
            this.getdoucmentbyte = e.target.result;
            image.src = e.target.result;
            image.onload = rs => {
                const img_height = rs.currentTarget['height'];
                const img_width = rs.currentTarget['width'];
  
                console.log(img_height, img_width);
  
  
                if (img_height > max_height && img_width > max_width) {
                    this.imageError =
                        'Maximum dimentions allowed ' +
                        max_height +
                        '*' +
                        max_width +
                        'px';
                    return false;
                } else {
                    const imgBase64Path = e.target.result;
                    this.cardImageBase64 = imgBase64Path;
                    this.isImageSaved = true;
                    // this.previewImagePath = imgBase64Path;
                }
            };
        };
  
        this.filename = fileInput.target.files[0].name,
        this.filetype = fileInput.target.files[0].type 
     
        console.log(this.filetype);
        console.log(this.filename);
        reader.readAsDataURL(fileInput.target.files[0]); 
    }
  //   removeImage() {
  //     this.cardImageBase64 = null;
  //     this.isImageSaved = false;
  // }
  
  }


}
