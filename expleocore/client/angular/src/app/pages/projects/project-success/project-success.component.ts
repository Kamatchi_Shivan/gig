import { Component, OnInit } from '@angular/core';

import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'exp-project-success',
  templateUrl: './project-success.component.html',
  styleUrls: ['./project-success.component.css']
})
export class ProjectSuccessComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ProjectSuccessComponent>) { }

  ngOnInit(): void {
    this.loadbodyclass();
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-projectsuccess');
    } 
  onNoClick(): void {
    this.dialogRef.close();
  }
}
