import { Component, OnInit } from '@angular/core';
import {  FormGroup, Validators, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { ProjectDataService } from '../projectData.service';
import { Router } from '@angular/router';
import { Area } from '../../../models/models';
import { PageService } from '../../page-service';
import * as _ from 'lodash';
import { ToastrService } from '../../../shared/toastr/toastr.service';
import { NbIconLibraries } from '@nebular/theme';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'exp-project-publishing',
  templateUrl: './project-publishing.component.html',
  styleUrls: ['./project-publishing.component.css']
})
export class ProjectPublishingComponent implements OnInit {
  //date constarian
  minDate: Date = new Date();
  //test
  area: Area;
  currProCreationData:any;
  //file operation
  projectNameValue: string;
  projectname;
  eWorkorder;
  verifiedWorkorder:string ="0";
  verifiedproject:string="ABCD";
  selectedFiles;
  filename;
  filetype;
  name;
  filedata;
  getAreaList;
  Download;
  getdoucmentbyte;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  //form object
  proBasicForm: FormGroup;
  proCriteriaForm: FormGroup;

  //project basic input file controls
  projectName: FormControl;
  workOrderNumber: FormControl;
  projectArea: FormControl;
  customer: FormControl;
  projectDescription: FormControl;
  scopeOfActivity: FormControl;
  inputForProject: FormControl;
  tools: FormControl;
  deliverables: FormControl;

  //project criteria form control
  startDate: FormControl;
  endDate: FormControl;
  projectBudget: FormControl;
  workPackageStatus: FormControl;
  constrains: FormControl;
  acceptanceCriteria: FormControl;
  skills: FormArray;
  file: FormControl;

  selTab: number = 0;

  cumulativeWeightInvalid = false;
  cumulGreater = false;
  nextStep() {
    //this.createForm();
     if (this.proBasicForm.valid)
     {
      if (this.selTab != 2) {
        this.selTab = this.selTab + 1;
      }
    }
    //hard codeded the number tab as 2
  
  }

  previousStep() {
    if (this.selTab != 0) {
      this.selTab = this.selTab - 1;
    }
  }
  //creates the form input controlls 
  createFormControls() {
    this.projectName = new FormControl("", Validators.required);
    this.workOrderNumber = new FormControl("", Validators.required);
    this.projectArea = new FormControl("", Validators.required);
    this.customer = new FormControl("", [Validators.required,Validators.pattern('^[a-zA-Z ]*$')]);
    this.tools = new FormControl("", Validators.required);
    this.deliverables = new FormControl("", Validators.required);
    this.projectDescription = new FormControl("", Validators.required);
    this.scopeOfActivity = new FormControl("", Validators.required);
    this.inputForProject = new FormControl("", Validators.required);

    this.startDate = new FormControl("", Validators.required);
    this.endDate = new FormControl("", Validators.required);
    this.projectBudget = new FormControl("", Validators.required);
    this.workPackageStatus = new FormControl("", Validators.required);
    this.constrains = new FormControl("", Validators.required);
    this.acceptanceCriteria = new FormControl("", Validators.required);
    this.file = new FormControl('', [Validators.required]);
    this.skills =  new FormArray([]);
  }
  createForm() {
    this.proBasicForm = new FormGroup({
      projectName: this.projectName,
      workOrderNumber: this.workOrderNumber,
      projectArea: this.projectArea,
      customer: this.customer,
      tools: this.tools,
      deliverables: this.deliverables,
      projectDescription: this.projectDescription,
      scopeOfActivity: this.scopeOfActivity,
      inputForProject: this.inputForProject,
    });

    this.proCriteriaForm = new FormGroup({
      startDate: this.startDate,
      endDate: this.endDate,
      projectBudget: this.projectBudget,
      skills: this.skills,
      workPackageStatus: this.workPackageStatus,
      constrains: this.constrains,
      acceptanceCriteria: this.acceptanceCriteria
    })
  }
  download() {
    var decodedString = atob(this.name);
    //var bindata = window.atob(this.name);
    console.log(decodedString);
    function DownloadExcel() {
      window.location.href = "data:application/vnd.ms-excel;base64, decodedString";
    }
    var blob = new Blob([decodedString], { type: 'application/vnd.ms-excel' });
    if (window.navigator && window.navigator.msSaveBlob) {
      window.navigator.msSaveBlob(blob);
    }
    else {
      this.Download = URL.createObjectURL(blob);
      window.open(this.Download);

    }

  }
  getNewSkillGroup(): FormGroup{
    return this.fb.group({
      id:Date.now(),
      name: ['', Validators.required],
      rating: ['', [Validators.required]],
      weightage: ['', [Validators.required, Validators.min(1),Validators.max(100)]]
    })
  }
  //creat the skill row with validation rules
  onAddNewSkill(){
    (<FormArray>this.proCriteriaForm.get('skills'))
    .push(this.getNewSkillGroup());
  }
  constructor(
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private _proSharedData: ProjectDataService,
    private router: Router,
    private pageService: PageService,
    private iconLibraries: NbIconLibraries){
    this.iconLibraries.registerFontPack('font-awesome', { iconClassPrefix: 'fas' } ) 
  }

  setselectarealist()
  {
    // var i:number;
    // var data = [];
    // for (i = 0; i < this.proBasicForm.value.projectArea.length; i++) {
    //   const arealist ={
    //     'areaId': this.proBasicForm.value.projectArea[i],
    //   };
    //   data.push(arealist);
    //   console.log(data);
      this._proSharedData.arealist =  this.proBasicForm.value.projectArea;
    //}
  }
  sumOfArr(items, prop){
      return items.reduce( function(a, b){
          return parseInt(a) + parseInt(b[prop]);
      }, 0);
  };
  isSkillWeightageNotValid(skills){
    let sum = this.sumOfArr(skills,'weightage');
    if(sum>=100){
      this.cumulGreater  = true;
    }
    else{
      this.cumulGreater = false;
    }
    return  sum != 100;
  }
  
 onSubmit(){
   if(this.proBasicForm.valid){
     if (this.proCriteriaForm.valid){
      console.log("post-1");
       if(this.isSkillWeightageNotValid(this.proCriteriaForm.value.skills)){
        this.toastrService.show('danger', 'Skill Weightage', 'Cumulative weightage must be 100');
        return;
       }
       if(this.filename != undefined)
       {
        console.log("post-2");
         if (this.proCriteriaForm.value.skills.length !=0)
         {
           console.log("post-3");
          this.saveQAfiledata();
          this.setselectarealist();
          setTimeout(() => {
          const newData = { 
            proBasic: this.proBasicForm.value, 
            proCriteria: this.proCriteriaForm.value
          } 

          this._proSharedData.changeData(newData);
          this.router.navigate(['/pages/project-review']); 
        }, 2000);   
        
  
         }
         else{
          this.toastrService.show('danger', 'Select Skills', 'Select the project Skills');
         }
   }
   else{
    this.toastrService.show('danger', 'Upload Q & A file', 'Upload the project Q & A file');
   } 
  }
  else{
    this.toastrService.show('danger', 'Criteria information', 'Please fill the Criteria information');
  }
  }
  else{
    this.toastrService.show('danger', 'Basic information', 'Please fill the Basic information');
  }
  }


  ngOnInit(): void {
    this.loadbodyclass();
    this.loadArealist();
    this.createFormControls();
    this.createForm();
    this._proSharedData.currentData.subscribe(data => this.currProCreationData = data); 
    this.proCriteriaForm
        .get('skills')
        .valueChanges
        .debounceTime(200)
        .subscribe(val => {
          this.onSkillsChanged(val);
        });
  }
  onSkillsChanged(val){
      this.cumulativeWeightInvalid = this.isSkillWeightageNotValid(val)
  }
  loadArealist() {
    this.pageService.getarealist().subscribe((res: Area) => {
      this.area = res;
    });
 }

  onStartChanged(){
    this.proCriteriaForm.patchValue( {'endDate':null} )
  }

  saveQAfiledata()
  {
    this._proSharedData.filedata = this.getdoucmentbyte;
    this._proSharedData.filename = this.filename;
    this._proSharedData.filetype = this.filetype;
  }

  Clearfile()
  {
     this.filename =undefined;
     this.filename =undefined;
    
  }

  checkproject(e)
  {
    this.projectname = e.target.value;
    console.log(this.projectname);
    this.pageService.projectAvailability(this.projectname).subscribe(res => {
        if (res.code==200)
        {
          this.verifiedproject=this.projectname;
          (<HTMLInputElement> document.getElementById("btnnext")).disabled = true;
        }
        else{
          (<HTMLInputElement> document.getElementById("btnnext")).disabled = false;
        }
    
    });
  }

  checkworkorder(e)
  {
    this.eWorkorder = e.target.value;
    console.log(this.eWorkorder);
    this.pageService.workorderAvailability(this.eWorkorder).subscribe(res => {
        if (res.code==200)
        {
          this.verifiedWorkorder=this.eWorkorder;
          (<HTMLInputElement> document.getElementById("btnnext")).disabled = true;
        }
        else{
          (<HTMLInputElement> document.getElementById("btnnext")).disabled = false;
        }
    });
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-publishing');
  }

  fileChangeEvent = (fileInput: any) => {
    console.log(this.projectNameValue);
    const reader = new FileReader();
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
        // Size Filter Bytes
        const max_size = 20971520;
        const allowed_types = ['application/vnd.ms-excel'];
        const max_height = 300;
        const max_width = 300;

        if (fileInput.target.files[0].size > max_size) {
            this.imageError =
                'Maximum size allowed is ' + max_size / 1000 + 'Mb';
  
            return false;
        }

        if(fileInput.target.files[0].name.replace(/\.[^.]+$/, '') != ('Q&A '+ this.projectNameValue.trim())){
          this.imageError = 'File name must be  Q&A ' + this.projectNameValue.trim();
            return false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
            this.imageError = 'Only EXCEL Document Allowed!';
            return false;
        }
        
       
        reader.onload = (e: any) => {
            const image = new Image();
            this.getdoucmentbyte = e.target.result;
            image.src = e.target.result;
            image.onload = rs => {
                const img_height = rs.currentTarget['height'];
                const img_width = rs.currentTarget['width'];
  
                console.log(img_height, img_width);
  
  
                if (img_height > max_height && img_width > max_width) {
                    this.imageError =
                        'Maximum dimentions allowed ' +
                        max_height +
                        '*' +
                        max_width +
                        'px';
                    return false;
                } else {
                    const imgBase64Path = e.target.result;
                    this.cardImageBase64 = imgBase64Path;
                    this.isImageSaved = true;
                    // this.previewImagePath = imgBase64Path;
                }
            };
        };
  
        this.filename = fileInput.target.files[0].name,
        this.filetype = fileInput.target.files[0].type 
     
        console.log(this.filetype);
        console.log(this.filename);
        reader.readAsDataURL(fileInput.target.files[0]); 
  }
  //   removeImage() {
  //     this.cardImageBase64 = null;
  //     this.isImageSaved = false;
  // }
  
  }
}
