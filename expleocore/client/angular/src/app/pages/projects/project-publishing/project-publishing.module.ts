import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { NbCardModule } from '@nebular/theme';

import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';

import { CommonModule } from '@angular/common';
import { ProjectPublishingComponent } from './project-publishing.component';

import { SharedModule } from '../../../shared/shared.module';

const materialModules = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatButtonModule,
  MatStepperModule,
  MatTabsModule,
  MatTooltipModule
];

@NgModule({
  imports: [
    NbCardModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    ...materialModules
  ],
  declarations: [
    ProjectPublishingComponent,
  ]
})
export class ProjectPublishingModule {
  
}
