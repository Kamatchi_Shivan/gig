import { Component } from '@angular/core';
import { 
  NbSidebarService } from '@nebular/theme';
@Component({
  selector: 'exp-one-column-layout',
  styleUrls: ['./one-column.layout.scss'],
  template: `
    <nb-layout windowMode>
      <nb-layout-header fixed>
        <exp-header></exp-header>
      </nb-layout-header>
      <nb-sidebar (clickOutside)="close($event)"
      id="menu-sidebar" 
      class="menu-sidebar" 
      tag="menu-sidebar" 
      responsive 
      start
      state = "compacted"
      fixed>
      <ng-content select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column>
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>
      </nb-layout>`,

  //     <nb-layout-footer fixed>
  //       <exp-footer></exp-footer>
  //     </nb-layout-footer>
  //   </nb-layout>
  // `,
})
export class OneColumnLayoutComponent {
  constructor(private sidebarService: NbSidebarService) {
  }
  close(event){
    if(!event.target.classList.contains("sidebar-img-toggle")){
      this.sidebarService.compact('menu-sidebar');
    }
  }
}
