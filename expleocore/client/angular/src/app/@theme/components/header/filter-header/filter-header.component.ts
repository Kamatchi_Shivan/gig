import { Component, OnInit } from '@angular/core';

import { FilterProjectService } from '../../../../shared/filter-project/filterProject.service';

@Component({
  selector: '[expFilterHeader]',
  templateUrl: './filter-header.component.html',
  styleUrls: ['./filter-header.component.css']
})
export class FilterHeaderComponent implements OnInit {

  constructor(private _proSharedData: FilterProjectService) { }
  projectCount:any;
  isFilterVisible: boolean;
  currentFilter: string = 'all';
  onFilter(type){
    this._proSharedData.onFilterProject(type);
    this.currentFilter = type;
  }
  ngOnInit(): void {
    this._proSharedData.projectCount.subscribe(data => this.projectCount = data); 
    this._proSharedData.showFilter.subscribe(data => this.isFilterVisible = data); 
  }

}
