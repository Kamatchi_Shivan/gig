import { Component, OnDestroy, OnInit } from '@angular/core';
import {  NbMediaBreakpointsService, 
          NbMenuService, 
          NbSidebarService, 
          NbThemeService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { SharedService } from '../../../shared/shared.service';
import { domain } from 'process';
import { Userdetails } from '../../../models/models';
import { RootService } from '../../../root.service';
import { debug } from 'console';
import { PageTitleService } from '../../../shared/pageTitle.service';
import { RolesService } from '../../../shared/roles.service';
@Component({
  selector: 'exp-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
  user_name:any;
  img_pic:any;
  role;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';
  private username: string;
  pic: string;
  name: string;
  userMenu = [{ title: 'Profile' }, { title: 'Log out' }];
  pageTitle: string;
  menuExpanded: boolean = false;

  constructor(private sidebarService: NbSidebarService,
    private _pageTitleService: PageTitleService,
    private _rolesService: RolesService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserData,
    private breakpointService: NbMediaBreakpointsService,
    private router: Router,public sharedService: SharedService,public rootService: RootService) {
  }

  ngOnInit() {
    this._pageTitleService.currPgTitle.subscribe(data => this.pageTitle = data);
    this._rolesService.currRole.subscribe(data => this.role = data);  
    this.menuService.onItemSelect()
    .pipe(takeUntil(this.destroy$))
    .subscribe((event: { tag: string, item: any }) => {
        console.log("Menu selected");
        this.menuExpanded = false;
        this.sidebarService.compact('menu-sidebar');
        
    });

    this.sidebarService.onCompact()
    .pipe(takeUntil(this.destroy$))
    .subscribe((event: {compact:boolean, tag: string }) => {
      if(document.getElementById('menu-sidebar').classList.contains('expanded')){
        this.menuExpanded = false;
      }
    });
    setTimeout (() => {
      this.img_pic=  this.rootService.mdpic;
      this.rootService.getuserimg(this.img_pic.username).subscribe(res => {
        console.log(res);
        let name = res.user.name;
        let  loggedUserinfo = JSON.parse(localStorage.getItem('user')).role;

        let role = loggedUserinfo;//res.user.role;
        this.role = role.charAt(0).toUpperCase() + role.slice(1);
        this.name = name.charAt(0).toUpperCase() + name.slice(1);
        this.pic = res.user.profilepic;
      });
   }, 1000);

    
    this.currentTheme = this.themeService.currentTheme;
     this.sharedService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => this.user = this.img_pic);
      console.log(this.user);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
     this.sidebarService.toggle(true, 'menu-sidebar');
    //this.sidebarService.toggle(false, 'menu-sidebar');
    setTimeout(() => {
      this.menuExpanded = document.getElementById('menu-sidebar').classList.contains('expanded');
    }, 0);

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/auth']);
  }
}
