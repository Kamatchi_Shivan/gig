import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { RootService } from './root.service';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private rootService: RootService) { }
  intercept(req, next) {
    if (localStorage.getItem('auth_token') !== null) {
      let tokenizedReq = req.clone(
        {
          headers: req.headers.set('auth_token', this.rootService.getToken())
        }
      );
      return next.handle(tokenizedReq);
    } else {
      return next.handle(req);
    }
  }
}
