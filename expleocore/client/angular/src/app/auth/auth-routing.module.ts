import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './registration/registration.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { UpdatepasswordComponent } from './updatepassword/updatepassword.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: '', redirectTo: 'login' },
  {
    path: '', component: AuthComponent, children: [
      { path: 'login', component: LoginComponent },
    ],
  },
  { path: '', redirectTo: 'registration' },
  {
    path: '', component: AuthComponent, children: [
      { path: 'registration', component: RegistrationComponent },
    ],
  },
  { path: '', redirectTo: 'forgetpassowrd' },
  {
    path: '', component: AuthComponent, children: [
      { path: 'forgetpassowrd', component: ForgetpasswordComponent },
    ],
  },
  { path: '', redirectTo: 'updatepassword' },
  {
    path: '', component: AuthComponent, children: [
      { path: 'updatepassword', component: UpdatepasswordComponent },
    ],
  },
  { path: '', redirectTo: 'profile' },
  {
    path: '', component: AuthComponent, children: [
      { path: 'profile', component: ProfileComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
