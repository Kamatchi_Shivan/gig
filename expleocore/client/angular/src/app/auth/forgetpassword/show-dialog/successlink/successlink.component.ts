import { Component, OnInit,AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'exp-successlink',
  templateUrl: './successlink.component.html',
  styleUrls: ['./successlink.component.css']
})
export class SuccesslinkComponent implements OnInit, AfterContentInit {

  constructor(private router: Router,public dialogRef: MatDialogRef<SuccesslinkComponent>) { }

  ngOnInit(): void {
    this.loadbodyclass();
  }
  onNoClick(): void {
    this.dialogRef.close();
   this.router.navigate(['/auth/login']);
    
  }
 
  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-forgetsuccesslink');
  }
  
  
  ngAfterContentInit() {
   
  }

}
