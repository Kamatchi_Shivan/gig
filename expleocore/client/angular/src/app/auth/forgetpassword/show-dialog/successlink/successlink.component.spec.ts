import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccesslinkComponent } from './successlink.component';

describe('SuccesslinkComponent', () => {
  let component: SuccesslinkComponent;
  let fixture: ComponentFixture<SuccesslinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccesslinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccesslinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
