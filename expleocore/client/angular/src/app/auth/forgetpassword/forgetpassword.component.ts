import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { MailService } from '../mail.service';
import { SharedService } from '../../shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from '../../shared/toastr/toastr.service';
import { SuccesslinkComponent } from './show-dialog/successlink/successlink.component';
import { Router } from '@angular/router';

@Component({
  selector: 'exp-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {
  forgetpassword: FormGroup;
  constructor(public sharedService: SharedService, private mailService: MailService,private toastrService: ToastrService, private fb: FormBuilder, private authService: AuthService, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog, private router: Router) {
    iconRegistry.addSvgIcon(
      'person',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/person.svg'));

    this.forgetpassword = this.fb.group({
      Email: ['', [Validators.required, Validators.email]],
      // password: ['', Validators.required, Validators.maxLength(6)]
    });

  }
  get f() { return this.forgetpassword.controls; }

  ngOnInit(): void {
    this.loadbodyclass();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SuccesslinkComponent, {
      width: '500px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-forgetpassword');
  }

  forget() {
    if (this.forgetpassword.valid) {
      this.authService.checkuserid(this.forgetpassword.value).subscribe(res => {
        if (res.code == 200) {
          this.inserttoken();
          this.openDialog();
        } else {
          this.toastrService.show('danger', 'The User is not registered', 'Please register your details');
        }
      }, err => {
        console.error('Error logging in : ', err);
        this.toastrService.show('danger', 'Error', 'Error');
      });
    }
  }

  encrptusername() {
    if (this.forgetpassword.valid) {
       this.mailService.setencrption(this.forgetpassword.value).subscribe(res => {
          if (res.emailid) {
            const encmail=res.emailid;
            const tomailid=this.forgetpassword.value;
            console.log("testmail",tomailid)
            this.mailService.sendreseturl(tomailid,encmail).subscribe(res => { 
           }, err => {
            console.error('Error logging in : ', err);
            this.toastrService.show('danger', 'Error', 'Error');
          });
        }
       }, err => {
        console.error('Error logging in : ', err);
        this.toastrService.show('danger', 'Error', 'Error');
      });
    }
  }

  inserttoken() {
    if (this.forgetpassword.valid) {
       this.authService.setinserttoken(this.forgetpassword.value).subscribe(res => {
        console.log(res.code);
          if (res.code == 200) {
           this.encrptusername();
         }
       }, err => {
        console.error('Error logging in : ', err);
        this.toastrService.show('danger', 'Error', 'Error');
      });
  }
  }
}

