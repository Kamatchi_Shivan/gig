import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SharedService } from '../../../../shared/shared.service';

@Component({
  selector: 'exp-profilesuccess',
  templateUrl: './profilesuccess.component.html',
  styleUrls: ['./profilesuccess.component.css']
})
export class ProfilesuccessComponent implements OnInit {

  constructor(public sharedService: SharedService,private router: Router,public dialogRef: MatDialogRef<ProfilesuccessComponent>) { }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
    const role =this.sharedService.userdetails.role;
    this.router.navigate(['/pages/plp']);
  }
}
