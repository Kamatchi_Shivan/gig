import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { SharedService } from '../../shared/shared.service';
import { Area } from '../../models/models';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ToastrService } from '../../shared/toastr/toastr.service';
import { ProfilesuccessComponent } from './Show-dialog/profilesuccess/profilesuccess.component';
import { Router } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'exp-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
  profile: FormGroup;
  areaexpertise: Area;
  selectedFiles;
  filename;
  filetype;
  getdoucmentbyte;
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  constructor( private router: Router,public sharedService: SharedService,private toastrService: ToastrService,private formBuilder: FormBuilder, private authService: AuthService, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,public dialog: MatDialog) { 
    iconRegistry.addSvgIcon(
      'person',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/person.svg'));
   }

  ngOnInit(): void {
    this.loadbodyclass();
    this.createForm();
    this.loadAreaexpertise();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ProfilesuccessComponent, {
      panelClass: 'icon-outside',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  createForm() {
    this.profile = this.formBuilder.group({
      area: [null, Validators.required],
      years: [null, Validators.required],
      months: [null, Validators.required]
      // resume:[undefined, Validators.required]
    });
  }

  loadAreaexpertise() {
    const domainid = this.sharedService.userdetails;
    var strArraydomain = domainid.domain.map(function(value) {
      return String(value);
     });
     const postdomaininfo = {
      'domainId': strArraydomain
    };
    this.authService.getAreaexpertiselist(postdomaininfo).subscribe((res: any) => {
      console.log(res);
      this.areaexpertise = res;
    });
 }

 loadbodyclass() {
  const body = document.getElementsByTagName('body')[0];
  body.classList.add('pg-profile');
}
loading = false;
updateprofile(){
  if (this.profile.valid) {
    if (this.filename != undefined) {
    this.loading = true;
    if (this.filetype =="application/pdf") {
  const domainid = this.sharedService.userdetails;

  const fileinfo = {
    'id': domainid._id,
    'fileName': this.filename,
    'fileType': this.filetype,
    'fileData': this.getdoucmentbyte
  };

  this.authService.fileinfo(fileinfo).subscribe((res: any) => {
    if(res.insertedId){
      const userexpinfo = {
        'userId': domainid._id,
        'areaofExpert': this.profile.value.area,
        'yearsofExp': this.profile.value.years + 'yrs,'+ this.profile.value.months +'Mths',
        'fileId': res.insertedId,
        'resume': res.ops[0].fileName
      };
      console.log(userexpinfo);
      this.authService.userexpertise(userexpinfo).subscribe((res: any) => {
       console.log(res);
       if(res.insertedId)
       {
        const user = this.sharedService.userdetails;
        console.log(user.username);
        this.authService.updateprofilestatus(user.username).subscribe((res: any) => {
          if(res.code ==200)
          {
            this.openDialog();
          }
          else{
            this.toastrService.show('danger', 'The User profile info not updated', 'Please check your details');
            this.router.navigate(['/auth/login']);
          }
         
        });
        this.loading = false;
       }

    });

    }
  });

}
else{
  this.toastrService.show('danger', 'Please select the pdf format', 'Please verify your file format');
}
}
}
else{
  this.toastrService.show('danger', 'Please fill the profile data', 'Please check your details');
}
}


fileChangeEvent(fileInput: any) {
  const reader = new FileReader();
  this.imageError = null;
  if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['application/pdf','.pdf'];
      const max_height = 300;
      const max_width = 300;

      if (fileInput.target.files[0].size > max_size) {
          this.imageError =
              'Maximum size allowed is ' + max_size / 1000 + 'Mb';

          return false;
      }

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
          this.imageError = 'Only pdf are allowed ( .pdf )';
          return false;
      }
    
      reader.onload = (e: any) => {
          const image = new Image();
          this.getdoucmentbyte = e.target.result;
         /** console.log(this.getdoucmentbyte); */
          image.src = e.target.result;
          image.onload = rs => {
              const img_height = rs.currentTarget['height'];
              const img_width = rs.currentTarget['width'];

              console.log(img_height, img_width);


              if (img_height > max_height && img_width > max_width) {
                  this.imageError =
                      'Maximum dimentions allowed ' +
                      max_height +
                      '*' +
                      max_width +
                      'px';
                  return false;
              } else {
                  const imgBase64Path = e.target.result;
                  this.cardImageBase64 = imgBase64Path;
                  this.isImageSaved = true;
                  // this.previewImagePath = imgBase64Path;
              }
          };
      };

      this.filename = fileInput.target.files[0].name,
      this.filetype = fileInput.target.files[0].type 

      reader.readAsDataURL(fileInput.target.files[0]); 
  }
//   removeImage() {
//     this.cardImageBase64 = null;
//     this.isImageSaved = false;
// }

}

}
