import { Component, OnInit, ViewChild,ElementRef,AfterContentInit} from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { ToastrService } from '../../shared/toastr/toastr.service';
import { Router } from '@angular/router';
import { RootService } from '../../root.service';


@Component({
  selector: 'exp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit,AfterContentInit {
  passInputType = 'password';
  Login: FormGroup;
  emailAddress: String;
  password: String;
  hide =true;
  tokenForcaptcha;
  loginProcessing:boolean = false;
  @ViewChild('loginFrm', { static: true })
  loginFrm: ElementRef;

  constructor(public sharedService: SharedService, public rootservice: RootService,private authService: AuthService,
    private toastrService: ToastrService, private router: Router,private formBuilder: FormBuilder) {

      
     }
     ngAfterContentInit() {
      
    }

  ngOnInit() {
    this.loadbodyclass();
    this.createForm();
    console.log("calling");
  //  this.addRecaptchaScript();
  }
  loadbodyclass()
  {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-login');
  }
  resolved(captchaResponse: string, res) {
    //<!-- Sitekey:6LdqorEZAAAAAMthf0DllDA3KuWpRywnbmzkrsoX-->
   // <!-- seckrt :6LdqorEZAAAAAOXVV8bI0tHWD6EH2yRdGiciyVHj-->
    console.log(`Resolved response token: ${captchaResponse}`);
    this.tokenForcaptcha = captchaResponse;
    
  }

  createForm() {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.Login = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern(emailregex)]],
      password: [null, [Validators.required]]
    });
  }
  getErrorEmail() {
    return this.Login.get('email').hasError('required') ? 'EmailId is required' :
      this.Login.get('email').hasError('pattern') ? 'Enter valid EmailId' :
        this.Login.get('email').hasError('alreadyInUse') ? 'This emailaddress is already in use' : '';
  }

  getErrorPassword() {
    return this.Login.get('password').hasError('required') ? 'Password is required' : '';
  }


  login() {
    if (this.Login.valid) {
      //if (this.tokenForcaptcha == undefined){
      if (false){ //Removed captcha
        this.toastrService.show('danger', 'verify the captcha', 'verify the captcha');
      }
      else{
         this.loginProcessing = true;
         this.authService.login(this.Login.value.email, this.Login.value.password).subscribe(res => {
          this.loginProcessing = false;
          localStorage.setItem('user', JSON.stringify(res.user));
          localStorage.setItem('previousRole', JSON.stringify(res.user.role));
          this.rootservice.mdpic=res.user;
          const userrole= res.user.role;
          localStorage.setItem('auth_token', res.token);
      if (userrole == "Consultant")
      {
        if (res.user.firstTimeLogin == true)
        {
          this.sharedService.userdetails = res.user;
          this.authService.regStatus(res.user._id).subscribe(res => { 
            if (res.user.regStatus == "accept"){
              this.toastrService.show('success', 'Success', 'Login success');
              this.router.navigate(['/auth/profile']);
            }
            else{
              this.toastrService.show('danger', 'user not accepted', 'Pls contact to project manager');
            }

          });
        }
        else if (res) {
          this.sharedService.userdetails = res.user;
          this.authService.regStatus(res.user._id).subscribe(res => { 
            if (res.user.regStatus == "accept"){
              this.toastrService.show('success', 'Success', 'Login success');
              this.router.navigate(['/pages/plp']);
            }
            else{
              this.toastrService.show('danger', 'user not accepted', 'Pls contact to project manager');
            }
          });
        } else {
          this.toastrService.show('danger', 'Invalid Credatial', 'Username or password is invalid');
        }
      }
      else if (userrole == "Project Manager" || userrole == "Reviewer" || userrole == "Line Manager")
      {
        if (res.user.firstTimeLogin == true)
        {
          this.sharedService.userdetails = res.user;
          this.authService.regStatus(res.user._id).subscribe(res => { 
         
            if (res.user.regStatus == "accept"){
              this.toastrService.show('success', 'Success', 'Login success');
              this.router.navigate(['/auth/profile']);
            }
            else{
              this.toastrService.show('danger', 'user not accepted', 'Pls contact to project manager');
            }
          });
        }
        else if (res) {
          this.sharedService.userdetails = res.user;
          this.authService.regStatus(res.user._id).subscribe(res => { 
            if (res.user.regStatus == "accept"){
          this.toastrService.show('success', 'Success', 'Login success');
          this.router.navigate(['/pages/plp']);
        }
        else{
          this.toastrService.show('danger', 'user not accepted', 'Pls contact to project manager');
        }
        });
        } else {
          this.toastrService.show('danger', 'Invalid Credatial', 'Username or password is invalid');
        }
      }
      else if (userrole == "Admin")
      { 
        this.sharedService.userdetails = res.user;
        this.toastrService.show('success', 'Success', 'Login success');
        this.router.navigate(['/pages/manageUserRegistration']);

      }   
      // else if (userrole == "Reviewer")
      // { 
      //   this.sharedService.userdetails = res.user;
      //   this.toastrService.show('success', 'Success', 'Login success');
      //   this.router.navigate(['/pages/plp']);

      // }
      // else if (userrole == "Line Manager")
      // { 
      //   this.sharedService.userdetails = res.user;
      //   this.toastrService.show('success', 'Success', 'Login success');
      //   this.router.navigate(['/pages/plp']);

      // }
   
    }, err => {
      this.loginProcessing = false;
      console.error('Error logging in : ', err);
      this.toastrService.show('danger', 'Invalid Credatial', 'Username or password is invalid');
    });
  }
}
  }

  register() {
    this.router.navigate(['/auth/registration']);
  }

  forget() {
    this.router.navigate(['/auth/forgetpassowrd']);
  }


  addRecaptchaScript() {
    (function(d, s, id, obj){
      console.log(d.getElementsByTagName(s)[0]);
      var js, fjs = d.getElementsByTagName(s)[0];

      if (d.getElementById(id)) { return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://www.google.com/recaptcha/api.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'recaptcha-jssdk', this));
   
  }

 
 
}
