import { Component, OnInit,Inject } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'exp-regsuccess',
  templateUrl: './regsuccess.component.html',
  styleUrls: ['./regsuccess.component.css']
})
export class RegsuccessComponent implements OnInit {

  constructor(private router: Router,public dialogRef: MatDialogRef<RegsuccessComponent>) { }

  ngOnInit(): void {
    
  }
  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/auth/login']);
  }
}
