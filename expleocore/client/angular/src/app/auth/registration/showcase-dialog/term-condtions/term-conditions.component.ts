import { Component, OnInit } from '@angular/core';

import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'term-conditions-success',
  templateUrl: './term-conditions.component.html',
  styleUrls: ['./term-conditions.component.css']
})
export class TermConditionsComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TermConditionsComponent>) { }

  ngOnInit(): void {
    this.loadbodyclass();
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-projectsuccess');
    } 
  onNoClick(): void {
    this.dialogRef.close();
  }
}
