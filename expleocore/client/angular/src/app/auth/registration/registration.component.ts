import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { SharedService } from '../../shared/shared.service';
import { Domain } from '../../models/models';
import { Role } from '../../models/models';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RegsuccessComponent } from './showcase-dialog/regsuccess/regsuccess.component';
import { ToastrService } from '../../shared/toastr/toastr.service';
import * as _ from 'lodash';
import { MailService } from '../mail.service';

import { TermConditionsComponent } from '../registration/showcase-dialog/term-condtions/term-conditions.component';

@Component({
  selector: 'exp-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  domain: Domain;
  role: Role;
  titleAlert: string = 'Field is required';
  registration: FormGroup;
  hide = true;
  Conhide = true;
  getimagebyte
  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;
  termsCondition: boolean = false;
  regFormProcessing: boolean = false;
  constructor(public sharedService: SharedService, private toastrService: ToastrService,
    private formBuilder: FormBuilder, private authService: AuthService,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog,
    private mailService: MailService) {
    iconRegistry.addSvgIcon(
      'person',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/person.svg'));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RegsuccessComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


  ngOnInit() {
    this.loadbodyclass();
    this.loadDomainData();
    this.loadroleData();
    this.createForm();
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-registration');
  }

  loadDomainData() {
    this.authService.getdomainlist()
      .subscribe((res: Domain) => {
        this.domain = res;
        console.log(res);
      });
  }

  loadroleData() {
    this.authService.getrolelist()
      .subscribe((res: Role) => {
        this.role = res;
      });
  }
  openTermCondtionsDialog(): void {
    const dialogRef = this.dialog.open(TermConditionsComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  createForm() {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.registration = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.pattern(emailregex)]],
      phone: [null, [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      employeeid: [null, Validators.required],
      password: [null, [Validators.required, this.checkPassword]],
      confirmpassword: [null, [Validators.required, this.checkPassword]],
      domain: [null, Validators.required],
      role: [null, Validators.required],
      // password: ['', Validators.required, Validators.maxLength(6)]
    });
  }
  _keyPress(event: any) {
      const pattern = /[0-9]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (!pattern.test(inputChar)) {
          event.preventDefault();

      }
  }
  register() {
    if (this.registration.valid) {
      if(!this.termsCondition){
        this.toastrService.show('info', 'Terms and conditions ', 'Please accept the terms and conditions');
        return;
      }
      this.regFormProcessing = true;
      this.authService.checkusername(this.registration.value).subscribe(res => {
        this.regFormProcessing = false;
        console.log(res.code);
        if (res.code == 200) {
          this.toastrService.show('danger', 'The User already registered', 'Please login');
        } else {
          this.saveRegisterDetails();
          // this.registration.reset();
        }
      }, err => {
        console.error('Error logging in : ', err);
        this.regFormProcessing = false;
        this.toastrService.show('danger', 'Invalid Credatial', 'Username or password is invalid');
      });
    }
  }

  saveRegisterDetails() {
    if (this.registration.valid) {
      console.log(this.registration.value);
      this.authService.register(this.registration.value, this.getimagebyte).subscribe(res => {
        if (res.insertedId) {
          const userid = res.insertedId;
          const userrole = res.ops[0].role;
          if (res.insertedId) {
            console.log(userid);
            console.log(userrole);
            this.authService.setPrevileges(userid, userrole).subscribe(res => {
              
              if (res.insertedId) {
                console.log(res.insertedId);
              }
            });
            let date = new Date().getTime();
            this.authService.userRegistrationStatus(userid, date).subscribe(res => {
              if (res) {
                console.log(res);
                this.openDialog();
              }
            }, err => {
              console.error('Error logging in : ', err);
            });
            this.afterUserRegistartion();
          }
        }
      });
    }
  }
  getErrorEmail() {
    return this.registration.get('email').hasError('required') ? 'Field is required' :
      this.registration.get('email').hasError('pattern') ? 'Not a valid emailaddress' :
        this.registration.get('email').hasError('alreadyInUse') ? 'This emailaddress is already in use' : '';
  }

  get name() {
    return this.registration.get('name');
  }

  getErrorPassword() {
    return this.registration.get('password').hasError('required') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters)' :
      this.registration.get('password').hasError('requirements') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters' : '';
  }

  getErrorConfirmPassword() {
    return this.registration.get('confirmpassword').hasError('required') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters)' :
      this.registration.get('confirmpassword').hasError('requirements') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters' : '';
  }

  checkPassword(control) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{10,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }


  fileChangeEvent(fileInput: any) {
    const reader = new FileReader();
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['image/jpeg', 'image/jpg', 'image/png'];
      const max_height = 300;
      const max_width = 300;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }

      if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
        this.imageError = 'Only Images are allowed ( .jpg )';
        return false;
      }

      reader.onload = (e: any) => {
        const image = new Image();
        this.getimagebyte = e.target.result;
        console.log(this.getimagebyte);
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];

          console.log(img_height, img_width);


          if (img_height > max_height && img_width > max_width) {
            this.imageError =
              'Maximum dimentions allowed ' +
              max_height +
              '*' +
              max_width +
              'px';
            return false;
          } else {
            const imgBase64Path = e.target.result;
            this.cardImageBase64 = imgBase64Path;
            this.isImageSaved = true;
            // this.previewImagePath = imgBase64Path;
          }
        };
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }
    //   removeImage() {
    //     this.cardImageBase64 = null;
    //     this.isImageSaved = false;
    // }

  }

  /**
   * Email intergration for Registred USER
   * @params userEmailId
   */
  afterUserRegistartion() {
    this.mailService.sendMailToUser(this.registration.value).subscribe(res => {
      this.sendMailToAdmin();
    }, err => {
      console.error('Error logging in : ', err);
      this.toastrService.show('danger', 'Error', 'Error');
    });
  }
  // end

  /**
   * email intergartion for admin
   */
  sendMailToAdmin() {
    this.mailService.sendMailToAdmin(this.registration.value).subscribe(res => {
    }, err => {
      console.error('Error logging in : ', err);
      this.toastrService.show('danger', 'Error', 'Error');
    });
  }

  /**
   * end
   */

}
