import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, empty } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Console } from 'console';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  apiUrl = environment.apiUrl; 
  hostUrl = environment.appUrl;
  constructor(private http: HttpClient) {}


  setencrption (data): Observable<any> {
    console.log(data.Email);
    const setmail = {
     'emailid': data.Email,
     };
    return this.http.post (`${this.apiUrl}registration/setmail`, setmail);
  }

  sendreseturl (tomailid,encmailid): Observable<any> {

    const htmltext = `
    <h4>Hello,</h4>
    <h4>We wanted to let you know that your NOVA platform password was reset.</h4>
    <h4>If you did not perform this action, you can recover access by entering ${tomailid.Email} into the form at
    ${this.hostUrl}#/auth/updatepassword?mailid=${encmailid}</h4>
    <h4>Please do not reply to this email with your password. We will never ask for your password, and we strongly discourage you from sharing it with anyone.
    If you run into problems, please contact support by visiting?? </h4><br/>`;
  
    const setmail = {
      'content':htmltext,
      'header':'[Gig] Your password was reset',
      'to': tomailid.Email  
    };
    return this.http.post (`${this.apiUrl}registration/sendresetpass`, setmail);
  }

  decyptpassword (actualmailid): Observable<any> {
    const getdecmailid = {
      'emailid': actualmailid,
      };
      console.log(getdecmailid);
    return this.http.post (`${this.apiUrl}registration/getmail`, getdecmailid);
  }

  sendResetpassword (password,decrptmailid): Observable<any> {
   const sendresetinfo = {
      'username': decrptmailid,
      'newPassword':password.password
    };
   return this.http.post (`${this.apiUrl}registration/resetpassword`, sendresetinfo);
  }

  /**
   * Email intergration for Registred USER service start
   * @params email id
   * @method post
   */
  sendMailToUser(toEmail: any): Observable<any> {
    const userEmailContent = `
    <h4>Hello,</h4>
    <h4>We wanted to let you know that your NOVA platform user registration will be completed.</h4>
    <h4>Please wait for NOVA admin approval once it is done you will get an email notification to your registrated Email ${toEmail.email}
    after which you will be able to login with the given url ${this.hostUrl}#/auth/login </h4>
    <h4>Please do not reply to this email</h4><br/>`;

    const setMailToUser = {
      'content': userEmailContent,
      'header':'[Gig] Your registration will be completed',
      'to': toEmail.email
    };
    return this.http.post (`${this.apiUrl}registration/sendresetpass`, setMailToUser);
    
  }
  // end

  /**
   * Email intergration for admin service start
   * @params email id
   * @method post
   */
  sendMailToAdmin(toEmail: any): Observable<any> {
    const admin = "roopa.rajesh@expleogroup.com";
    const adminEmailContent = `
    <h4>Hello,</h4>
    <h4>We wanted to let you know NOVA platform user ${toEmail.email} is registered.</h4>
    <h4>Please review and approve the registered user on NOVA platform. The registered user will be ${toEmail.email}</h4>
    <h4>Please do not reply to this email</h4><br/>`;

    const setMailToAdmin = {
      'content': adminEmailContent,
      'header':'[Gig] Your registration will be completed',
      'to': admin,
    };
    return this.http.post (`${this.apiUrl}registration/sendresetpass`, setMailToAdmin);
  }
  // end
}

