import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { ThemeModule } from '../@theme/theme.module';
import { NbSelectModule, NbLayoutModule, NbIconModule, NbInputModule ,NbSpinnerModule} from '@nebular/theme';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './auth/auth.component';
import { RegistrationComponent } from './registration/registration.component';

import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from "@angular/material/icon";
import { RegsuccessComponent } from './registration/showcase-dialog/regsuccess/regsuccess.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { UpdatepasswordComponent } from './updatepassword/updatepassword.component';
import { SuccesslinkComponent } from './forgetpassword/show-dialog/successlink/successlink.component';
import { ResetsuccessComponent } from './updatepassword/show-dialog/resetsuccess/resetsuccess.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfilesuccessComponent } from './profile/Show-dialog/profilesuccess/profilesuccess.component';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TermConditionsComponent } from './registration/showcase-dialog/term-condtions/term-conditions.component';

const materialModules = [
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatCheckboxModule,
  MatSlideToggleModule,
  MatRadioModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule
];
@NgModule({
  declarations: [ 
    LoginComponent, 
    AuthComponent, 
    RegistrationComponent, 
    RegsuccessComponent, 
    ForgetpasswordComponent, 
    UpdatepasswordComponent, 
    SuccesslinkComponent, 
    ResetsuccessComponent, 
    ProfileComponent, 
    ProfilesuccessComponent,
    TermConditionsComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AuthRoutingModule,
    NbLayoutModule,
    ThemeModule,
    NbSelectModule,
    SharedModule,
    NbIconModule,
    NbInputModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    ...materialModules,
    NbSpinnerModule,
  ],
  schemas : [CUSTOM_ELEMENTS_SCHEMA],
})
export class AuthModule { }
