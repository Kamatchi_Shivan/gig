import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'exp-resetsuccess',
  templateUrl: './resetsuccess.component.html',
  styleUrls: ['./resetsuccess.component.scss']
})
export class ResetsuccessComponent implements OnInit {

  constructor(private router: Router,public dialogRef: MatDialogRef<ResetsuccessComponent>) { }

  ngOnInit(): void {
    this.loadbodyclass();
  }

  loadbodyclass() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('pg-resetsuccesslink');
  }
  
  onNoClick(): void {
    this.dialogRef.close();
     this.router.navigate(['/auth/login']);
  }

}
