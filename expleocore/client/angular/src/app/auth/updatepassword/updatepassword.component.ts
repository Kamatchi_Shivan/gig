import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { MailService } from '../mail.service';
import { SharedService } from '../../shared/shared.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from '../../shared/toastr/toastr.service';
import { ResetsuccessComponent } from './show-dialog/resetsuccess/resetsuccess.component';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'exp-updatepassword',
  templateUrl: './updatepassword.component.html',
  styleUrls: ['./updatepassword.component.scss']
})
export class UpdatepasswordComponent implements OnInit {
  updatepassword: FormGroup;
  mailid: string;
  actualmailid: string;
  decrptmailid: string;
  hide = true;
  Conhide =true;
  constructor(private route: ActivatedRoute,public sharedService: SharedService,private mailService: MailService,private toastrService: ToastrService, private fb: FormBuilder, private authService: AuthService, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, public dialog: MatDialog, private router: Router) {
    iconRegistry.addSvgIcon(
      'person',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/person.svg'));

    this.updatepassword = this.fb.group({
      password: [null, [Validators.required,this.checkPassword]],
      confirmpassword: [null, [Validators.required,this.checkPassword]],
    });
   }
   get f() { return this.updatepassword.controls; }


  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.mailid = params['mailid'];
      console.log(this.mailid);
      this.actualmailid =this.mailid.replace(" ","+");
       console.log(this.actualmailid);
  })
  this.loadbodyclass();
  }

  
 loadbodyclass() {
  const body = document.getElementsByTagName('body')[0];
  body.classList.add('pg-updatepassword');
}

   openDialog(): void {
    const dialogRef = this.dialog.open(ResetsuccessComponent, {
     width: '500px',
   });
   dialogRef.afterClosed().subscribe(result => {
   console.log('The dialog was closed');
   });
  }

  getErrorconfirmPassword() {
    return this.updatepassword.get('confirmpassword').hasError('required') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters)' :
      this.updatepassword.get('confirmpassword').hasError('requirements') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters' :'';
  }

  getErrorPassword() {
    return this.updatepassword.get('password').hasError('required') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters)' :
      this.updatepassword.get('password').hasError('requirements') ? 'Password must contain at least 10 characters,Including one UPPER case, one lower case letter and one number and one special characters' : '';
  }

  checkPassword(control) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{10,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }

  sendresetpassword(decrptmail) {
     if (decrptmail) {
    this.mailService.sendResetpassword(this.updatepassword.value,this.decrptmailid).subscribe(res => {
        console.log(res);
         if (res.code == 200) {
           this.openDialog();
        } else {
         this.toastrService.show('danger', 'The User is not registered', 'Please register your details');
         }
       }, err => {
           console.error('Error logging in : ', err);
          this.toastrService.show('danger', 'Error', 'Error');
         });
     }
}


resetpassword() {
  if (this.updatepassword.valid) {
    console.log(this.actualmailid);
     this.mailService.decyptpassword(this.actualmailid).subscribe(res => {
      if (res.emailid) {
        this.decrptmailid=res.emailid;
       this.sendresetpassword(res.emailid);
     } else {
      this.toastrService.show('danger', 'The User is not registered', 'Please register your details');
       }
    }, err => {
        console.error('Error logging in : ', err);
       this.toastrService.show('danger', 'Error', 'Error');
      });
  }
}

}
