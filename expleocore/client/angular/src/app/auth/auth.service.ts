import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable, empty } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Domain } from '../models/models';
import { Area } from '../models/models';
import { Role } from '../models/models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  login (username, password): Observable<any> {
      // call api or adfs
      const user = {
        'username': username,
        'password': password,
      };
      console.log(user);
      return this.http.post (`${this.apiUrl}registration/login`, user);
    }

    regStatus (userid): Observable<any> {
      const userID = {
        'userId': userid,
      };
      return this.http.post (`${this.apiUrl}v1/registration/useraccptence`, userID);
    }

    setinserttoken (userdata): Observable<any> {
      const user = {
        'username': userdata.Email,
      };
      console.log()
      return this.http.post (`${this.apiUrl}registration/forgot-password`, user);
    }

    getdomainlist (): Observable<Domain> {
      return this.http.get (`${this.apiUrl}v2/registration/find/Domain`)
      .pipe(map((res: Domain) => {      
        return res;
      }));
    }

    getAreaexpertiselist (postdomaininfo): Observable<any> {
      return this.http.post (`${this.apiUrl}v2/registration/findarea/AreaofInterest`, postdomaininfo);
    }
    
    getrolelist (): Observable<Role> {
      return this.http.get (`${this.apiUrl}v2/registration/findrole/Role`)
      .pipe(map((res: Role) => {     
        return res;
      }));
    }

    checkusername (data): Observable<any> {
      const user = {
        'username': data.email,
      };
      return this.http.post (`${this.apiUrl}registration/usercheck`, user);
    }
    checkuserid (data): Observable<any> {
      const user = {
        'username': data.Email,
      };
      return this.http.post (`${this.apiUrl}registration/usercheck`, user);
    }

    register (data,imgdatabyte): Observable<any> {
      const user = {
        'name': data.name,
        'email': data.email,
        'username': data.email,
        'password': data.password,
        'role': data.role,
        'domain': data.domain,
        'phone': data.phone,
        'employeeid': data.employeeid, 
        'imgpic':imgdatabyte
      };
        return this.http.post (`${this.apiUrl}v2/registration/insert/Register`, user);
    }

    setPrevileges (id, role): Observable<any> { 
     if ("Consultant" == role)
     {
       const userrole = {
      'userId': id,
      'isConsultant': true,
    };
    return this.http.post (`${this.apiUrl}v2/registration/userprevil/UserPrevileges`, userrole);
     }
     /** Remove thE reviewer Role*/
     /**else if("Reviewer" == role)
     {
      const userrole = {
        'userId': id,
        'isReviewer': true,
      };
      return this.http.post (`${this.apiUrl}v2/registration/userprevil/UserPrevileges`, userrole);
     }**/
     else if("Project Manager" == role)
     {
      const userrole = {
        'userId': id,
        'isProjectManager': true,
      };
      return this.http.post (`${this.apiUrl}v2/registration/userprevil/UserPrevileges`, userrole);
     }
     else if("Line Manager" == role)
     {
      const userrole = {
        'userId': id,
        'isLineManager': true,
      };
      return this.http.post (`${this.apiUrl}v2/registration/userprevil/UserPrevileges`, userrole);
     }
    }


    fileinfo (data): Observable<any> {
      console.log(data);
      return this.http.post (`${this.apiUrl}v2/registration/fileinfo/FileInfo`, data);
    }

    userexpertise (data): Observable<any> {
      console.log(data);
      return this.http.post (`${this.apiUrl}v2/registration/expertise/UserExpertise`, data);
    }
    
    updateprofilestatus(data): Observable<any> {
      const info = {
        'username': data
      };
      return this.http.post (`${this.apiUrl}registration/FirstTimeStatus`, info);
    }

    /**
     * user registartion status service start
     * @param id
     * @param date 
     * @method post
     */
    userRegistrationStatus(id: string, date: number): Observable<any> {
      const user = {
        "userId": id,
        "regStatus": "",
        "regComments": "",
        "createdDate": date,
        "lastUpdateDate": date,
      };
      return this.http.post (`${this.apiUrl}v2/ManageUserRegistration/insert/UserRegistrationStatus`, user);
    }
    /**
     * end
     */
  
    // recaptcha (data): Observable<any> {

    //   const info = {
    //     'url': `https://www.google.com/recaptcha/api/siteverify?secret="`${secretKey}`&response="${data}'&remoteip=''`
    //   };
    //   return this.http.post (`${this.apiUrl}v2/registration/captcha`, data);
    // }
    
   
}
