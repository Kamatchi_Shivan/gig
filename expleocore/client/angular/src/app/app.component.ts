/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

import { AnalyticsService } from './@core/utils/analytics.service';
import { NbIconLibraries } from '@nebular/theme';
import { PageTitleService } from './shared/pageTitle.service';

import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
@Component({
  selector: 'exp-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {


  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  constructor(
    private analytics: AnalyticsService, 
    iconsLibrary: NbIconLibraries,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private _pageTitleService: PageTitleService,private idle: Idle, private keepalive: Keepalive
    ) {
    iconsLibrary.registerFontPack('fa', { packClass: 'fa', iconClassPrefix: 'fa' });
    iconsLibrary.registerFontPack('far', { packClass: 'far', iconClassPrefix: 'fa' });
    iconsLibrary.registerFontPack('ion', { iconClassPrefix: 'ion' });
   /****Idle start***/

   idle.setIdle(5);
   // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
   idle.setTimeout(300);
   // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
   idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

   idle.onIdleEnd.subscribe(() => { 
     this.idleState = 'No longer idle.'
     console.log(this.idleState);
     this.reset();
   });
   
   idle.onTimeout.subscribe(() => {
     this.idleState = 'Timed out!';
     this.timedOut = true;
     //console.log(this.idleState);
     this.router.navigate(['/auth']);
     location.reload(); 
   });
   
   idle.onIdleStart.subscribe(() => {
       this.idleState = 'You\'ve gone idle!'
       //console.log(this.idleState);
   });
   
   idle.onTimeoutWarning.subscribe((countdown) => {
     this.idleState = 'You will time out in ' + countdown + ' seconds!'
     //console.log(this.idleState);
   });

   // sets the ping interval to 15 seconds
   keepalive.interval(15);

   keepalive.onPing.subscribe(() => this.lastPing = new Date());

   this.reset();
 }

 reset() {
   this.idle.watch();
   this.idleState = 'Started.';
   this.timedOut = false;
 }


   /*******Idel ends*******/





  


  ngOnInit() {
    this.analytics.trackPageViews();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    )
    .subscribe(() => {

      var rt = this.getChild(this.activatedRoute)

      rt.data.subscribe(data => {
        console.log(data);
        this._pageTitleService.changeData(data.title);
        //this.titleService.setTitle(data.title)
      })
    })
  }

  getChild(activatedRoute: ActivatedRoute) {
    if (activatedRoute.firstChild) {
      return this.getChild(activatedRoute.firstChild);
    } else {
      return activatedRoute;
    }
 
  }
}
