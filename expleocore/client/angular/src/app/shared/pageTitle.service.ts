import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class PageTitleService {

  title :string = '';

  private dataSource = new BehaviorSubject(this.title);
  currPgTitle = this.dataSource.asObservable();

  constructor() { }

  changeData(newData: string){
    this.dataSource.next(newData); 
  }

}