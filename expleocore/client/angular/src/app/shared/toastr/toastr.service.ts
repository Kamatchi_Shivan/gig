import { Injectable } from '@angular/core';
import { NbToastrService, NbToastrConfig, NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus } from '@nebular/theme';

@Injectable({
  providedIn: 'root',
})
export class ToastrService {

  config: NbToastrConfig;

  destroyByClick = true;
  duration = 2000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'primary';

  constructor(private toastrService: NbToastrService) { }

  show(status: NbComponentStatus, title: string, body: string, duration?, hasIcon?, position?, preventDuplicates?) {
    const config = {
      status: status || this.status,
      destroyByClick: this.destroyByClick,
      duration: duration || this.duration,
      hasIcon: hasIcon || this.hasIcon,
      position: position || this.position,
      preventDuplicates: preventDuplicates || this.preventDuplicates,
    };

    this.toastrService.show(body, title, config);
  }
}
