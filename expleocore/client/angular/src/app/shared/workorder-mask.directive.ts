import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[formControlName][expWorkOrderMask]',
})
export class WorkOrderMaskDirective {

  constructor(public ngControl: NgControl) { }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(event) {
    this.onInputChange(event, false);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event) {
    this.onInputChange(event.target.value, true);
  }
  

  onInputChange(event, backspace) {
    let newVal = event.replace(/[^0-9a-z]/gi, '');
    // if (backspace && newVal.length <= 6) {
    //   newVal = newVal.substring(0, newVal.length - 1);
    // }
    if (newVal.length === 0) {
      newVal = '';
    } 
    else if (newVal.length <= 3) {
      newVal = newVal.replace(/[^A-Za-z]/g, '').toUpperCase();
      newVal = newVal.replace(/^(\w{0,3})/, '$1');
    } 
    else if (newVal.length <= 6) {
      let subString = newVal.substring(3, 6).replace(/[^A-Za-z]/g, '').toUpperCase();
      newVal = newVal.substring(0, 3) + subString;

      newVal = newVal.replace(/^(\w{0,3})(\w{0,3})/, '$1/$2');
    } 
    else if (newVal.length <= 10) {
        let subString = newVal.substring(6, 10).replace(/\D/g, '');
        newVal = newVal.substring(0, 6) + subString;

       newVal = newVal.replace(/^(\w{0,3})(\w{0,3})(\d{0,4})/, '$1/$2/$3');
    } 
    else if (newVal.length <= 13) {
        let subString = newVal.substring(10, 13).replace(/\D/g, '');
        newVal = newVal.substring(0, 10) + subString;

        newVal = newVal.replace(/^(\w{0,3})(\w{0,3})(\d{0,4})(\d{0,3})/, '$1/$2/$3/$4');
    } 
    else {
      newVal = newVal.substring(0, 13);
      newVal = newVal.replace(/^(\w{0,3})(\w{0,3})(\d{0,4})(\d{0,3})/, '$1/$2/$3/$4');
    }
    this.ngControl.valueAccessor.writeValue(newVal);
  }
}
