import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class RolesService {

  title :string = '';

  private dataSource = new BehaviorSubject(this.title);
  currRole = this.dataSource.asObservable();

  constructor() { }

  changeData(newData: string){
    this.dataSource.next(newData); 
  }

}