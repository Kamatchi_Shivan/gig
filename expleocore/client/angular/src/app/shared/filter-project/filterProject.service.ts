import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class FilterProjectService {
  filterVisible = false;
  data = { 
        all: null,
        launched: null,
        inProgress: null,
        completed: null,
        onHold: null
  };
  private dataSource = new BehaviorSubject(this.data);
  projectCount = this.dataSource.asObservable();
  private projectType = new BehaviorSubject<any>(undefined);

  constructor() { }
  private showFilterDataSource = new BehaviorSubject(this.filterVisible);
  showFilter = this.showFilterDataSource.asObservable();

  showProjectFilter(newData:boolean) { this.showFilterDataSource.next(newData) }
  changeData(newData: any){
    console.log("newData",newData);
    this.dataSource.next(newData); 
  }

  

  onFilterProject(param: any) {
      this.projectType.next(param);
  }
 
  getFilterType(): BehaviorSubject<any> {
     return this.projectType;
  }

}