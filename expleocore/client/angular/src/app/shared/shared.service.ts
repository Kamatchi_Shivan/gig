import { Injectable } from '@angular/core';
import { User } from '../models/models';
import { Userdetails } from '../models/models';
import { of as observableOf,  Observable } from 'rxjs';

const LANG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  private currentuser;
  userinfo;
  private language = 'en';
  private user: User;
  private domain: Userdetails;
  md;

  constructor() { }

  get selectedLanguage() {
    let lang = this.language;
    if (!lang)
      lang = localStorage.getItem(LANG_KEY);
    return lang;
  }

  set selectedLanguage(lang: string) {
    this.language = lang;
    localStorage.setItem(LANG_KEY, lang);
  }

  public setCurrentUser(user: any) {
    this.user = user;
    this.currentuser=user;
  }

  public getCurrentUser():any {
    return this.user;
  }

  get userdetails() {
    return this.domain;
  }

  set userdetails(domain: Userdetails) {
    this.domain = domain;
    this.userinfo = domain;
  }


  private users = {
    nick: { name: 'Eva Moor', picture: ''},
  };

 // private users = {
  //  nick: { name: 'Eva Moor', picture: ''}
  //   eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
  //   jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
  //   lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
  //   alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
  //   kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
 //  };


  getUsers(): Observable<any> {
    return observableOf(this.users);
  }
}
