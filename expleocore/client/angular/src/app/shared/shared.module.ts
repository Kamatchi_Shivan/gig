import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedService } from './shared.service';
import { RolesService } from './roles.service';

import { NumericDirective } from "./numeric.directive";
import { WorkOrderMaskDirective } from "./workorder-mask.directive";


@NgModule({
  declarations: [NumericDirective, WorkOrderMaskDirective],
  exports:[NumericDirective, WorkOrderMaskDirective],
  imports: [
    CommonModule,
  ],
  providers: [SharedService,RolesService],
})
export class SharedModule { }
