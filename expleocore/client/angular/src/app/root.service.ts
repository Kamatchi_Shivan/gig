import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class RootService {
  mdpic = JSON.parse(localStorage.getItem('user'));;
  apiUrl = environment.apiUrl;
  projectInfo: any;
  schdularLis: any; 
  constructor(private http: HttpClient) { }

  getToken() {
    return localStorage.getItem('auth_token') ;
  }

  getuserimg (userid): Observable<any> {
    const userID = {
      'userId': userid,
    };
    return this.http.post (`${this.apiUrl}userimage`, userID);
  }

  /**
   * get projectInfo
   */
  getProjectInfo() {
      return this.projectInfo;
  }
  /**
   * set projectInfo
   * @param projectInfo 
   */
  setProjectInfo(projectInfo: any) {
    this.projectInfo = projectInfo;
  }

  getSchularList() {
    return this.schdularLis;
  }

  setSchdularList(result: any) {
    this.schdularLis = result.data
  }
  

}
