export const config = {
    environment: process.env.REACT_APP_NODE_ENV,
    serviceUrl: process.env.REACT_APP_SERVICE_URL || 'http://localhost:8000/',
    companyName: 'Expleo',
    secretKey:process.env.REACT_APP_SECRET_KEY,
    appBaseName:process.env.REACT_APP_BASENAME,
    appGAID:process.env.REACT_APP_GAID,
    captcha:"google",
    apiRootPath: process.env.REACT_APP_API_ROOT_PATH ||"http://localhost:8000/api",
    authenticationURL:"http://localhost:8000/api/v1/authenticate"
};
