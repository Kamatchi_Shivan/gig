import { CHANGE_PASSWORD } from "../types";
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
 
export const changePassword = (values:any) => (dispatch:any) => {
    console.log("Values in action",values,);
    let payload = {
        userName:values.Email,
        password: values.ConfirmPassword
    }
    console.log("Inside Update Password...",payload)
    axios.post('http://localhost:8000/api/v1/updatePassword', payload,  {
      headers: {
          'Content-Type': 'application/json',
      }
    })
    .then(res => {
        console.log("In Res Dispatch")
           dispatch({
               payload:{
                   ...res.data
               },
               type: CHANGE_PASSWORD
           })
        })
  };