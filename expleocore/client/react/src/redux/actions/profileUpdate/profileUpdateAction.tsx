import { UPDATE_PROFILE } from "../types";
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
 
export const updateUser = (username:any,status:any,values:any) => (dispatch:any) => {
    console.log("Values in action",values, "Username",username);
    let payload = {
        userName:username,
        status:status,
        firstName: values.firstname,
        middleName: values.middlename,
        lastName: values.lastname
    }
    console.log("Inside Update user...",payload)
    axios.post('http://localhost:8000/api/v1/updateUser/', payload,  {
      headers: {
          'Content-Type': 'application/json',
      }
    })
    .then(res => {
        console.log("In Res Dispatch")
           dispatch({
               payload:{
                   ...res.data
               },
               type: UPDATE_PROFILE
           })
        })
  };