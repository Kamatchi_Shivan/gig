import { LANGUAGE } from '../types';

/* Select Language */
export const inmemoryLanguage = (objData) => dispatch => {
    console.log("inmemoryLanguage action......", objData);
    dispatch({
      payload: objData,
      type: LANGUAGE
    });
  };