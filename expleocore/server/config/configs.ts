export const config = {
    //
    environment: process.env.REACT_APP_NODE_ENV,
    // Configure app port no to run
    app:{
        portNo:process.env.REACT_APP_PORT_NO || 8000
    }

};