let env = require('dotenv')
//initialize .env file
env.config();

let http = require('http');
let express = require("express");
let RED = require("node-red");
let cors = require("cors");
let nodeRedSettings = require('./node-red-settings.ts');

// Create an Express app
let app = express();
//require('./External/routes')(app);

// Add a simple route for static content served from 'public'
app.use("/", express.static("public"));
app.use(cors());
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


app.options('/http://localhost:8500/', function(req, res, next){
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'POST');
    res.header("Access-Control-Allow-Headers", "accept, content-type");
    res.header("Access-Control-Max-Age", "1728000");
    return res.sendStatus(200);
 });
// Create a server
let server = http.createServer(app);

// Initialise the runtime with a server and settings
RED.init(server, nodeRedSettings);

// Serve the editor UI from /red
app.use(nodeRedSettings.httpAdminRoot, RED.httpAdmin);

// Serve the http nodes UI from /api
app.use(nodeRedSettings.httpNodeRoot, RED.httpNode);


server.listen(process.env.APP_PORT_NO);

// Start the runtime
RED.start();
//call upload api
